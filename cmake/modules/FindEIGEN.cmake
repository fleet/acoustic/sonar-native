# Where is eigen3 ?
find_path(EIGEN_INCLUDE_DIR Eigen PATHS ${CONDA_PREFIX}/Library/include/Eigen)
message(STATUS "Found EIGEN ${EIGEN_INCLUDE_DIR}")
find_package(Eigen3 REQUIRED NO_MODULE)

INCLUDE(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Eigen3 DEFAULT_MSG)
