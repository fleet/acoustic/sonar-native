Rem To be launched from root directory in conda environment
call conda activate sonarnative-dev

Rem CLEAN
rmdir /s /q build

Rem BUILD
mkdir build
cd build
cmake .. 
Rem -G "Visual Studio 16 2019"
cmake --build . --config Release --target install

Rem Generate and deploy Maven package, be sure to have Private_token configured in ~/.m2/settings.xml as described in
Rem https://gitlab.ifremer.fr/help/user/packages/pypi_repository/index.md#authenticate-with-a-personal-access-token
call copy "..\script\pom.xml"  "pom.xml"
call mvn clean deploy -P windows

Rem --file script/pom.xml
Rem Generate and deploy Pypi package, be sure to have Private_token configured in ~/.pypirc as described in 
Rem https://gitlab.ifremer.fr/help/user/packages/pypi_repository/index.md#authenticate-with-a-personal-access-token
cd install/modules
python setup.py sdist
python setup.py bdist_wheel --plat-name win32
python setup.py bdist_wheel --plat-name win_amd64
python -m twine upload --repository gitlab dist/*