Rem To be launched from root directory in conda environment
call conda activate sonarnative-dev

Rem CLEAN
rmdir /s /q install

Rem BUILD
mkdir build
cd build
cmake .. 
Rem -G "Visual Studio 16 2019" 
cmake --build . --config Release --target install