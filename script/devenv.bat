Rem To be launched from root directory in conda environment
call conda activate sonarnative-dev

Rem Launch devenv
Rem call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\IDE\devenv.exe"
call "C:\Program Files\Microsoft Visual Studio\2022\Community\Common7\IDE\devenv.exe"
Rem call "C:\Users\asaunier\AppData\Local\Programs\Microsoft VS Code\Code.exe"