#!/bin/bash
set -e
conda activate sonarnative-dev
rm -rf build-docker-linux
mkdir build-docker-linux
cd build-docker-linux
cmake ..
cmake --build . --config Release --target install

cd install/modules
python setup.py bdist_wheel