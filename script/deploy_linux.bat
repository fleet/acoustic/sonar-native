Rem To be launched from root directory in conda environment
Rem call docker-compose down --rmi all -v --remove-orphans
call docker-compose run --rm build-sonarnative

Rem to be moved in docker ?
call conda activate sonarnative-dev

cd build-docker-linux
Rem Generate and deploy Maven package, be sure to have Private_token configured in ~/.m2/settings.xml as described in
Rem https://gitlab.ifremer.fr/help/user/packages/pypi_repository/index.md#authenticate-with-a-personal-access-token
call copy "..\script\pom.xml"  "pom.xml"
call mvn clean deploy -P linux
Rem --file script/pom.xml
Rem Generate and deploy Pypi package, be sure to have Private_token configured in ~/.pypirc as described in 
Rem https://gitlab.ifremer.fr/help/user/packages/pypi_repository/index.md#authenticate-with-a-personal-access-token
cd install/modules
python -m twine upload --repository gitlab dist/*