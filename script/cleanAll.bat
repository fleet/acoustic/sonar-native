Rem To be launched from root directory in conda environment
call conda activate sonarnative-dev

Rem CLEAN
rmdir /s /q install
rmdir /s /q build
