#include <iostream>

#include "sonarnative/sonarnative.hpp"
#include "sonarnative/sonarnative_config.hpp"

int main(int argc, char *argv[]){
    std::cout<<"SONAR_NATIVE_VERSION_MAJOR "<<SONAR_NATIVE_VERSION_MAJOR<<"\n";
    std::cout<<"SONAR_NATIVE_VERSION_MINOR "<<SONAR_NATIVE_VERSION_MINOR<<"\n";
    std::cout<<"SONAR_NATIVE_VERSION_PATCH "<<SONAR_NATIVE_VERSION_PATCH<<"\n";
}