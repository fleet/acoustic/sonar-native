#pragma once

#include <sonarnative/log/proxy.hpp>
#include <sonarnative/log/appender.hpp>

namespace sonarnative {
    

	/*
	  Singleton to manage the log proxies
	  Provides static methods to add or remove LogProxy
	*/
	class Log
	{
		Log();

	public :
		/* Access method to the singleton */
		static Log* instance();
		/* Add a new LogProxy */
		static void initializeLog(sonarnative::LogProxy* p);
		/* Clear added LogProxy */
		static void detachProxy();
		/* Destructor */
		virtual ~Log();


	private:
		/* Add a new LogProxy */
		void initialize(sonarnative::LogProxy* p);
		/* Free current appender */
		void removeAppender();

		/* Static instance of the singleton */
		static Log* singleton;
		sonarnative::LogAppender* appender;
	};
} // end namespace sonarnative