/*
 * CustomLayout.hpp
 *
 * Custom Layout inspired from BasicLayout.hpp to avoid line breaks 
 */

#pragma once

#include "log4cpp/Portability.hh"
#include "log4cpp/Layout.hh"

namespace sonarnative {
    /**
     * BasicLayout is a simple fixed format Layout implementation. 
     **/
    class CustomLayout : public log4cpp::Layout {
        public:
        CustomLayout();
        virtual ~CustomLayout();

        /**
         * Formats the LoggingEvent in CustomLayout style:<br>
         * "timeStamp priority category ndc: message"
         **/
        virtual std::string format(const log4cpp::LoggingEvent& event);
    };        
}