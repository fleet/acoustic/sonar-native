#pragma once

#include <string>

namespace sonarnative {
    
	/*
	 * Pure virtual class. Have to provide implementations from Java or Python.
	 * Implement this class for your own strategies for printing log statements.
	 */
	class LogProxy
    {
    public:
		// Constructor
		LogProxy() {};
		// Destructor
		virtual ~LogProxy() {};


		/**
		 * Log a message with fatal priority.
		 * @param message string to write in the log file
		 **/
		virtual void fatal(std::string message) = 0;
		/**
		 * Log a message with error priority.
		 * @param message string to write in the log file
		 **/
		virtual void error(std::string message) = 0;
		/**
		 * Log a message with warning priority.
		 * @param message string to write in the log file
		 **/
		virtual void warn(std::string message) = 0;
		/**
		 * Log a message with info priority.
		 * @param message string to write in the log file
		 **/
		virtual void info(std::string message) = 0;
		/**
		 * Log a message with debug priority.
		 * @param message string to write in the log file
		 **/
		virtual void debug(std::string message) = 0;
    };

} // end namespace sonarnative