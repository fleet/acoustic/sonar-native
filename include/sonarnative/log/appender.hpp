#pragma once

#include <log4cpp/LayoutAppender.hh>
#include <log4cpp/LoggingEvent.hh>

#include "sonarnative/log/proxy.hpp"

namespace sonarnative {

	/*
	 *  Implement this interface for your own strategies for printing log
	 *  statements.
	 */
	class LogAppender : public log4cpp::LayoutAppender {
	public:

		LogAppender(LogProxy * p);
		virtual ~LogAppender();

		virtual bool reopen();
		virtual void close();

	protected:

		virtual void _append(const log4cpp::LoggingEvent& event);

		LogProxy * proxy;
	};
}