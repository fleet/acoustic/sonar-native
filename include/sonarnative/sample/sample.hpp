#pragma once

#include <string>

#include <sonarnative/utils/progress_callback.hpp>

namespace sonarnative {
    
    class Sample
    {
    public:
        Sample(std::string name); 
		std::string name();

		/* Fake method to get the value given as parameter */
		int getVal(int val) { return val; }

		/* Little loop with progress feedback for each step */
		void count(int from, int to, sonarnative::ProgressCallback* callback);

	private:
        std::string m_name;
    };

} // end namespace sonarnative