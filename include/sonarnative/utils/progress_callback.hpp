#pragma once

#include <string>

namespace sonarnative {
    
	/*
	 * Pure virtual class. Have to provide implementations from Java or Python.
	 * Implement this class to have a the progress feedback.
	 */
	class ProgressCallback
    {
    public:
		// Constructor
		ProgressCallback() {};
		// Destructor
		virtual ~ProgressCallback() {};


		/**
		 * Progression evolution.
		 * @param complete progression between 0.0 and 1.0
		 **/
		virtual void progress(double complete, std::string message) const = 0;
    };

} // end namespace sonarnative