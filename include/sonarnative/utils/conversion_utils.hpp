#pragma once

#include <stdint.h>
namespace sonarnative {

	// COMMON FUNCTIONS ------------------------------------------------------------ 
	double toRadians(double x);
	double toDegrees(double x);

	float dBToEnergy(float dB);
	float energyTodB(float value);
	float dBToAmplitude(float dB);
	float amplitudeTodB(float value);

	inline uint64_t toNanoseconds(float seconds) { return static_cast<uint64_t>(seconds * 1e9); }
} // end namespace sonarnative