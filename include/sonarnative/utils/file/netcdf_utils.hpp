#pragma once

#include <string>
#include <vector>
#include <memory>
#include <netcdf.h>

namespace sonarnative {

	/*
		Open a netcdf file in read mode and return the netcdf ID
		Raise string on error
	*/
	int nc_open_file(std::string path);

	/*
		Close a netcdf file
		Raise string on error
	*/
	void nc_close_file(int file_id);

	/*
		Close a netcdf file
		return the error code
	*/
	int nc_close_quietly(int file_id);

	/*
		return the full ncid given a group name. -1 on error
	*/
	int nc_get_group_quietly(int file_id, std::string group_path);

	/*
		return the full ncid given a group name
		Raise string on error
	*/
	int nc_get_group(int file_id, std::string group_path);

	/*
		return the ID of a variable, from the name
		On error, return -1
	*/
	int nc_get_variable_quietly(int group_id, std::string var_name);

	/*
		return the ID of a variable, from the name
		Raise string on error
	*/
	int nc_get_variable(int group_id, std::string var_name);

	/*
		return the  length of a dimension
		Raise string on error
	*/
	size_t nc_get_dimension(int group_id, std::string dim_name);

	/*
		return the length of all dimensions of the specified variable
		Raise string on error
	*/
	std::vector<size_t> nc_get_var_dimensions(int group_id, int var_id);

	/*
		return the type of the specified variable
		Raise string on error
	*/
	nc_type nc_get_var_type(int group_id, int var_id);

	/*
		return the base type of the specified vlen
		Raise string on error
	*/
	nc_type nc_get_vlen_base_type(int group_id, const char* vlen_type_name);

	/*
		Fill the resulting buffer with some values of the variable
		Raise string on error
	*/
	void nc_fill_with_values(int group_id, int var_id, const size_t* from, const size_t* count, void* result);

	/*
		Return the offset of the variable. 0.0 by default
	*/
	float nc_offset(int group_id, int var_id);

	/*
		Return the scale factor of the variable. 1.0 by default
	*/
	float nc_scale_factor(int group_id, int var_id);

	/*
		Return the missing value of the variable. -32768 by default
	*/
	short nc_short_missing_value(int group_id, int var_id);

	/*
	*   Returns the group attribute. -32768 by default
	*/
	int nc_group_att_int(int group_id, const char* attName);

	/*
	*   Returns the group attribute. Empty string by default
	*/
	std::string nc_group_att_string(int group_id, const char* attName);

	/*
		Free memory of a read VLEN type
	*/
	void nc_delete_vlen(std::vector<nc_vlen_t>* vlen);

	/* Read the entire double variable
		Raise string on error 
		"buffer" is filled with the doubles when not null. Otherwise, an array of float
		is created and returned.
	*/
	double* nc_read_double(int group_id, int var_id, std::unique_ptr<std::vector<char>>& buffer);
	float* nc_read_float(int group_id, int var_id, std::unique_ptr<std::vector<char>>& buffer);
	int* nc_read_int(int group_id, int var_id, std::unique_ptr<std::vector<char>>& buffer);
	unsigned char* nc_read_byte(int group_id, int var_id, std::unique_ptr<std::vector<char>>& buffer);
	uint64_t* nc_read_ulong(int group_id, int var_id, std::unique_ptr<std::vector<char>>& buffer);

	/*
		Read the entire string variable
	*/
	std::vector<std::string> nc_read_string_var(const int group_id, const int var_id);

	/* Read some float data from the variable specified by its group_id/var_id.
		Raise string on error
		Floats are read along the first dimension from index "from" to "from + nb".
		"buffer" is filled with the floats when not null. Otherwise, an array of float
		is created and returned.
	*/
	float* nc_read_float_2d_var(int group_id, int var_id, int from, int nb, std::unique_ptr<std::vector<char>>& buffer);
	double* nc_read_double_2d_var(int group_id, int var_id, int from, int nb, std::unique_ptr<std::vector<char>>& buffer);
	int* nc_read_int_2d_var(int group_id, int var_id, int from, int nb, std::unique_ptr<std::vector<char>>& buffer);
	short* nc_read_short_2d_var(int group_id, int var_id, int from, int nb, std::unique_ptr<std::vector<char>>& buffer);
	unsigned char* nc_read_byte_2d_var(int group_id, int var_id, int from, int nb, std::unique_ptr<std::vector<char>>& buffer);

	/*
		Read some vlen data from the variable specified by its group_id/var_id.
		Raise string on error
		vlens are read along the first dimension from index "from" to "from + nb".
	*/
	std::unique_ptr<std::vector<nc_vlen_t>> nc_read_vlen_var(int group_id, int var_id, int from, int nb);

} // end namespace sonarnative
