#pragma once

#include <string>
#include <limits>

#include <cstdio>
#include <fstream>

namespace sonarnative {

	template <typename T>
	class MappedFile {
	public:
		MappedFile(const std::string &filePath);
		~MappedFile();

		T get(long index);
		void set(long index, T value);
		void close();

	private:
		int getMappedIndex(long index);

		std::ifstream stream;
		T buffer[1024];
		size_t bufSize;
		long currentIndex;
		bool dirty = false;
	};

	template <class T>
	MappedFile<T>::MappedFile(const std::string& filePath)  
	{
		stream = std::ifstream(filePath, std::ios::binary);
		bufSize = 0;
		currentIndex = std::numeric_limits<long>::max();
	}

	template <class T>
	MappedFile<T>::~MappedFile()
	{
		close();
	}

	template <class T>
	T MappedFile<T>::get(long index)
	{
		int mappedIndex = getMappedIndex(index);
		return mappedIndex >= 0 ? buffer[mappedIndex] : std::numeric_limits<T>::quiet_NaN();
	}

	template <class T>
	int MappedFile<T>::getMappedIndex(long index)
	{
		int result = -1;

		// Loading buffer ?
		if (index < currentIndex || index >= currentIndex + bufSize)
		{
			stream.seekg(index * sizeof(T));
			if (stream.good())
			{
				stream.read((char*)buffer, sizeof(T) * 1024);
				bufSize = stream.gcount() / sizeof(T);
				currentIndex = index;
			}
		}

		if (index >= currentIndex && index < currentIndex + bufSize)
		{
			result = (int) (index - currentIndex);
		}

		return result;
	}

	template <class T>
	void MappedFile<T>::close()
	{
		stream.close();
	}
}
