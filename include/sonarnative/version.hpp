#pragma once

#include <string>

namespace sonarnative {
	// Return the version of this library
	std::string version();
} // end namespace sonarnative
