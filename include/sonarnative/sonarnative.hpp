#pragma once

namespace sonarnative {

	// COMMON TYPES ------------------------------------------------------------ 

	enum class SpatializationMethod
	{
		DetectionInterpolation,
		EquivalentCelerity,
		ConstantCelerity
	};

	// COMMON STRUCTURES ------------------------------------------------------------ 

	/* Geographical point Longitude/Latitude */
	struct LonLat  {
		double longitude, latitude, longitudeRad, latitudeRad;
		LonLat() : longitude(0.0), latitude(0.0), longitudeRad(0.0), latitudeRad(0.0) { }
		LonLat(double longitude, double latitude) : longitude(longitude), latitude(latitude), longitudeRad(0.0), latitudeRad(0.0) { }
	};

	/* Geographical point Longitude/Latitude/depth */
	struct LonLatZ : LonLat {
		double depth;
		LonLatZ() : LonLat(0.0, 0.0), depth(0.0) { }
		LonLatZ(double longitude, double latitude, double depth) :
			LonLat(longitude, latitude), depth(depth) { }
	};

} // end namespace sonarnative