#pragma once

#include <netcdf.h>

#include <array>
#include <memory>
#include <string>
#include <vector>

namespace sonarnative {
namespace xsf {

enum GRP_IDX {
    GRP_IDX_SONAR,
    GRP_IDX_CURRENT_BEAM_GROUP,
    GRP_IDX_BATHYMETRY,
    GRP_IDX_PLATFORM,
    GRP_IDX_ATTITUDE,
    GRP_IDX_ATTITUDE_SUBGROUP,
    GRP_IDX_NUM
};

/* Netcdf Dimensions */
enum DIM_IDX { DIM_IDX_PING_TIME, DIM_IDX_BEAM, DIM_IDX_TX_BEAM, DIM_IDX_TRANSDUCER, DIM_IDX_DETECTION, DIM_IDX_ATTITUDE_TIME, DIM_IDX_NUM };

/* Netcdf Variables */
enum VAR_IDX {
    VAR_IDX_TRANSDUCER_FUNCTION,
    VAR_IDX_TRANSDUCER_OFFSET_X,
    VAR_IDX_TRANSDUCER_OFFSET_Y,
    VAR_IDX_TRANSDUCER_OFFSET_Z,
    VAR_IDX_TRANSDUCER_ROTATION_X,
    VAR_IDX_TRANSDUCER_ROTATION_Y,
    VAR_IDX_TRANSDUCER_ROTATION_Z,
    VAR_IDX_ATTITUDE_ROLL,
    VAR_IDX_ATTITUDE_PITCH,
    VAR_IDX_ATTITUDE_TIME,
    VAR_IDX_DETECTION_TX_BEAM,
    VAR_IDX_DETECTION_RX_TRANSDUCER_INDEX,
    VAR_IDX_DETECTION_TX_TRANSDUCER_INDEX,
    VAR_IDX_STATUS,
    VAR_IDX_DETECTION_X,
    VAR_IDX_DETECTION_Y,
    VAR_IDX_DETECTION_Z,
    VAR_IDX_DETECTION_BEAM_POINTING_ANGLE,
    VAR_IDX_DETECTION_BEAM_STABILISATION,
    VAR_IDX_PLATFORM_LATITUDE,
    VAR_IDX_PLATFORM_LONGITUDE,
    VAR_IDX_PLATFORM_HEADING,
    VAR_IDX_PLATFORM_PITCH,
    VAR_IDX_PLATFORM_ROLL,
    VAR_IDX_PING_TIME,
    VAR_IDX_SAMPLE_INTERVAL,
    VAR_IDX_SOUND_SPEED_AT_TRANSDUCER,
    VAR_IDX_RX_BEAM_ROTATION_PHI,
    VAR_IDX_TX_BEAM_ROTATION_THETA,
    VAR_IDX_BEAMWIDTH_RECEIVE_MAJOR,
    VAR_IDX_BEAMWIDTH_TRANSMIT_MINOR,
    VAR_IDX_BACKSCATTER_R,
    VAR_IDX_TX_TRANSDUCER_DEPTH,
    VAR_IDX_RECEIVE_TRANSDUCER_INDEX,
    VAR_IDX_TRANSMIT_TRANSDUCER_INDEX,
    VAR_IDX_TRANSMIT_BEAM_INDEX,
    VAR_IDX_DETECTED_BOTTOM_RANGE,
    VAR_IDX_BLANKING_INTERVAL,
    VAR_IDX_SAMPLE_TIME_OFFSET,
    VAR_IDX_DETECTION_TWO_WAY_TRAVEL_TIME,
    VAR_IDX_MULTIPING_SEQUENCE,
    VAR_IDX_NUM
};

class XsfDriver {
   public:
    /* Constructor / Destructor */
    XsfDriver(std::string xsf_path, int ncid = -1);
    XsfDriver(XsfDriver&& to_move) noexcept;
    ~XsfDriver() = default;

    /* Open the file in a read mode. On success, return the Netcdf ID */
    int open();

    void close();

    /* Return true if the file contains bathimetric group and variable */
    bool has_bathymetry() { return bathymetry_available; }
    /* Return true if the file contains attitude group and variable */
    bool has_attitude() { return nc_grp_ids[GRP_IDX_ATTITUDE_SUBGROUP] != -1; }
    /* Return true if the manufacturer is Reson */
    bool isReson() { return is_reson; }

    /* Return the Netcdf ID of the XSF file */
    int get_file_id();

    /* Return the number currently used to identify the Beam_group to use */
    int get_beam_group_number();
    /* Change the number of the Beam_group to use. Return true on sucess */
    bool change_beam_group_number(int other_beam_group_number);

    /* Return the group ID of Sonar/Beam_group1. Raise std::string on error */
    int get_group_id_sonar();
    /* Return the group ID of Sonar/Beam_group1. Raise std::string on error */
    int get_group_id_beam_group();
    /* Return the group ID of Platform. Raise std::string on error */
    int get_group_id_platform();
    /* Return the group ID of Bathymetry. Raise std::string on error */
    int get_group_id_bathymetry();
    /* Return preferred attitude subgroup or first one if not found */
    int get_preferred_attitude_subgroup_id();

    /* Return the variable ID of transducer_offset_x. Raise std::string on error */
    int get_var_id_transducer_function();
    /* Return the variable ID of transducer_offset_x. Raise std::string on error */
    int get_var_id_transducer_offset_x();
    /* Return the variable ID of transducer_offset_y. Raise std::string on error */
    int get_var_id_transducer_offset_y();
    /* Return the variable ID of transducer_offset_z. Raise std::string on error */
    int get_var_id_transducer_offset_z();
    /* Return the variable ID of transducer_rotation_x. Raise std::string on error */
    int get_var_id_transducer_rotation_x();
    /* Return the variable ID of transducer_rotation_y. Raise std::string on error */
    int get_var_id_transducer_rotation_y();
    /* Return the variable ID of transducer_rotation_z. Raise std::string on error */
    int get_var_id_transducer_rotation_z();
    /* Return the variable ID of detection_tx_beam. Raise std::string on error */
    int get_var_id_detection_tx_beam();

    /* Return the variable ID of detection_rx_transducer_index. Raise std::string on error */
    int get_var_id_detection_rx_transducer_index();
    /* Return the variable ID of detection_tx_transducer_index. Raise std::string on error */
    int get_var_id_detection_tx_transducer_index();
    /* Return the variable ID of status. Raise std::string on error */
    int get_var_id_status();
    /* Return the variable ID of detection_x. Raise std::string on error */
    int get_var_id_detection_x();
    /* Return the variable ID of detection_y. Raise std::string on error */
    int get_var_id_detection_y();
    /* Return the variable ID of detection_z. Raise std::string on error */
    int get_var_id_detection_z();
    /* Return the variable ID of detection_beam_pointing_angle. Raise std::string on error */
    int get_var_id_detection_beam_pointing_angle();
    /* Return the variable ID of detection_beam_stabilisation. Raise std::string on error */
    int get_var_id_detection_beam_stabilisation();
    /* Return the variable ID of platform_latitude. Raise std::string on error */
    int get_var_id_platform_latitude();
    /* Return the variable ID of platform_longitude. Raise std::string on error */
    int get_var_id_platform_longitude();
    /* Return the variable ID of platform_heading. Raise std::string on error */
    int get_var_id_platform_heading();
    /* Return the variable ID of platform_heading. Raise std::string on error */
    int get_var_id_platform_roll();
    /* Return the variable ID of ping_time. Raise std::string on error */
    int get_var_id_ping_time();
    /* Return the variable ID of sample_interval. Raise std::string on error */
    int get_var_id_sample_interval();
    /* Return the variable ID of sound_speed_at_transducer. Raise std::string on error */
    int get_var_id_sound_speed_at_transducer();
    /* Return the variable ID of rx_beam_rotation_phi. Raise std::string on error */
    int get_var_id_rx_beam_rotation_phi();
    /* Return the variable ID of tx_beam_rotation_theta. Raise std::string on error */
    int get_var_id_tx_beam_rotation_theta();
    /* Return the variable ID of beamwidth_receive_major. Raise std::string on error */
    int get_var_id_beamwidth_receive_major();
    /* Return the variable ID of beamwidth_transmit_minor. Raise std::string on error */
    int get_var_id_beamwidth_transmit_minor();
    /* Return the variable ID of backscatter_r. Raise std::string on error */
    int get_var_id_backscatter_r();
    /* Return the variable ID of Attitude roll. Raise std::string on error */
    int get_var_id_attitude_roll();
    /* Return the variable ID of Attitude time. Raise std::string on error */
    int get_var_id_attitude_time();

    /* Return the entire platform_latitude as double. Raise std::string on error */
    double* read_var_platform_latitude();
    /* Return the entire platform_longitude as double. Raise std::string on error */
    double* read_var_platform_longitude();
    /* Return the entire platform_heading as float. Raise std::string on error */
    float* read_var_platform_heading();
    /* Return the entire platform_pitch as float. Raise std::string on error */
    float* read_var_platform_pitch();
    /* Return the entire platform_roll as float. Raise std::string on error */
    float* read_var_platform_roll();
    /* Return the entire ping_time as ulong. Raise std::string on error */
    uint64_t* read_var_ping_time();
    /* Return the entire sample_interval as float. Raise std::string on error */
    float* read_var_sample_interval();
    /* Return the entire sound_speed_at_transducer as float. Raise std::string on error */
    float* read_var_sound_speed_at_transducer();
    /* Return the entire tx_transducer_depth as float. Raise std::string on error */
    float* read_var_tx_transducer_depth();
    /* Return the entire transducer_function as byte. Raise std::string on error */
    unsigned char* read_var_transducer_function();
    /* Return the entire transducer_offset_x as float. Raise std::string on error */
    float* read_var_transducer_offset_x();
    /* Return the entire transducer_offset_y as float. Raise std::string on error */
    float* read_var_transducer_offset_y();
    /* Return the entire transducer_offset_z as float. Raise std::string on error */
    float* read_var_transducer_offset_z();
    /* Return the entire transducer_rotation_x as float. Raise std::string on error */
    float* read_var_transducer_rotation_x();
    /* Return the entire transducer_rotation_y as float. Raise std::string on error */
    float* read_var_transducer_rotation_y();
    /* Return the entire transducer_rotation_z as float. Raise std::string on error */
    float* read_var_transducer_rotation_z();
    /* Return the beamwidth_receive_major as float. Raise std::string on error */
    float* read_var_beamwidth_receive_major();
    /* Return the beamwidth_transmit_minor as float. Raise std::string on error */
    float* read_var_beamwidth_transmit_minor();
    /* Return the entire sample_time_offset as float. Raise std::string on error */
    float* read_var_sample_time_offset();
    /* Return receive_transducer_index as int. Raise std::string on error */
    int* read_var_receive_transducer_index();
    /* Return the entire attitude roll as float. Raise std::string on error */
    float* read_var_attitude_roll();
    /* Return the entire attitude pitch as float. Raise std::string on error */
    float* read_var_attitude_pitch();
    /* Return the entire attitude time as unsigned long. Raise std::string on error */
    uint64_t* read_var_attitude_time();
    /* Return the entire detection_beam_stabilisation as byte. Raise std::string on error */
    int8_t* read_var_detection_beam_stabilisation();
    /* Return the entire multiping_sequence as byte. Raise std::string on error */
    unsigned char* read_var_multiping_sequence();


    /* Return some transmit_transducer_index as int. Raise std::string on error */
    int* read_var_transmit_transducer_index(int from, int nb);
    /* Return the some rx_beam_rotation_phi as float. Raise std::string on error */
    float* read_var_rx_beam_rotation_phi(int from, int nb);
    /* Return the some tx_beam_rotation_theta as float. Raise std::string on error */
    float* read_var_tx_beam_rotation_theta(int from, int nb);
    /* Return the some transmit_beam_index as int. Raise std::string on error */
    int* read_var_transmit_beam_index(int from, int nb);
    /* Return the some transmit_beam_index as int. Raise std::string on error */
    short* read_var_detection_tx_beam(int from, int nb);
    /* Return the some detection_rx_transducer_index as short. Raise std::string on error */
    short* read_var_detection_rx_transducer_index(int from, int nb);
    /* Return the some detection_tx_transducer_index as short. Raise std::string on error */
    short* read_var_detection_tx_transducer_index(int from, int nb);
    /* Return the some status as byte. Raise std::string on error */
    unsigned char* read_var_status(int from, int nb);
    /* Return the some detection_x as float. Raise std::string on error */
    float* read_var_detection_x(int from, int nb);
    /* Return the some detection_y as float. Raise std::string on error */
    float* read_var_detection_y(int from, int nb);
    /* Return the some detection_z as float. Raise std::string on error */
    float* read_var_detection_z(int from, int nb);
    /* Return the some detection_beam_pointing_angle as float. Raise std::string on error */
    float* read_var_detection_beam_pointing_angle(int from, int nb);
    /* Return some detected_bottom_range as float. Raise std::string on error */
    float* read_var_detected_bottom_range(int from, int nb);
    /* Return some blanking_interval as float. Raise std::string on error */
    float* read_var_blanking_interval(int from, int nb);
    /* Return some detection_two_way_travel_tim as float. Raise std::string on error */
    float* read_var_detection_two_way_travel_time(int from, int nb);

    std::vector<nc_vlen_t>* load_vlen_backscatter_r(int ping_time_idx);
    float get_backscatter_r_offset();
    float get_backscatter_r_scale();
    nc_type get_backscatter_r_type();
    short get_backscatter_r_missing_value();

    /* Return true if the variable tx_transducer_depth is present in the file. */
    bool has_var_tx_transducer_depth();
    /* Return true if the variable receive_transducer_index is present in the file. */
    bool has_var_receive_transducer_index();
    /* Return true if the variable transmit_transducer_index is present in the file. */
    bool has_var_transmit_transducer_index();
    /* Return true if the variable transmit_beam_index is present in the file. */
    bool has_var_transmit_beam_index();
    /* Return true if the variable detected_bottom_range is present in the file. */
    bool has_var_detected_bottom_range();
    /* Return true if the variable detection_two_way_travel_time is present in the file. */
    bool has_var_detection_two_way_travel_time();
    /* Return true if the variable multiping_sequence is present in the file. */
    bool has_var_multiping_sequence();
    /* Return true if the variable detection_beam_stabilisation is present in the file. */
    bool has_var_detection_beam_stabilisation();

    /* Return the ping_time dimension. Raise std::string on error */
    size_t get_dim_ping_time();
    /* Return the beam dimension. Raise std::string on error */
    size_t get_dim_beam();
    /* Return the tx_beam dimension. Raise std::string on error */
    size_t get_dim_tx_beam();
    /* Return the transducer dimension. Raise std::string on error */
    size_t get_dim_transducer();
    /* Return the detection dimension. Raise std::string on error */
    size_t get_dim_detection();
    /* Return the attitude time dimension. Raise std::string on error */
    size_t get_dim_attitude_time();

    const std::vector<int> getRxTransducers() { return rx_transducer_idx; }
    const std::vector<int> getTxTransducers() { return tx_transducer_idx; }

   private:
    /* Path of the XSF file */
    std::string xsf_path;
    /* True when XSF contains bathymetric data */
    bool bathymetry_available = false;
    /* Netcdf ID of the XSF file */
    int file_id = -1;
    /* Is open internally (should be close)*/
    bool is_internally_open = false;
    /* Number of the current used group Beam_group. */
    int beam_group_number = 0;
    /* True if Xsf comes from Reson */
    bool is_reson = false;

    /* Indices of receive transducers*/
    std::vector<int> rx_transducer_idx;
    /* Indices of transmit transducers*/
    std::vector<int> tx_transducer_idx;

    /* All managed groups */
    std::array<std::string, GRP_IDX_NUM> GRP_NAMES{
        "Sonar",
        "Sonar/Beam_group1",             // May changed, depends on beam_group_number
        "Sonar/Beam_group1/Bathymetry",  // May changed, depends on beam_group_number
        "Platform",
        "Platform/Attitude",
        "Platform/Attitude/001"  // May changed, depends on mru_id
    };
    /* Return the NC ID of a group specified by its index. Raise std::string on error */
    int get_nc_group_id(int group_idx);
    /* Netcdf group/variable IDs */
    std::array<int, GRP_IDX_NUM> nc_grp_ids{};

    /* All managed variables */
    const std::array<std::string, VAR_IDX_NUM> VAR_NAMES{"transducer_function",
                                                         "transducer_offset_x",
                                                         "transducer_offset_y",
                                                         "transducer_offset_z",
                                                         "transducer_rotation_x",
                                                         "transducer_rotation_y",
                                                         "transducer_rotation_z",
                                                         "roll",
                                                         "pitch",
                                                         "time",
                                                         "detection_tx_beam",
                                                         "detection_rx_transducer_index",
                                                         "detection_tx_transducer_index",
                                                         "status",
                                                         "detection_x",
                                                         "detection_y",
                                                         "detection_z",
                                                         "detection_beam_pointing_angle",
                                                         "detection_beam_stabilisation",
                                                         "platform_latitude",
                                                         "platform_longitude",
                                                         "platform_heading",
                                                         "platform_pitch",
                                                         "platform_roll",
                                                         "ping_time",
                                                         "sample_interval",
                                                         "sound_speed_at_transducer",
                                                         "rx_beam_rotation_phi",
                                                         "tx_beam_rotation_theta",
                                                         "beamwidth_receive_major",
                                                         "beamwidth_transmit_minor",
                                                         "backscatter_r",
                                                         "tx_transducer_depth",
                                                         "receive_transducer_index",
                                                         "transmit_transducer_index",
                                                         "transmit_beam_index",
                                                         "detected_bottom_range",
                                                         "blanking_interval",
                                                         "sample_time_offset",
                                                         "detection_two_way_travel_time",
                                                         "multiping_sequence"};

    /* Netcdf group/variable IDs */
    std::array<int, VAR_IDX_NUM> nc_var_ids{};
    /* Buffers of variables content */
    std::array<std::unique_ptr<std::vector<char>>, VAR_IDX_NUM> var_values;
    /* Return true if the variable is present in the file. */
    bool has_var(int group_idx, int var_idx);
    /* Return the ID of a variable specified by its index. Raise std::string on error */
    int get_var_id(int group_idx, int var_idx);

    /* Return the entire double variable specified by its index. Raise std::string on error */
    double* read_double_var(int group_idx, int var_idx);
    float* read_float_var(int group_idx, int var_idx);
    int* read_int_var(int group_idx, int var_idx);
    uint64_t* read_ulong_var(int group_idx, int var_idx);
    unsigned char* read_byte_var(int group_idx, int var_idx);

    /* Return the a part of the variable values. Raise std::string on error */
    float* read_float_2d_var(int group_idx, int var_idx, int from, int nb);
    int* read_int_2d_var(int group_idx, int var_idx, int from, int nb);
    short* read_short_2d_var(int group_idx, int var_idx, int from, int nb);
    unsigned char* read_byte_2d_var(int group_idx, int var_idx, int from, int nb);

    /* All managed dimensions */
    const std::array<std::string, DIM_IDX_NUM> DIM_NAMES{"ping_time", "beam", "tx_beam", "transducer", "detection", "time"};
    /* Return the size of a dimension specified by its index. Raise std::string on error */
    size_t get_dim(int group_idx, int dim_idx);
    /* Netcdf dimensions */
    std::array<size_t, DIM_IDX_NUM> nc_dims{};

    /* buffer of loaded backscatter_r */
    using VlenDeleter = void (*)(std::vector<nc_vlen_t>*);
    using VlenUniquePtr = std::unique_ptr<std::vector<nc_vlen_t>, VlenDeleter>;
    VlenUniquePtr vlen_backscatter_r;

    /* Reset this instance after construction or movement */
    void reset() noexcept;

    /* Reads transducers indices */
    void readTransducers();

    /* Reads constructor value and checks if its Reson */
    void readConstructor();

    /* Reads attitude values of active mru*/
    void readAttitude();
};
}  // namespace xsf
}  // end namespace sonarnative