#pragma once
#include <climits>

namespace sonarnative {
	namespace api {
		/* Parameters of Specific Reson value converter */
		struct ResonOffsetParameter {
			ResonOffsetParameter() = default;
			ResonOffsetParameter(float offset) : offset(offset) {}
			float offset = -42.f;
		};

		/* Parameters of Range normalization filter. Compute compensated values */
		struct RangeNormalizationParameter {
			RangeNormalizationParameter() = default;
			RangeNormalizationParameter(bool enable) : enable(enable) {}
			RangeNormalizationParameter(bool enable, float offset) : enable(enable), offset(offset) {}
			bool enable = false;
			// offset value in dB to use for normalization as reference level
			float offset = 0.0f;
		};
	}
}
