#pragma once
#include <climits>

namespace sonarnative {
namespace api {

/* Parameters of Sampling filter. Sub sample echoes, retains a value every */
struct SamplingParameter {
    SamplingParameter() = default;
    SamplingParameter(int threshold);
    int threshold = 1;
};

/* Parameters of Multiping filter. Sub sample pings, retains pings according their multiping index */
struct MultipingParameter {
    MultipingParameter() = default;
    MultipingParameter(bool enable, int index);
    bool enable = false;
    int index = 0;
};

/* Parameters of Threshold filter */
struct ThresholdParameter {
    ThresholdParameter() = default;
    ThresholdParameter(bool enable, float min, float max);
    bool enable = false;
    float min = -64.0f, max = 64.0f;
};

/* Parameters of Bottom filter */
struct BottomFilterParameter {
    enum class ToleranceType { sample, range_percent };
    // Constructs ToleranceType::sample parameters
    static BottomFilterParameter new_sample(double angle_coefficient, int toleranceAbsolute, bool enable = true);
    // Constructs ToleranceType::range_percent parameters
    static BottomFilterParameter new_range_percent(double angle_coefficient, double tolerance_percent, bool enable = true);
    bool enable = false;
    ToleranceType tolerance_type = ToleranceType::range_percent;
    int tolerance_absolute = 0;
    double tolerance_percent = 0.0, angle_coefficient = 0.0;
};

/* Parameters of Specular filter */
struct SpecularFilterParameter {
    SpecularFilterParameter() = default;
    SpecularFilterParameter(bool enable, bool below, int tolerance);
    bool enable = false;
    bool below = true;
    int tolerance = 3;
};

/* Parameters of Beam index filter */
struct BeamIndexParameter {
    BeamIndexParameter() = default;
    BeamIndexParameter(bool enable, int min, int max);
    bool enable = false;
    int min = 0, max = INT_MAX;  // All beams by default
};

/* Parameters of Sample index filter */
struct SampleIndexParameter {
    SampleIndexParameter() = default;
    SampleIndexParameter(bool enable, int min, int max);
    bool enable = false;
    int min = 0, max = INT_MAX;  // All ranks by default
};

/* Parameters of Depth filter */
struct DepthParameter {
    DepthParameter() = default;
    DepthParameter(bool enable, float min, float max);
    bool enable = false;
    float min = 0.0f, max = 10000.0f;
};

/* Parameters of across distance filter */
struct AcrossDistanceParameter {
    AcrossDistanceParameter() = default;
    AcrossDistanceParameter(bool enable, float min, float max);
    bool enable = false;
    float min = -5000.0f, max = 5000.0f;
};

/* Parameters of side lobe effects filter */
struct SideLobeParameter {
    SideLobeParameter() = default;
    SideLobeParameter(bool enable, float threshold);
    SideLobeParameter(bool enable, float threshold, float quantile);
    bool enable = false;
    float threshold = 20.0f;
    float quantile = 0.5f;
};
}  // namespace api
}  // namespace sonarnative
