#pragma once
#include <string>

#include "sonarnative/api/filters_api.hpp"
#include "sonarnative/api/signal_processing_api.hpp"
#include "sonarnative/sonarnative.hpp"
#include "sonarnative/spatialization/xsf_spatializer.hpp"

namespace sonarnative {
namespace api {

/* Pointer on a xsf::XsfSpatializer */
struct SpatializerHolder {
    SpatializerHolder(std::string xsf, int ncid);
    std::string last_error_msg;
    size_t get_swath_count();
    size_t get_beam_count();
    std::shared_ptr<xsf::XsfSpatializer> hold;
};

/* Creates a spatializer for the specified XSF file */
SpatializerHolder open_spatializer(std::string xsf_path, int ncid = -1, bool multi_thread = true) noexcept;

/* Close an opend SpatializerHolder */
void close_spatializer(SpatializerHolder& spatializer) noexcept;

/* Change the number of the Beam_group to use. */
void change_beam_group_number(SpatializerHolder& spatializer, int other_beam_group_number) noexcept;

/* Set the spatialization method to use. */
void set_spatialization_method(SpatializerHolder& spatializer, SpatializationMethod method) noexcept;

/* Close an opend SpatializerHolder */
void close_spatializer(SpatializerHolder& spatializer) noexcept;

/* Return the number of echo in the specified swath range */
int estimate_beam_echo_count(SpatializerHolder& spatializer, int from_swath, int swath_count) noexcept;

/* Return the estimated reference level in the specified swath range */
float estimate_normalization_ref_level(SpatializerHolder& spatializer, int from_swath, int swath_count) noexcept;

// All produced layers
enum LAYER_IDX {
    longitude,
    latitude,
    across,
    along,
    beam_opening_across,
    beam_opening_along,
    elevation,
    height,
    echo,
    non_overlapping_beam_opening_angle,
    COUNT
};
const int LAYER_COUNT = LAYER_IDX::COUNT;
const std::array<std::string, LAYER_IDX::COUNT> LAYER_NAME{
    "longitude",          "latitude",  "across", "along", "beam_opening_across",
    "beam_opening_along", "elevation", "height", "echo",  "non_overlapping_beam_opening_angle"};

/* Result of a call of spatialize_in_memory */
struct MemEchos {
    MemEchos();
    MemEchos(int capacity);
    ~MemEchos();
    int capacity;
    int size;
    double* longitude;
    double* latitude;
    float* across;
    float* along;
    float* beam_opening_across;
    float* beam_opening_along;
    float* elevation;
    float* height;
    float* echo;
    float* non_overlapping_beam_opening_angle;
};

/* Generates all the beam echoes belonging to the specified range of swath and beep them in memory */
void spatialize_in_memory(SpatializerHolder& spatializer, int from_swath, int swath_count, MemEchos& mem_echos) noexcept;

/* Result of a call of spatialize_all_on_disk */
class FileEchos {
    friend class FileBeamEchoCallback;

    int _size;
    std::array<std::string, LAYER_IDX::COUNT> _file_path;

   public:
    FileEchos(std::string folder);

    int size();
    void size(int size);
    std::string file_path(int layer_idx);
    std::string longitude();
    std::string latitude();
    std::string across();
    std::string along();
    std::string beam_opening_along();
    std::string beam_opening_across();
    std::string elevation();
    std::string height();
    std::string echo();
    std::string non_overlapping_beam_opening_angle();
};

/* Generates all the beam echoes belonging to the specified range of swath and write the result of disk*/
FileEchos spatialize_on_disk(SpatializerHolder& spatializer, int from_swath, int swath_count, std::string folder) noexcept;
/* Generates all the beam echoes and write the result of disk */
FileEchos spatialize_all_on_disk(SpatializerHolder& spatializer, std::string folder) noexcept;

/* Define a sampling filter for all futur spatialization calls. */
void apply_filter(SpatializerHolder& spatializer, const SamplingParameter& param) noexcept;
/* Define a threshold filter for all futur spatialization calls. */
void apply_filter(SpatializerHolder& spatializer, const ThresholdParameter& param) noexcept;
/* Define a bottom filter for all futur spatialization calls. */
void apply_filter(SpatializerHolder& spatializer, const BottomFilterParameter& param) noexcept;
/* Define a Specular filter for all futur spatialization calls. */
void apply_filter(SpatializerHolder& spatializer, const SpecularFilterParameter& param) noexcept;
/* Define a BeamIndex filter for all futur spatialization calls. */
void apply_filter(SpatializerHolder& spatializer, const BeamIndexParameter& param) noexcept;
/* Define a SampleIndex filter for all futur spatialization calls. */
void apply_filter(SpatializerHolder& spatializer, const SampleIndexParameter& param) noexcept;
/* Define a Multiping filter for all futur spatialization calls. */
void apply_filter(SpatializerHolder& spatializer, const MultipingParameter& param) noexcept;
/* Define a Depth filter for all futur spatialization calls. */
void apply_filter(SpatializerHolder& spatializer, const DepthParameter& param) noexcept;
/* Define an across distance filter for all futur spatialization calls. */
void apply_filter(SpatializerHolder& spatializer, const AcrossDistanceParameter& param) noexcept;
/* Define an side lobe effects filter for all futur spatialization calls. */
void apply_filter(SpatializerHolder& spatializer, const SideLobeParameter& param) noexcept;

/* Define an offset for reson files conversions for all futur spatialization calls. */
void apply_signal_processing(SpatializerHolder& spatializer, const ResonOffsetParameter& param) noexcept;
/* Define a Range normalization process for all futur spatialization calls. */
void apply_signal_processing(SpatializerHolder& spatializer, const RangeNormalizationParameter& param) noexcept;

}  // namespace api
}  // namespace sonarnative
