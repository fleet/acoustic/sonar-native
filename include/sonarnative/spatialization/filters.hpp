#pragma once

#include "sonarnative/api/filters_api.hpp"
#include "sonarnative/spatialization/spatializer.hpp"

namespace sonarnative {
namespace spatialization {

// Interface of all filters
class EchoFilter {
    /* Filter parameters */
    api::SamplingParameter sampling_parameter;
    api::BottomFilterParameter bottom_parameter;
    api::ThresholdParameter threshold_parameter;
    api::SpecularFilterParameter specular_parameter;
    api::BeamIndexParameter beam_index_parameter;
    api::SampleIndexParameter sample_index_parameter;
    api::DepthParameter depth_parameter;
    api::AcrossDistanceParameter across_distance_parameter;
    api::SideLobeParameter side_lobe_parameter;
    api::MultipingParameter multiping_parameter;
    
   public:
    /* Inspects an echo before calling spatialization and returns true if filter does not reject the it */
    bool inspects_echo(SwathData& swath_data, BeamData& beam_data, EchoData& echo) noexcept;
    /* Inspects an echo after calling spatialization and returns true if filter does not reject the it */
    bool inspects_localized_echo(EchoData& echo) noexcept;

    /* Return the sampling threshold */
    int get_sampling_threshold() noexcept;
    /* Return the beam index range */
    api::BeamIndexParameter get_beam_index_parameter() noexcept;
    /* Return the sample index range */
    api::SampleIndexParameter get_sample_index_parameter() noexcept;
    /* Return the multiping sequence index */
    api::MultipingParameter get_multiping_parameter() noexcept;

    void apply(api::SamplingParameter) noexcept;
    void apply(api::BottomFilterParameter) noexcept;
    void apply(api::ThresholdParameter) noexcept;
    void apply(api::SpecularFilterParameter) noexcept;
    void apply(api::BeamIndexParameter) noexcept;
    void apply(api::SampleIndexParameter) noexcept;
    void apply(api::DepthParameter) noexcept;
    void apply(api::AcrossDistanceParameter) noexcept;
    void apply(api::SideLobeParameter) noexcept;
    void apply(api::MultipingParameter) noexcept;

    api::SpecularFilterParameter get_specular_parameter() { return specular_parameter; }
    api::SideLobeParameter get_side_lobe_parameter() { return side_lobe_parameter; }

   protected:
    bool is_threshold_suitable(EchoData& echo) noexcept;
    bool is_bottom_suitable(SwathData& swath_data, BeamData& beam_data, EchoData& echo) noexcept;
    bool is_specular_suitable(SwathData& swath_data, EchoData& echo) noexcept;
    bool is_depth_suitable(EchoData& echo) noexcept;
    bool is_across_distance_suitable(EchoData& echo) noexcept;
    bool is_side_lobe_suitable(SwathData& swath_data, BeamData& beam_data, EchoData& echo) noexcept;
};

}  // namespace spatialization
}  // namespace sonarnative
