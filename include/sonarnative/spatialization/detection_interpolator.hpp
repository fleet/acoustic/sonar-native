#pragma once

#include <cstdlib>
#include <cmath>
#include <map>

#include "sonarnative/sonarnative.hpp"

namespace sonarnative {
	namespace spatialization {

		struct DetectionPoint
		{
			float along, across, depth;
			float travelTime;
		};

		/**
		* Interpolator defined as a map containing tan(beamPointingAngle) as Keys and DetectionPoint as Values			
		*/
		class DetectionInterpolator : public std::map<float, DetectionPoint>
		{
		public:
			DetectionInterpolator() = default;
			~DetectionInterpolator() = default;

			/**
			 * Does the interpolation. shouldcontain at least 2 elements
			 * Returns a struct containg normalized detection direction and the estimated travel time to reach 1 meter in this direction 
			 * @param beamAngleRad : extrinsic beam pointing angle relative to vertical plane in radians
			 */
			DetectionPoint getNormalizedInterpolatedValue(float beamAngleRad) {
				float tanBeamAngle = tan(beamAngleRad);
				auto upIt = lower_bound(tanBeamAngle); //first key grether or equal
				DetectionInterpolator::iterator previous;
				DetectionInterpolator::iterator next;

				if (upIt != end()) {
					if (upIt->first == tanBeamAngle) { //exact match
						auto& value = upIt->second;
						float rangeNorm = sqrt(value.across * value.across + value.along * value.along + value.depth* value.depth);
						DetectionPoint point;
						point.across = value.across / rangeNorm;
						point.along = value.along / rangeNorm;
						point.depth = value.depth / rangeNorm;
						point.travelTime = value.travelTime / rangeNorm;
						return point;
					}
					else if (upIt == begin()) { // lower than first element
						previous = begin();
						next = std::next(previous);
					}
					else { //between two elements
						next = upIt;
						previous = std::prev(next);
					}
				}
				else { //greather than last element
					next = std::prev(end());
					previous = std::prev(next);
				}

				// Compute interpolated values on a horizontal plane placed at 1 meter depth 

				// first computes interpolation coefficient using tangeant values of pointing angles
				float coeff = (tanBeamAngle - previous->first) / (next->first - previous->first);

				// computes values of reference detections projected on the 1 meter depth reference plane 
				const auto& previousValue = previous->second;
				const auto& nextValue = next->second;

				float prev_across_proj = previousValue.across / previousValue.depth;
				float next_across_proj = nextValue.across / nextValue.depth;

				float prev_along_proj = previousValue.along / previousValue.depth;
				float next_along_proj = nextValue.along / nextValue.depth;

				float prev_range_proj = sqrt(prev_across_proj*prev_across_proj + prev_along_proj*prev_along_proj + 1.f);
				float next_range_proj = sqrt(next_across_proj*next_across_proj + next_along_proj*next_along_proj + 1.f);

				float prev_travelTime_proj = previousValue.travelTime / previousValue.depth;
				float next_travelTime_proj = nextValue.travelTime / nextValue.depth;

				// linear interpolation on reference plane 
				float across_proj = prev_across_proj + coeff * (next_across_proj - prev_across_proj);
				float along_proj = prev_along_proj + coeff * (next_along_proj - prev_along_proj);
				float range_proj = sqrt(across_proj*across_proj + along_proj*along_proj + 1.f);

				float coeffRange = (next_range_proj == prev_range_proj) ? 1.f : (range_proj - prev_range_proj) / (next_range_proj - prev_range_proj);
				float travelTime_proj = prev_travelTime_proj + coeffRange * (next_travelTime_proj - prev_travelTime_proj);

				// normalization 
				DetectionPoint point;
				point.across = across_proj / range_proj;
				point.along = along_proj / range_proj;
				point.depth = 1.f / range_proj;
				point.travelTime = travelTime_proj / range_proj;
				return point;
			}

/**
			 * Does a simple interpolation. extrapolation using closest value.
			 * Returns a struct containg interpolated detection and the estimated travel time
			 * @param beamAngleRad : extrinsic beam pointing angle in radians
			 */
			DetectionPoint getInterpolatedValue(float beamAngleRad) {
				float tanBeamAngle = tan(beamAngleRad);
				auto upIt = lower_bound(tanBeamAngle); //first key greather or equal
				DetectionInterpolator::iterator previous;
				DetectionInterpolator::iterator next;

				if (upIt != end()) {
					if (upIt->first == tanBeamAngle) { //exact match
						return upIt->second;
					} else if (upIt == begin()) { // lower than first element
						return upIt->second;
					} else { //between two elements
						next = upIt;
						previous = std::prev(next);
					}
				} else { //greather than last element
					next = std::prev(end());
					return next->second;
				}

				// first computes interpolation coefficient using tangeant values of pointing angles
				float coeff = (tanBeamAngle - previous->first) / (next->first - previous->first);

				// computes values of reference detections projected on the 1 meter depth reference plane 
				const auto& previousValue = previous->second;
				const auto& nextValue = next->second;

				// linear interpolation on reference plane 
				DetectionPoint point;
				point.across = previousValue.across + coeff * (nextValue.across - previousValue.across);
				point.along = previousValue.along + coeff * (nextValue.along - previousValue.along);
				point.depth = previousValue.depth + coeff * (nextValue.depth - previousValue.depth);

				float range = sqrt(point.across*point.across + point.along*point.along + point.depth*point.depth);
				float prev_range = sqrt(previousValue.across*previousValue.across + previousValue.along*previousValue.along + previousValue.depth*previousValue.depth);
				float next_range = sqrt(nextValue.across*nextValue.across + nextValue.along*nextValue.along + nextValue.depth*nextValue.depth);
				float coeffRange = (next_range == prev_range) ? 1.f : (range - prev_range) / (next_range - prev_range);

				point.travelTime = previousValue.travelTime + coeffRange * (nextValue.travelTime - previousValue.travelTime);

				return point;
			}
		};

	}
} // end namespace sonarnative