#pragma once

#include <netcdf.h>

#include "sonarnative/driver/xsf_driver.hpp"

namespace sonarnative {
namespace xsf {

// Structure to hold all needed XSF data.
struct XsfData {
    // variable acquired for all swath
    double* platform_longitude = nullptr;
    double* platform_latitude = nullptr;
    float* platform_heading = nullptr;
    float* platform_pitch = nullptr;
    float* platform_roll = nullptr;
    uint64_t* ping_time = nullptr;
    float* sample_interval = nullptr;
    float* sound_speed_at_transducer = nullptr;
    float* tx_transducer_depth = nullptr;
    float* transducer_offset_x = nullptr;
    float* transducer_offset_y = nullptr;
    float* transducer_offset_z = nullptr;
    float* transducer_rotation_x = nullptr;
    float* transducer_rotation_y = nullptr;
    float* transducer_rotation_z = nullptr;
    float* attitude_roll = nullptr;
    float* attitude_pitch = nullptr;
    uint64_t* attitude_time = nullptr;
    int8_t* detection_beam_stabilisation = nullptr;
    unsigned char* multiping_sequence = nullptr;

    // receive beam angular rotation about the _x_ axis (arc_degree)
    float* rx_beam_rotation_phi = nullptr;
    // transmit beam angular rotation about the _y_ axis (arc_degree)
    float* tx_beam_rotation_theta = nullptr;
    // Receive transducer index associated with the given rx beam
    int* receive_transducer_index = nullptr;
    // Transmit transducer index associated with the given tx beam
    int* transmit_transducer_index = nullptr;
    // Transmit beam index associated with the given rx beam
    int* transmit_beam_index = nullptr;
    // invalidity flag for soundings data (valid when value == 0)
    unsigned char* status = nullptr;
    // Index of the transmit beam associated with the detection
    short* detection_tx_beam = nullptr;
    // Index of the rx transducer associated with the detection
    short* detection_rx_transducer_index = nullptr;
    // Index of the tx transducer associated with the detection
    short* detection_tx_transducer_index = nullptr;
    // Distance from vessel reference point at time of first tx pulse in ping, to depth point.
    float* detection_x = nullptr;
    // Distance from vessel reference point at time of first tx pulse in ping, to depth point.
    float* detection_y = nullptr;
    // Distance from vessel reference point at time of first tx pulse in ping, to depth point.
    float* detection_z = nullptr;
    // detection synthetic beam pointing angle relative to Rx reference plane.
    float* detection_beam_pointing_angle = nullptr;
    // Half power one-way receive beam width along major (horizontal) axis of beamHorizontal direction matched across ship direction in MBES case
    // (arc_degree)
    float* beamwidth_receive_major = nullptr;
    // Half power one-way transmit beam width along minor (vertical) axis of beamvertical direction matched along ship direction in MBES case
    // (arc_degree)
    float* beamwidth_transmit_minor = nullptr;
    // Detected range of the bottom (m)
    float* detected_bottom_range = nullptr;
    // Detection travel time
    float* detection_two_way_travel_time = nullptr;
    // Amount of time during reception where samples are discarded (s)
    float* blanking_interval = nullptr;
    // Time offset that is subtracted from the timestamp of each sample (s)
    float* sample_time_offset = nullptr;

    // Backscatter
    std::vector<nc_vlen_t>* swath_backscatter_r;
    float backscatter_r_scale;
    float backscatter_r_offset;
    nc_type backscatter_r_type;
    short backscatter_r_missing_value;

    int default_rx_transducer_index = 0;
    int default_tx_transducer_index = 1;

    /* Prepare the process by loading needed data of a swath */
    void load(XsfDriver& xsf_driver, int swath);
};
}  // namespace xsf
}  // end namespace sonarnative