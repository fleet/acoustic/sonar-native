#pragma once

#include <stdint.h>

#include <cmath>
#include <cstdlib>
#include <map>
#include <vector>

#include "detection_interpolator.hpp"
#include "sonarnative/sonarnative.hpp"

namespace sonarnative {
namespace spatialization {

// Structure to hold all values of one swath.
struct SwathData {
    // Swath index
    int swath = 0;
    // Platform longitude
    double platform_longitude = 0.0;
    // Platform latitude
    double platform_latitude = 0.0;
    // Platform heading
    float platform_heading = 0.0f;
    // Platform roll
    float platform_roll = 0.0f;
    // Platform pitch
    float platform_pitch = 0.0f;
    // Ping time
    uint64_t ping_time = 0;
    // Interval between recorded raw data samples (s)
    float sample_interval = 0.0f;
    // Indicative sound speed at ping time and transducer depth (m/s)
    float sound_speed_at_transducer = 0.0f;
    // Tx transducer depth below waterline
    float tx_transducer_depth = 0.0f;
    // Detection beam is stabalized (ME70 case)
    bool detectionBeamStabilized = false;

    // Range of the sphere, ie min value of the bottom ranges. Computed for the sphere filters
    int sphere_range = 0;
    // Median value of backscatter_r per rank per sector. Computed for the side lobe filter.
    size_t sectorCount = 1;
    std::vector<float> sidelobe_ref_backscatter_per_rank_per_sector;
    float sidelobe_ref_quantile = 0.5f;

    std::vector<float> normalization_ref_backscatter_per_rank_per_sector;
    float normalization_ref_backscatter = 0.f;
    float normalization_ref_quantile = 0.5f;

    // Equivalent tilt angle per tx sector
    std::vector<double> equivalentTiltAngleRadPerSector;
    // Equivalent celerity over surface ratio per tx sector
    double equivalentCelerityRatio;
    // detection interpolator per tx sector per antenna
    std::vector<DetectionInterpolator> detectionsPerSectorPerAntenna;
    size_t antennaCount = 0;
};

// Structure to hold all values of one beam.
struct BeamData {
    int beam = 0;

    // Detected range of the bottom
    int bottomRangeIndex = 0;
    //// Amount of time during reception where samples are discarded
    // float blanking = 0.0f;
    //// Time offset that is subtracted from the timestamp of each sample
    // float time_offset = 0.0f;
    //  Receive beam angular rotation about the _x_ axis
    float rx_beam_rotation_phi = 0.0f;
    float non_overlapping_opening_angle = 0.0f;
    // Receive transducer index associated with the given beam
    int rx_transducer = -1;
    // Transmit transducer index associated with the given tx beam
    int tx_transducer = -1;
    // Transmit beam index associated with the given beam
    int tx_sector = -1;
    float rx_transducer_offset_x = 0.0f;
    float rx_transducer_offset_y = 0.0f;
    float rx_transducer_offset_z = 0.0f;
    float tx_transducer_offset_z = 0.0f;
    float tx_transducer_rotation_z = 0.0f;
    float beam_opening_across = 0.0f;
    float beam_opening_along = 0.0f;
    double equivalentTiltAngleRad = 0.0;
    double equivalentBeamPointingAngleRad = 0.0;
    float equivalentCelerity = 0.0f;
};

// Structure to hold all values of one echo.
struct EchoData {
    // Rank of the echo in the beam
    int rank = 0;
    // Values computed by the spatializer
    double longitude = 0.0;
    double latitude = 0.0;
    float beam_opening_across = 0.0f;
    float beam_opening_along = 0.0f;
    float non_overlapping_beam_opening_angle = 0.0f;
    float across = 0.0f;
    float along = 0.0f;
    float height = 0.0f;
    float elevation = 0.0f;
    // Value of the echo (backscatter_r_value when XSF)
    float rawValue = 0.0f;
    float value = 0.0f;
};

/*
Utility class to compute spatialization data.
Call sequentially :
        - configure_swath_level to set parameters at the swath level
        - configure_beam_level to set parameters at the beam level
        - spatialize on each Echo to spatialize.
*/
class Spatializer {
    SpatializationMethod method;
    SwathData swath_data;
    BeamData beam_data;

    // Pre-computed data when receiving a new SwathData
    // Computed once for all Beams and Echoes
    double cos_platform_latitude = 0.0;
    double sin_platform_latitude = 0.0;
    double cos_platform_heading = 0.0;
    double sin_platform_heading = 0.0;
    double cos_heading_div_radius = 0.0;
    double sin_heading_div_radius = 0.0;
    double cos_heading_div_norm = 0.0;
    double sin_heading_div_norm = 0.0;
    double samplerate = 0.0;

    // Pre-computed data when receiving a new BeamData
    // Computed once for all Echoes
    double beamRange = 0.0;
    double sinEquivalentPointingAngle = 0.0;
    double cosEquivalentPointingAngle = 1.0;
    double cosEquivalentTiltAngle = 1.0;
    double tanEquivalentTiltAngle = 0.0;
    double cosHeadingAngle = 1.0;
    double tanHeadingAngle = 0.0;

    // bathy placement
    double across_norm = 0.0;
    double along_norm = 0.0;
    double depth_norm = 0.0;
    bool hasDetection = false;

   public:
    /* Constructor / Destructor */
    Spatializer(SpatializationMethod method) : method(method){};
    ~Spatializer() = default;

    // Initialize the spatializer with the swath's values
    void configure_swath_level(SwathData &);
    // Initialize the spatializer with the beam's values
    void configure_beam_level(BeamData &);
    // spatialize the echo specified by its rank
    void spatialize(EchoData &);
};
}  // namespace spatialization
}  // end namespace sonarnative