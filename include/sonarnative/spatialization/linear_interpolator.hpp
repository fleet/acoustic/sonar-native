#pragma once

#include <cstdlib>
#include <cmath>
#include <span>

namespace sonarnative {
	namespace spatialization {

		/**
		* Interpolator defined as a spans containing Keys and Values
		*/
		template <class K, class V>
		class LinearInterpolator
		{
		public:
			LinearInterpolator(K* keys, V* values, size_t size) {
				this->keys = std::span(keys, size);
				this->values = std::span(values, size);
			}

			~LinearInterpolator() = default;

			/**
			 * Does the interpolation. shouldcontain at least 2 elements
			 * Returns a the linear interpolation at the requested key value
			 * @param key : key to retrieve value for
			 */
			V getInterpolatedValue(K key) {
				auto upIt = std::lower_bound(keys.begin(), keys.end(), key); //first key grether or equal
				typename std::span<K>::iterator previous;
				typename std::span<K>::iterator next;

				if (upIt != keys.end()) {
					if (*upIt == key) { //exact match
						size_t pos = std::distance(keys.begin(), upIt);
						return values[pos];
					}
					else if (upIt == keys.begin()) { // lower than first element
						return values.front();
						//previous = keys.begin();
						//next = std::next(previous);
					}
					else { //between two elements
						next = upIt;
						previous = std::prev(next);
					}
				}
				else { //greather than last element
					return values.back();
					//next = std::prev(keys.end());
					//previous = std::prev(next);
				}

				// Compute interpolated values

				// first computes interpolation coefficient
				double coeff = 1.0*(key - *previous) / (*next - *previous);

				// retrieve values of reference keys
				size_t prev_pos = std::distance(keys.begin(), previous);
				size_t next_pos = std::distance(keys.begin(), next);

				const auto& previousValue = values[prev_pos];
				const auto& nextValue = values[next_pos];

				// linear interpolation 
				V value = static_cast<V>(previousValue + coeff * (nextValue - previousValue));

				return value;
			};

		protected:
			std::span<K> keys;
			std::span<V> values;
		};
	}
} // end namespace sonarnative