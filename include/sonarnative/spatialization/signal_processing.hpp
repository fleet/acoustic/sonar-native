#pragma once

#include "sonarnative/api/signal_processing_api.hpp"
#include "sonarnative/spatialization/spatializer.hpp" 

namespace sonarnative {
	namespace spatialization {

		// Interface of all filters
		class SignalProcessing {

			/* image processing parameters */
			api::ResonOffsetParameter reson_offset_parameter;
			api::RangeNormalizationParameter range_normalization_parameter;

		public:
			/* Return the sampling threshold */
			void apply(api::ResonOffsetParameter) noexcept;
			void apply(api::RangeNormalizationParameter) noexcept;

			const api::ResonOffsetParameter& get_reson_offset_parameter() { return reson_offset_parameter; }
			const api::RangeNormalizationParameter& get_range_normalization_parameter() { return range_normalization_parameter; }

			void process_echo(SwathData& swath_data, BeamData& beam_data, EchoData& echo);
		};
	}
}
