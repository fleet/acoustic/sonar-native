#pragma once

#include <string>
#include <functional>
#include <array>
#include <vector>
#include <map>

#include "sonarnative/driver/xsf_driver.hpp"
#include "sonarnative/spatialization/filters.hpp" 
#include "sonarnative/spatialization/signal_processing.hpp" 
#include "sonarnative/spatialization/xsf_swath_data.hpp" 

namespace sonarnative {
	namespace xsf {

		/* Max nb of BeamEcho when calling a BeamEchoCallback */
		static const size_t BEAM_ECHO_PER_CALLBACK = 2048;
		// Structure to hold data of a produced points  
		struct BeamEchos
		{
			size_t size = 0;
			std::array<double, BEAM_ECHO_PER_CALLBACK> longitude, latitude;
			std::array<float, BEAM_ECHO_PER_CALLBACK> across, along, beam_opening_across, beam_opening_along, elevation, height, echo, non_overlapping_beam_opening_angle;
		};

		using BeamEchoCallback = std::function<void(BeamEchos&)>;

		class XsfSpatializer
		{
			XsfDriver xsf_driver;

			spatialization::EchoFilter echo_filter;
			spatialization::SignalProcessing signal_processing;
			bool multi_thread = true;
			SpatializationMethod method = SpatializationMethod::DetectionInterpolation;

		public:
			/* Constructor / Destructor */
			XsfSpatializer(std::string xsf_path, int ncid=-1);
			XsfSpatializer(XsfSpatializer&& to_move) noexcept;
			~XsfSpatializer();

			/* Open the XSF file */
			void initialize();

			/* Return the number of echo in the specified swath range */
			size_t estimate_beam_echo_count(size_t from_swath, size_t swath_count);
            /* Return the computed normalization ref level */
            float estimate_normalization_ref_level(size_t from_swath, size_t swath_count);
            /* Change the number of the Beam_group to use. Return true on sucess */
			void change_beam_group_number(int other_beam_group_number);
			/* Generate all the BeamEcho of all swaths */
			void perform_for_swaths(int from_swath, int swath_count, BeamEchoCallback &callback);
			/* Generate all the BeamEcho of all swaths */
			void perform_all_swath(BeamEchoCallback &callback);

			/* Close the XSF file */
			void close();

			/* XSF infos */
			size_t get_swath_count();
			size_t get_beam_count();
			size_t get_detection_count();
			size_t get_sector_count();
			size_t get_transducer_count();
			size_t get_attitude_count();
			spatialization::EchoFilter& get_filter();
			spatialization::SignalProcessing& get_signal_processing();

			void set_multi_thread(bool multi_thread);
			void set_spatialization_method(SpatializationMethod method);

		private:
			// Compute swath data to guess beam positions (equivalent tilt angle / equivalent celerity / detection positions ...)
			void compute_swath_data(XsfData& xsf_data, spatialization::SwathData& swath_data);
			// 
			float compute_two_way_travel_time(const XsfData& xsf_data, const spatialization::SwathData& swath_data, const int beam);
			//
			int compute_bottom_range_index(const XsfData& xsf_data, const spatialization::SwathData& swath_data, const float twoWayTravelTime, const int beam);
			
			int guess_bottom_range_index(XsfData& xsf_data, int beam);
			// Fills the values of the object SwathData
			void init_swath_data(XsfData& xsf_data, int swath, spatialization::SwathData& swath_data);
			// Fills the values of the object BeamData
			void init_beam_data(XsfData& xsf_data, int beam, spatialization::SwathData& swath_data, spatialization::BeamData& beam_data);

			// Computes the value of the swath_data's attribute sphere_range (in case of threshold sphere filtering)
			void compute_sphere_range(const XsfData& xsf_data, spatialization::SwathData& swath_data);
			// Computes the value of the swath_data's attribute median_backscatter_per_rank (in case of side lobe filtering)
			void compute_median_backscatter_per_rank(XsfData& xsf_data, spatialization::SwathData& swath_data, bool force = false);
			// Converts Reson backscatter from amplitude to dB
			float convertTodB(float value);

			/* Generate all the BeamEcho of prepared xsf data */
			void perform(int swath, XsfData& xsf_data, BeamEchoCallback& callback);
			/* Generate all the BeamEcho of prepared xsf data */
			void _perform(int swath, XsfData& xsf_data, spatialization::SwathData& swath_data, int first_beam, int last_beam, BeamEchoCallback& callback);
		};
	}
} // end namespace sonarnative