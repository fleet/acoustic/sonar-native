/**
 * SonarNative - Ifremer
 */
package fr.ifremer.sonarnative;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

/**
 * Utility class to load sonarnative-jni library
 */
public class SonarNativeLibLoader {

	/**
	 * Constructor
	 */
	private SonarNativeLibLoader() {
	}

	/** True when native library has been loaded */
	private static boolean available = false;

	/** Invokes System.loadLibrary */
	public static void loadNativeLibrary() {
		if (!available) {
			try {
				System.out.println("Loading native library sonarnative-jni");
				System.loadLibrary("sonarnative-jni");
				available = true;
			} catch (UnsatisfiedLinkError e) {
				System.err.println("Native library load failed : " + e.getMessage());
			}
		}

	}

	/**
	 * @return the {@link #available}
	 */
	public static boolean isAvailable() {
		return available;
	}
}
