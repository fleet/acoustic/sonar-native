/**
 * SonarNative - Ifremer
 */
package fr.ifremer.sonarnative;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;

import fr.ifremer.sonarnative.jni.MemEchos;

/**
 * Wrapper of MemEchos. Usefull to maintain the references on buffer
 */
public class SonarNativeEchoBuffers {

	public final DoubleBuffer longitudes;
	public final DoubleBuffer latitudes;
	public final FloatBuffer acrosses;
	public final FloatBuffer alongs;
	public final FloatBuffer beamOpeningAcrosses;
	public final FloatBuffer beamOpeningAlongs;
	public final FloatBuffer elevations;
	public final FloatBuffer heights;
	public final FloatBuffer echoes;
	public final FloatBuffer nonOverlappingBeamOpeningAngles;

	public final MemEchos memEchos;

	/**
	 * Constructor
	 */
	public SonarNativeEchoBuffers(int capacity) throws SonarNativeException {
		if (capacity <= 0) {
			throw new SonarNativeException("Capacity of a buffer must be > 0");
		}

		try {
			longitudes = ByteBuffer.allocateDirect(Double.BYTES * capacity).order(ByteOrder.nativeOrder())
					.asDoubleBuffer();
			latitudes = ByteBuffer.allocateDirect(Double.BYTES * capacity).order(ByteOrder.nativeOrder())
					.asDoubleBuffer();
			acrosses = ByteBuffer.allocateDirect(Float.BYTES * capacity).order(ByteOrder.nativeOrder()).asFloatBuffer();
			alongs = ByteBuffer.allocateDirect(Float.BYTES * capacity).order(ByteOrder.nativeOrder()).asFloatBuffer();
			beamOpeningAcrosses = ByteBuffer.allocateDirect(Float.BYTES * capacity).order(ByteOrder.nativeOrder())
					.asFloatBuffer();
			beamOpeningAlongs = ByteBuffer.allocateDirect(Float.BYTES * capacity).order(ByteOrder.nativeOrder()).asFloatBuffer();
			elevations = ByteBuffer.allocateDirect(Float.BYTES * capacity).order(ByteOrder.nativeOrder())
					.asFloatBuffer();
			heights = ByteBuffer.allocateDirect(Float.BYTES * capacity).order(ByteOrder.nativeOrder()).asFloatBuffer();
			echoes = ByteBuffer.allocateDirect(Float.BYTES * capacity).order(ByteOrder.nativeOrder()).asFloatBuffer();
			nonOverlappingBeamOpeningAngles = ByteBuffer.allocateDirect(Float.BYTES * capacity).order(ByteOrder.nativeOrder())
					.asFloatBuffer();

			memEchos = MemEchos.getInstance();
			memEchos.setCapacity(capacity);
			memEchos.setLongitude(longitudes);
			memEchos.setLatitude(latitudes);
			memEchos.setAcross(acrosses);
			memEchos.setAlong(alongs);
			memEchos.setBeam_opening_across(beamOpeningAcrosses);
			memEchos.setBeam_opening_along(beamOpeningAlongs);
			memEchos.setElevation(elevations);
			memEchos.setHeight(heights);
			memEchos.setEcho(echoes);
			memEchos.setNon_overlapping_beam_opening_angle(nonOverlappingBeamOpeningAngles);
		} catch (OutOfMemoryError e) {
			throw new SonarNativeException(e.getMessage());
		}
	}

	/** Return the capacity of the buffers */
	public int getCapacity() {
		return memEchos.getCapacity();
	}

	/** Return the number of echos after a spatialization */
	public int getEchoCount() {
		return memEchos.getSize();
	}
}
