/* 
 * double *DOUBLE_BUFFER typemaps. 
 * This is for mapping Java nio double buffers to C char arrays.
 * It is useful for performance critical code as it reduces the memory copy an marshaling overhead.
 * Note: The Java buffer has to be allocated with allocateDirect.
 *
 * Example usage wrapping:
 *   %apply double *DOUBLE_BUFFER { double *buf };
 *   void foo(double *buf);
 *  
 * Java usage:
 *   java.nio.ByteBuffer b = ByteBuffer.allocateDirect(20); 
 *   modulename.foo(b);
 */

%typemap(jni) double *DOUBLE_BUFFER "jobject"  
%typemap(jtype) double *DOUBLE_BUFFER "java.nio.DoubleBuffer"  
%typemap(jstype) double *DOUBLE_BUFFER "java.nio.DoubleBuffer"  
%typemap(javain,
  pre="  assert $javainput.isDirect() : \"Buffer must be allocated direct.\";") double *DOUBLE_BUFFER "$javainput"
%typemap(javaout) double *DOUBLE_BUFFER {  
  return $jnicall;  
}  
%typemap(in) double *DOUBLE_BUFFER {  
  $1 = (double *) JCALL1(GetDirectBufferAddress, jenv, $input); 
  if ($1 == NULL) {  
    SWIG_JavaThrowException(jenv, SWIG_JavaRuntimeException, "Unable to get address of a java.nio.ByteBuffer direct byte buffer. Buffer must be a direct buffer and not a non-direct buffer.");  
  }  
}  
%typemap(memberin) double *DOUBLE_BUFFER {  
  if ($input) {  
    $1 = $input;  
  } else {  
    $1 = 0;  
  }  
}  
%typemap(freearg) double *DOUBLE_BUFFER ""  


/* 
 * float *FLOAT_BUFFER typemaps. 
 * This is for mapping Java nio double buffers to C char arrays.
 * It is useful for performance critical code as it reduces the memory copy an marshaling overhead.
 * Note: The Java buffer has to be allocated with allocateDirect.
 *
 * Example usage wrapping:
 *   %apply float *FLOAT_BUFFER { float *buf };
 *   void foo(float *buf);
 *  
 * Java usage:
 *   java.nio.ByteBuffer b = ByteBuffer.allocateDirect(20); 
 *   modulename.foo(b);
 */

%typemap(jni) float *FLOAT_BUFFER "jobject"  
%typemap(jtype) float *FLOAT_BUFFER "java.nio.FloatBuffer"  
%typemap(jstype) float *FLOAT_BUFFER "java.nio.FloatBuffer"  
%typemap(javain,
  pre="  assert $javainput.isDirect() : \"Buffer must be allocated direct.\";") float *FLOAT_BUFFER "$javainput"
%typemap(javaout) float *FLOAT_BUFFER {  
  return $jnicall;  
}  
%typemap(in) float *FLOAT_BUFFER {  
  $1 = (float *) JCALL1(GetDirectBufferAddress, jenv, $input); 
  if ($1 == NULL) {  
    SWIG_JavaThrowException(jenv, SWIG_JavaRuntimeException, "Unable to get address of a java.nio.ByteBuffer direct byte buffer. Buffer must be a direct buffer and not a non-direct buffer.");  
  }  
}  
%typemap(memberin) float *FLOAT_BUFFER {  
  if ($input) {  
    $1 = $input;  
  } else {  
    $1 = 0;  
  }  
}  
%typemap(freearg) float *FLOAT_BUFFER ""  

