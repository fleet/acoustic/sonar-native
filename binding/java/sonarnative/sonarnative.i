
%module(directors="1") SonarNative

%include "std_string.i" // Use for std::string mapping to Java String
%include "typemaps.i" // Allows values to be used for C functions taking pointers for data input
%include "std_vector.i"
%include "typemap_buffer.i" // Bytebuffer


%ignore SonarNative;

%{
#include "sonarnative/sonarnative.hpp"
#include "sonarnative/version.hpp"
#include "sonarnative/utils/progress_callback.hpp"
#include "sonarnative/log/log.hpp"
#include "sonarnative/log/proxy.hpp"
#include "sonarnative/api/xsf_api.hpp"
#include "sonarnative/api/filters_api.hpp"
#include "sonarnative/api/signal_processing_api.hpp"

using namespace std;
using namespace sonarnative;
using namespace sonarnative::api;
using namespace sonarnative::xsf;
%}

%include "sonarnative/sonarnative.hpp"
%include "sonarnative/version.hpp"

%feature("director") sonarnative::ProgressCallback;      
%include "sonarnative/utils/progress_callback.hpp"


%apply double *DOUBLE_BUFFER { double *longitude, double *latitude };
%apply float *FLOAT_BUFFER { float *across, float *along, float *beam_opening_across, float *beam_opening_along, float *elevation, float *height, float *echo, float *non_overlapping_beam_opening_angle };

%ignore sonarnative::api::LAYER_COUNT;
%ignore sonarnative::api::LAYER_IDX;
%ignore sonarnative::api::LAYER_NAME;
%ignore sonarnative::api::SpatializerHolder::SpatializerHolder;
%ignore sonarnative::api::SpatializerHolder::hold;
%rename(LastErrorMsg) sonarnative::api::SpatializerHolder::last_error_msg; 

// Add in pure Java code proxy constructor
%typemap(javacode) sonarnative::api::MemEchos %{
    /** Creates the proxy which initially does not create nor own any C memory */
    public static MemEchos getInstance() {
        return new MemEchos(SonarNativeJNI.new_MemEchos__SWIG_0(), false);
    }
%}

%include "sonarnative/api/signal_processing_api.hpp"
%include "sonarnative/api/filters_api.hpp"
%include "sonarnative/api/xsf_api.hpp"

%include "sonarnative/log/log.hpp"
// generate directors for all virtual methods in class LogProxy
%feature("director") sonarnative::LogProxy;      
%include "sonarnative/log/proxy.hpp"
