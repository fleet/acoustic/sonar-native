/**
 * SonarNative - Ifremer
 */
package fr.ifremer.sonarnative;

import fr.ifremer.sonarnative.jni.AcrossDistanceParameter;
import fr.ifremer.sonarnative.jni.BeamIndexParameter;
import fr.ifremer.sonarnative.jni.BottomFilterParameter;
import fr.ifremer.sonarnative.jni.DepthParameter;
import fr.ifremer.sonarnative.jni.FileEchos;
import fr.ifremer.sonarnative.jni.SampleIndexParameter;
import fr.ifremer.sonarnative.jni.SamplingParameter;
import fr.ifremer.sonarnative.jni.MultipingParameter;
import fr.ifremer.sonarnative.jni.SideLobeParameter;
import fr.ifremer.sonarnative.jni.SpatializerHolder;
import fr.ifremer.sonarnative.jni.SpecularFilterParameter;
import fr.ifremer.sonarnative.jni.ThresholdParameter;
import fr.ifremer.sonarnative.jni.SpatializationMethod;
import fr.ifremer.sonarnative.jni.ResonOffsetParameter;
import fr.ifremer.sonarnative.jni.RangeNormalizationParameter;


/**
 *
 */
public class SonarNative {

	/**
	 * Constructor
	 */
	private SonarNative() {
	}

	/* Creates a spatializer for the specified Sonar Netcdf file */
	public static SpatializerHolder openSpatializer(String filePath, int ncid, boolean multiThread) throws SonarNativeException {
		var result = fr.ifremer.sonarnative.jni.SonarNative.open_spatializer(filePath, ncid, multiThread);
		if (!result.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(result.getLastErrorMsg());
		}
		return result;
	}

	/** Creates a spatializer for the specified Sonar Netcdf file, in a multi thread mode */
	public static SpatializerHolder openSpatializer(String filePath, int ncid) throws SonarNativeException {
		var result = fr.ifremer.sonarnative.jni.SonarNative.open_spatializer(filePath, ncid);
		if (!result.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(result.getLastErrorMsg());
		}
		return result;
	}

	/** Close an opened spatializer */
	public static void closeSpatializer(SpatializerHolder spatializer) {
		fr.ifremer.sonarnative.jni.SonarNative.close_spatializer(spatializer);
	}

	/** Change the number of the Beam_group to use. */
	public static void changeBeamGroupNumber(SpatializerHolder spatializer, int beamGroupNumber)
			throws SonarNativeException {
		fr.ifremer.sonarnative.jni.SonarNative.change_beam_group_number(spatializer, beamGroupNumber);
		if (!spatializer.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(spatializer.getLastErrorMsg());
		}
	}

	/** Return the number of echo in the specified swath range */
	public static int estimateBeamEchoCount(SpatializerHolder spatializer, int fromSwath, int swathCount)
			throws SonarNativeException {
		int result = fr.ifremer.sonarnative.jni.SonarNative.estimate_beam_echo_count(spatializer, fromSwath,
				swathCount);
		if (!spatializer.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(spatializer.getLastErrorMsg());
		}
		return result;
	}

	/** Return the number of echo in the specified swath range */
	public static float estimateNormalizationRefLevel(SpatializerHolder spatializer, int fromSwath, int swathCount)
			throws SonarNativeException {
		float result = fr.ifremer.sonarnative.jni.SonarNative.estimate_normalization_ref_level(spatializer, fromSwath,
				swathCount);
		if (!spatializer.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(spatializer.getLastErrorMsg());
		}
		return result;
	}

	/** Return the number of echo in the specified swath range */
	public static void setSpatializationMethod(SpatializerHolder spatializer, SpatializationMethod method)
			throws SonarNativeException {
		fr.ifremer.sonarnative.jni.SonarNative.set_spatialization_method(spatializer, method);
		if (!spatializer.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(spatializer.getLastErrorMsg());
		}
	}

	/** Generates all the beam echoes belonging to the specified range of swath and beep them in memory */
	public static void spatialize(SpatializerHolder spatializer, int fromSwath, int swathCount,
			SonarNativeEchoBuffers buffers) throws SonarNativeException {
		if (buffers.getCapacity() > 0) {
			fr.ifremer.sonarnative.jni.SonarNative.spatialize_in_memory(spatializer, fromSwath, swathCount,
					buffers.memEchos);
			if (!spatializer.getLastErrorMsg().isEmpty()) {
				throw new SonarNativeException(spatializer.getLastErrorMsg());
			}
		} else {
			throw new SonarNativeException("Can not perform a spatialization in an empty buffer");
		}
	}

	/** Generates all the beam echoes belonging to the specified range of swath and beep them in memory */
	public static FileEchos spatializeOnDisk(SpatializerHolder spatializer, String folder) throws SonarNativeException {
		var result = fr.ifremer.sonarnative.jni.SonarNative.spatialize_all_on_disk(spatializer, folder);
		if (!spatializer.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(spatializer.getLastErrorMsg());
		}
		return result;
	}

	/** Define a filter for all futur spatialization calls. */
	public static void applyFilter(SpatializerHolder spatializer, SamplingParameter param) throws SonarNativeException {
		fr.ifremer.sonarnative.jni.SonarNative.apply_filter(spatializer, param);
		if (!spatializer.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(spatializer.getLastErrorMsg());
		}
	}

	/** Define a filter for all futur spatialization calls. */
	public static void applyFilter(SpatializerHolder spatializer, ThresholdParameter param)
			throws SonarNativeException {
		fr.ifremer.sonarnative.jni.SonarNative.apply_filter(spatializer, param);
		if (!spatializer.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(spatializer.getLastErrorMsg());
		}
	}

	/** Define a filter for all futur spatialization calls. */
	public static void applyFilter(SpatializerHolder spatializer, BottomFilterParameter param)
			throws SonarNativeException {
		fr.ifremer.sonarnative.jni.SonarNative.apply_filter(spatializer, param);
		if (!spatializer.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(spatializer.getLastErrorMsg());
		}
	}

	/** Define a filter for all futur spatialization calls. */
	public static void applyFilter(SpatializerHolder spatializer, SpecularFilterParameter param)
			throws SonarNativeException {
		fr.ifremer.sonarnative.jni.SonarNative.apply_filter(spatializer, param);
		if (!spatializer.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(spatializer.getLastErrorMsg());
		}
	}

	/** Define a filter for all futur spatialization calls. */
	public static void applyFilter(SpatializerHolder spatializer, BeamIndexParameter param)
			throws SonarNativeException {
		fr.ifremer.sonarnative.jni.SonarNative.apply_filter(spatializer, param);
		if (!spatializer.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(spatializer.getLastErrorMsg());
		}
	}

	/** Define a filter for all futur spatialization calls. */
	public static void applyFilter(SpatializerHolder spatializer, SampleIndexParameter param)
			throws SonarNativeException {
		fr.ifremer.sonarnative.jni.SonarNative.apply_filter(spatializer, param);
		if (!spatializer.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(spatializer.getLastErrorMsg());
		}
	}

	/** Define a filter for all futur spatialization calls. */
	public static void applyFilter(SpatializerHolder spatializer, MultipingParameter param)
			throws SonarNativeException {
		fr.ifremer.sonarnative.jni.SonarNative.apply_filter(spatializer, param);
		if (!spatializer.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(spatializer.getLastErrorMsg());
		}
	}

	/** Define a filter for all futur spatialization calls. */
	public static void applyFilter(SpatializerHolder spatializer, DepthParameter param) throws SonarNativeException {
		fr.ifremer.sonarnative.jni.SonarNative.apply_filter(spatializer, param);
		if (!spatializer.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(spatializer.getLastErrorMsg());
		}
	}

	/** Define a filter for all futur spatialization calls. */
	public static void applyFilter(SpatializerHolder spatializer, AcrossDistanceParameter param)
			throws SonarNativeException {
		fr.ifremer.sonarnative.jni.SonarNative.apply_filter(spatializer, param);
		if (!spatializer.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(spatializer.getLastErrorMsg());
		}
	}

	/** Define a filter for all futur spatialization calls. */
	public static void applyFilter(SpatializerHolder spatializer, SideLobeParameter param) throws SonarNativeException {
		fr.ifremer.sonarnative.jni.SonarNative.apply_filter(spatializer, param);
		if (!spatializer.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(spatializer.getLastErrorMsg());
		}
	}

	/** Define a process for all futur spatialization calls. */
	public static void applySignalProcessing(SpatializerHolder spatializer, ResonOffsetParameter param) throws SonarNativeException {
		fr.ifremer.sonarnative.jni.SonarNative.apply_signal_processing(spatializer, param);
		if (!spatializer.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(spatializer.getLastErrorMsg());
		}
	}

	/** Define a process for all futur spatialization calls. */
	public static void applySignalProcessing(SpatializerHolder spatializer, RangeNormalizationParameter param) throws SonarNativeException {
		fr.ifremer.sonarnative.jni.SonarNative.apply_signal_processing(spatializer, param);
		if (!spatializer.getLastErrorMsg().isEmpty()) {
			throw new SonarNativeException(spatializer.getLastErrorMsg());
		}
	}
}
