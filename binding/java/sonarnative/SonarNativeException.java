/**
 * SonarNative - Ifremer
 */
package fr.ifremer.sonarnative;

/**
 * Exception raise when an error is detected in SonarNative
 */
public class SonarNativeException extends Exception {

	/**
	 * Serialization ID
	 */
	private static final long serialVersionUID = 6854666104130575648L;

	/**
	 * Constructor
	 */
	public SonarNativeException(String message) {
		super(message);
	}
}
