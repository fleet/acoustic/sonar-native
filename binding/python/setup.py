import setuptools
import sonarnative

class BinaryDistribution(setuptools.Distribution):
    """Distribution which always forces a binary package with platform name"""
    def has_ext_modules(foo):
        return True

setuptools.setup(
    name="sonarnative",
    version=f"{sonarnative.BuildConfiguration.VERSION_MAJOR}.{sonarnative.BuildConfiguration.VERSION_MINOR}.{sonarnative.BuildConfiguration.VERSION_PATCH}",
    author="Ifremer",
    author_email="globe-assistance@ifremer.fr",
    description="sonarnative contains high performance native library for sonar data processing",
    packages=["sonarnative"],
    package_data={'sonarnative' : ["*.pyd", "*.so"]},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
        "Operating System :: Microsoft :: Windows",
        "Operating System :: POSIX :: Linux"
    ],
    python_requires='>=3.6',
    distclass=BinaryDistribution
)
