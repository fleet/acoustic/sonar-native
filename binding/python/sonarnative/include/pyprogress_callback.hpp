#pragma once
#include "pybind11/pybind11.h"

#include "sonarnative/utils/progress_callback.hpp"


namespace sonarnative {

	class PyProgressCallback : public ProgressCallback {
	public:
		// Inherit the constructors 
		using ProgressCallback::ProgressCallback;

		void progress(double complete, std::string message) const override {
			PYBIND11_OVERLOAD_PURE(
				void,     // Return type 
				ProgressCallback, // Parent class 
				progress, // Name of function in C++ (must match Python name) 
				complete, // Argument
				message   // Argument) 
			);
		}
	};

}
