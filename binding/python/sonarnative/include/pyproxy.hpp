#pragma once
#include "pybind11/pybind11.h"

#include <sonarnative/log/log.hpp>
#include <sonarnative/log/proxy.hpp>


namespace sonarnative {

	class PyLogProxy : public LogProxy {
	public:
		// Inherit the constructors 
		using LogProxy::LogProxy;

		void fatal(std::string message) override {
			PYBIND11_OVERLOAD_PURE(
				void,     // Return type 
				LogProxy, // Parent class 
				fatal,    // Name of function in C++ (must match Python name) 
				message   // Argument) 
			);
		}
		void error(std::string message) override { PYBIND11_OVERLOAD_PURE(void, LogProxy, error, message); }
		void warn(std::string message) override { PYBIND11_OVERLOAD_PURE(void, LogProxy, warn, message); }
		void info(std::string message) override { PYBIND11_OVERLOAD_PURE(void, LogProxy, info, message); }
		void debug(std::string message) override { PYBIND11_OVERLOAD_PURE(void, LogProxy, debug, message); }
	};

}
