#include "pybind11/pybind11.h"


#define FORCE_IMPORT_ARRAY

#include <iostream>
#include <numeric>
#include <string>
#include <sstream>


// our headers
#include "sonarnative/sonarnative.hpp"
#include "sonarnative/sonarnative_config.hpp"

namespace py = pybind11;


// External definitions (in def_*.cpp files)
namespace sonarnative {
	void def_build_config(py::module& m);
	void def_log_class(py::module& m);
	void def_progress_callback_class(py::module& m);
	namespace xsf {
		void def_sonarnative_xsf(py::module& m);
	}
}

// Python Module
PYBIND11_MODULE(_sonarnative, module)
{
	//sonarnative::def_sonarnative_structs(module);
	sonarnative::def_build_config(module);

	// Exposes all C++ types in Python
	sonarnative::def_log_class(module);
	sonarnative::def_progress_callback_class(module);
	sonarnative::xsf::def_sonarnative_xsf(module);

	// make version string
	std::stringstream ss;
	ss << SONAR_NATIVE_VERSION_MAJOR << "."
		<< SONAR_NATIVE_VERSION_MINOR << "."
		<< SONAR_NATIVE_VERSION_PATCH;
	module.attr("__version__") = ss.str().c_str();
}