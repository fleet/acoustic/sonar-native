#include "pybind11/pybind11.h"
#include "pybind11/numpy.h"

#include <iostream>


// our headers
#include "pyprogress_callback.hpp"

namespace py = pybind11;

namespace sonarnative {

    void def_progress_callback_class(py::module & m)
    {
		py::class_<ProgressCallback, PyProgressCallback> (m, "progressCallback")
			.def(py::init<>())
			.def("progress", &ProgressCallback::progress)
			;
	}
}
