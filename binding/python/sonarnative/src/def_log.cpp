#include "pybind11/pybind11.h"
#include "pybind11/numpy.h"

#include <iostream>


// our headers
#include "pyproxy.hpp"

namespace py = pybind11;



namespace sonarnative {

    void def_log_class(py::module & m)
    {
		py::class_<LogProxy, PyLogProxy> (m, "logProxy")
			.def(py::init<>())
			.def("fatal", &LogProxy::fatal)
			.def("error", &LogProxy::error)
			.def("warn", &LogProxy::warn)
			.def("info", &LogProxy::info)
			.def("debug", &LogProxy::debug)
			;

		py::class_<Log>(m, "log")
			.def_static("initializeLog", &Log::initializeLog)
			.def_static("detachProxy", &Log::detachProxy)
			;


	}
}
