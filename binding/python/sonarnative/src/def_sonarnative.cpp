#include "pybind11/numpy.h"
#include "pybind11/pybind11.h"

// our headers
#include "sonarnative/api/filters_api.hpp"
#include "sonarnative/api/signal_processing_api.hpp"
#include "sonarnative/api/xsf_api.hpp"

namespace py = pybind11;

namespace sonarnative {
using namespace api;

namespace xsf {

void def_sonarnative_xsf(py::module& m) {
    py::class_<MemEchos>(m, "MemEchos")
        .def(py::init<int>())
        .def_readonly("size", &MemEchos::size)
        .def_property_readonly("longitude",
                               [](MemEchos& mem_echos) {
                                   return py::array_t<double>{py::ssize_t_cast(mem_echos.size), mem_echos.longitude};
                               })
        .def_property_readonly("latitude",
                               [](MemEchos& mem_echos) {
                                   return py::array_t<double>{py::ssize_t_cast(mem_echos.size), mem_echos.latitude};
                               })
        .def_property_readonly("across",
                               [](MemEchos& mem_echos) {
                                   return py::array_t<float>{py::ssize_t_cast(mem_echos.size), mem_echos.across};
                               })
        .def_property_readonly("along",
                               [](MemEchos& mem_echos) {
                                   return py::array_t<float>{py::ssize_t_cast(mem_echos.size), mem_echos.along};
                               })
        .def_property_readonly("beam_opening_across",
                               [](MemEchos& mem_echos) {
                                   return py::array_t<float>{py::ssize_t_cast(mem_echos.size), mem_echos.beam_opening_across};
                               })
        .def_property_readonly("beam_opening_along",
                               [](MemEchos& mem_echos) {
                                   return py::array_t<float>{py::ssize_t_cast(mem_echos.size), mem_echos.beam_opening_along};
                               })
        .def_property_readonly("elevation",
                               [](MemEchos& mem_echos) {
                                   return py::array_t<float>{py::ssize_t_cast(mem_echos.size), mem_echos.elevation};
                               })
        .def_property_readonly("height",
                               [](MemEchos& mem_echos) {
                                   return py::array_t<float>{py::ssize_t_cast(mem_echos.size), mem_echos.height};
                               })
        .def_property_readonly("echo",
                               [](MemEchos& mem_echos) {
                                   return py::array_t<float>{py::ssize_t_cast(mem_echos.size), mem_echos.echo};
                               })
        .def_property_readonly("non_overlapping_beam_opening_angle", [](MemEchos& mem_echos) {
            return py::array_t<float>{py::ssize_t_cast(mem_echos.size), mem_echos.non_overlapping_beam_opening_angle};
        });

    py::class_<FileEchos>(m, "FileEchos")
        .def_property_readonly("size", [](FileEchos& file_echos) { return file_echos.size(); })
        .def_property_readonly("longitude", &FileEchos::longitude)
        .def_property_readonly("latitude", &FileEchos::latitude)
        .def_property_readonly("across", &FileEchos::across)
        .def_property_readonly("along", &FileEchos::across)
        .def_property_readonly("beam_opening_across", &FileEchos::beam_opening_across)
        .def_property_readonly("beam_opening_along", &FileEchos::beam_opening_along)
        .def_property_readonly("elevation", &FileEchos::elevation)
        .def_property_readonly("height", &FileEchos::height)
        .def_property_readonly("echo", &FileEchos::echo)
        .def_property_readonly("non_overlapping_beam_opening_angle", &FileEchos::non_overlapping_beam_opening_angle);

    py::enum_<SpatializationMethod>(m, "SpatializationMethod")
        .value("DetectionInterpolation", SpatializationMethod::DetectionInterpolation)
        .value("EquivalentCelerity", SpatializationMethod::EquivalentCelerity)
        .value("ConstantCelerity", SpatializationMethod::ConstantCelerity);

    py::class_<SpatializerHolder>(m, "SpatializerHolder")
        .def("__repr__", [](const SpatializerHolder& spatializer_Holder) { return "<XSF Spatializer proxy"; })
        .def("get_swath_count", &SpatializerHolder::get_swath_count)
        .def("get_beam_count", &SpatializerHolder::get_beam_count);

    m.def("open_spatializer", &open_spatializer, py::return_value_policy::reference);
    m.def("estimate_beam_echo_count", &estimate_beam_echo_count);
    m.def("estimate_normalization_ref_level", &estimate_normalization_ref_level);
    m.def("change_beam_group_number", &change_beam_group_number);
    m.def("set_spatialization_method", &set_spatialization_method);
    m.def("spatialize_in_memory", &spatialize_in_memory);
    m.def("spatialize_all_on_disk", &spatialize_all_on_disk, py::return_value_policy::take_ownership);
    m.def("close_spatializer", &close_spatializer);

    py::class_<SamplingParameter>(m, "SamplingParameter").def(py::init<int>());
    m.def("apply_sampling_filter", [](SpatializerHolder& spatializer, SamplingParameter& param) { apply_filter(spatializer, param); });

    py::class_<ThresholdParameter>(m, "ThresholdParameter").def(py::init<bool, float, float>());
    m.def("apply_threshold_filter", [](SpatializerHolder& spatializer, ThresholdParameter& param) { apply_filter(spatializer, param); });

    py::class_<BottomFilterParameter>(m, "BottomFilterParameter")
        .def_static("new_sample", &BottomFilterParameter::new_sample)
        .def_static("new_range_percent", &BottomFilterParameter::new_range_percent);
    m.def("apply_bottom_filter", [](SpatializerHolder& spatializer, BottomFilterParameter& param) { apply_filter(spatializer, param); });

    py::class_<SpecularFilterParameter>(m, "SpecularFilterParameter").def(py::init<bool, bool, int>());
    m.def("apply_specular_filter", [](SpatializerHolder& spatializer, SpecularFilterParameter& param) { apply_filter(spatializer, param); });

    py::class_<BeamIndexParameter>(m, "BeamIndexParameter").def(py::init<bool, int, int>());
    m.def("apply_beam_index_filter", [](SpatializerHolder& spatializer, BeamIndexParameter& param) { apply_filter(spatializer, param); });

    py::class_<SampleIndexParameter>(m, "SampleIndexParameter").def(py::init<bool, int, int>());
    m.def("apply_sample_index_filter", [](SpatializerHolder& spatializer, SampleIndexParameter& param) { apply_filter(spatializer, param); });

    py::class_<MultipingParameter>(m, "MultipingParameter").def(py::init<bool, int>());
    m.def("apply_multiping_filter", [](SpatializerHolder& spatializer, MultipingParameter& param) { apply_filter(spatializer, param); });

    py::class_<DepthParameter>(m, "DepthParameter").def(py::init<bool, float, float>());
    m.def("apply_depth_filter", [](SpatializerHolder& spatializer, DepthParameter& param) { apply_filter(spatializer, param); });

    py::class_<AcrossDistanceParameter>(m, "AcrossDistanceParameter").def(py::init<bool, float, float>());
    m.def("apply_across_distance_filter", [](SpatializerHolder& spatializer, AcrossDistanceParameter& param) { apply_filter(spatializer, param); });

    py::class_<SideLobeParameter>(m, "SideLobeParameter").def(py::init<bool, float>());
    m.def("apply_side_lobe_filter", [](SpatializerHolder& spatializer, SideLobeParameter& param) { apply_filter(spatializer, param); });

    py::class_<ResonOffsetParameter>(m, "ResonOffsetParameter").def(py::init<float>());
    m.def("apply_reson_offset_signal_processing",
          [](SpatializerHolder& spatializer, ResonOffsetParameter& param) { apply_signal_processing(spatializer, param); });

    py::class_<RangeNormalizationParameter>(m, "RangeNormalizationParameter").def(py::init<bool, float>());
    m.def("apply_range_normalization_signal_processing",
          [](SpatializerHolder& spatializer, RangeNormalizationParameter& param) { apply_signal_processing(spatializer, param); });
}
}  // namespace xsf
}  // namespace sonarnative
