#!/bin/bash

set -e
# set -x


case "$1" in
    *)
        . /usr/local/miniconda/bin/activate sonarnative-dev
        exec "$@"
esac

exit 1