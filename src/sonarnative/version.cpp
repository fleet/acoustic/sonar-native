#include "sonarnative/version.hpp"
#include "sonarnative/sonarnative_config.hpp"

namespace sonarnative {
    
	std::string version()
	{
		return std::to_string(SONAR_NATIVE_VERSION_MAJOR) + "." + std::to_string(SONAR_NATIVE_VERSION_MINOR) + "." + std::to_string(SONAR_NATIVE_VERSION_PATCH);
	}

}
