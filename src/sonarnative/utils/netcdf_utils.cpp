
#include "sonarnative/utils/file/netcdf_utils.hpp"
#include <memory>
#include <numeric>

using namespace std;
namespace sonarnative {

	int nc_open_file(string path)
	{
		int retval, file_id;
		if (retval = nc_open(path.c_str(), NC_NOWRITE, &file_id)) {
			throw string("Unable to open the netcdf file. ").append(nc_strerror(retval));
		}
		return file_id;
	}

	void nc_close_file(int file_id)
	{
		int retval;
		if (retval = nc_close(file_id)) {
			throw string("Unable to close the netcdf file. ").append(nc_strerror(retval));
		}
	}

	int nc_close_quietly(int file_id)
	{
		return nc_close(file_id);
	}

	int nc_get_group_quietly(int file_id, string group_path) {
		int group_id;
		if (nc_inq_grp_full_ncid(file_id, group_path.c_str(), &group_id) == NC_NOERR) {
			return group_id;
		}
		return -1;
	}

	int nc_get_group(int file_id, string group_path) {
		int retval, group_id;
		if ((retval = nc_inq_grp_full_ncid(file_id, group_path.c_str(), &group_id))) {
			throw string("Unable to access group. ").append(group_path).append(". ").append(nc_strerror(retval));
		}
		return group_id;
	}

	int nc_get_variable_quietly(int group_id, string var_name)
	{
		int var_id;
		if (nc_inq_varid(group_id, var_name.c_str(), &var_id) == NC_NOERR) {
			return var_id;
		}
		return -1;
	}

	int nc_get_variable(int group_id, string var_name)
	{
		int retval, var_id;
		if ((retval = nc_inq_varid(group_id, var_name.c_str(), &var_id))) {
			throw string("Unable to access variable ").append(var_name).append(". ").append(nc_strerror(retval));
		}
		return var_id;
	}

	size_t nc_get_dimension(int group_id, string dim_name)
	{
		int retval, dim_id;
		/* Get the varid of the data variable, based on its name. */
		if ((retval = nc_inq_dimid(group_id, dim_name.c_str(), &dim_id))) {
			throw string("Unable to find dimension ").append(dim_name).append(". ").append(nc_strerror(retval));
		}
		size_t result;
		/* Get the varid of the data variable, based on its name. */
		if ((retval = nc_inq_dimlen(group_id, dim_id, &result))) {
			throw string("Unable to read dimension ").append(dim_name).append(". ").append(nc_strerror(retval));
		}

		return result;
	}


	vector<size_t> nc_get_var_dimensions(int group_id, int var_id)
	{
		int dim_count = 0;
		int retval;
		if ((retval = nc_inq_varndims(group_id, var_id, &dim_count))) {
			throw string("Unable to access variable dimensions. ").append(nc_strerror(retval));
		}

		vector<size_t> dimensions;
		if (dim_count > 0) {
			dimensions.resize(dim_count);
			auto dimidsp = std::make_unique<int[]>(dim_count);
			if (retval = nc_inq_vardimid(group_id, var_id, dimidsp.get())) {
				throw string("Unable to access variable dimensions. ").append(nc_strerror(retval));
			}
			for (size_t i = 0; i < dim_count; i++)
			{
				if ((retval = nc_inq_dimlen(group_id, dimidsp[i], &dimensions[i]))) {
					throw string("Unable to find dimension. ").append(nc_strerror(retval));
				}
			}
		}

		return dimensions;
	}

	nc_type nc_get_var_type(int group_id, int var_id)
	{
		nc_type result = 0;
		int retval;
		if ((retval = nc_inq_vartype(group_id, var_id, &result))) {
			throw string("Unable to access variable type. ").append(nc_strerror(retval));
		}

		return result;
	}

	nc_type nc_get_vlen_base_type(int group_id, const char* vlen_type_name)
	{
		nc_type typeidp;
		int retval;
		if ((retval = nc_inq_typeid(group_id, vlen_type_name, &typeidp))) {
			throw string("Datatype not found : ").append(vlen_type_name);
		}

		nc_type base_nc_typep;
		if ((retval = nc_inq_vlen(group_id, typeidp, NULL, NULL, &base_nc_typep))) {
			throw string("Unable to access variable type. ").append(nc_strerror(retval));
		}

		return base_nc_typep;
	}


	void nc_fill_with_values(int group_id, int var_id, const size_t* from, const size_t* count, void* result)
	{
		int retval;
		if (retval = nc_get_vara(group_id, var_id, from, count, result)) {
			throw string("Unable to read variable ").append(nc_strerror(retval));
		}
	}


	float nc_offset(int group_id, int var_id)
	{
		float retval = 0.f;
		if (nc_get_att_float(group_id, var_id, "add_offset", &retval) != 0) {
			return 0.f;
		}
		return retval;
	}

	float nc_scale_factor(int group_id, int var_id)
	{
		float retval = 1.f;
		if (nc_get_att_float(group_id, var_id, "scale_factor", &retval) != 0) {
			return 1.f;
		}
		return retval;
	}

	short nc_short_missing_value(int group_id, int var_id)
	{
		short retval = -32768;
		if (nc_get_att_short(group_id, var_id, "missing_value", &retval) != 0) {
			return -32768;
		}
		return retval;
	}

	int nc_group_att_int(int group_id, const char* attName) {
		int retval = -32768;
		if (nc_get_att_int(group_id, NC_GLOBAL, attName, &retval) != 0) {
			return -32768;
		}
		return retval;
	}

	string nc_group_att_string(int group_id, const char* attName) {
		char buffer[255];
		if (nc_get_att_text(group_id, NC_GLOBAL, attName, buffer) != 0) {
			return string("");
		}
		return string(buffer);
	}

	void nc_delete_vlen(std::vector<nc_vlen_t>* vlen_buffer)
	{
		if (vlen_buffer != nullptr)
			nc_free_vlens(vlen_buffer->size(), vlen_buffer->data());
	}

	// ENTIRE VARIABLE READING
	double* nc_read_double(const int group_id, const int var_id, std::unique_ptr<std::vector<char>>& buffer)
	{
		if (buffer == nullptr) {
			const vector<size_t> dimensions = nc_get_var_dimensions(group_id, var_id);
			size_t nb_doubles = std::accumulate(dimensions.begin(), dimensions.end(), (size_t)0, std::plus<>());
			buffer.reset(new std::vector<char>(sizeof(double) * nb_doubles));
		}
		int retval = NC_NOERR;
		if (retval = nc_get_var(group_id, var_id, buffer->data())) {
			throw string("Unable to read doubles from variable ").append(nc_strerror(retval));
		}
		return (double*)buffer->data();
	}

	float* nc_read_float(const int group_id, const int var_id, std::unique_ptr<std::vector<char>>& buffer)
	{
		if (buffer == nullptr) {
			vector<size_t> dimensions = nc_get_var_dimensions(group_id, var_id);
			int nb_floats = std::accumulate(dimensions.begin(), dimensions.end(), 1, std::multiplies<>()) ;
			buffer.reset(new std::vector<char>(sizeof(float) * nb_floats));
		}
		int retval = NC_NOERR;
		if (retval = nc_get_var(group_id, var_id, buffer->data())) {
			throw string("Unable to read floats from variable ").append(nc_strerror(retval));
		}
		return (float*)buffer->data();
	}

	int* nc_read_int(const int group_id, const int var_id, std::unique_ptr<std::vector<char>>& buffer)
	{
		if (buffer == nullptr) {
			vector<size_t> dimensions = nc_get_var_dimensions(group_id, var_id);
			int nb_ints = std::accumulate(dimensions.begin(), dimensions.end(), 1, std::multiplies<>());
			buffer.reset(new std::vector<char>(sizeof(int) * nb_ints));
		}
		int retval = NC_NOERR;
		if (retval = nc_get_var(group_id, var_id, buffer->data())) {
			throw string("Unable to read ints from variable ").append(nc_strerror(retval));
		}
		return (int*)buffer->data();
	}

	unsigned char* nc_read_byte(const int group_id, const int var_id, std::unique_ptr<std::vector<char>>& buffer)
	{
		if (buffer == nullptr) {
			vector<size_t> dimensions = nc_get_var_dimensions(group_id, var_id);
			int nb_bytes = std::accumulate(dimensions.begin(), dimensions.end(), 1, std::multiplies<>());
			buffer.reset(new std::vector<char>(sizeof(unsigned char) * nb_bytes));
		}
		int retval = NC_NOERR;
		if (retval = nc_get_var(group_id, var_id, buffer->data())) {
			throw string("Unable to read ints from variable ").append(nc_strerror(retval));
		}
		return (unsigned char*)buffer->data();
	}

	uint64_t* nc_read_ulong(const int group_id, const int var_id, std::unique_ptr<std::vector<char>>& buffer)
	{
		if (buffer == nullptr) {
			vector<size_t> dimensions = nc_get_var_dimensions(group_id, var_id);
			int nb_ulongs = std::accumulate(dimensions.begin(), dimensions.end(), 1, std::multiplies<>());
			buffer.reset(new std::vector<char>(sizeof(uint64_t) * nb_ulongs));
		}
		int retval = NC_NOERR;
		if (retval = nc_get_var(group_id, var_id, buffer->data())) {
			throw string("Unable to read ulongs from variable ").append(nc_strerror(retval));
		}
		return (uint64_t*)buffer->data();
	}

	// CHUNK READING
	float* nc_read_float_2d_var(const int group_id, const int var_id, const int from, const int nb, std::unique_ptr<std::vector<char>>& buffer) {
		vector<size_t> dimensions = nc_get_var_dimensions(group_id, var_id);
		if (dimensions.size() != 2) {
			throw string("Expected a 2D variable for calling nc_read_float_2d_var");
		}
		size_t from_tx[] = { (size_t)from, 0 };
		size_t count_tx[] = { (size_t)nb, dimensions[1] };
		if (buffer == nullptr) {
			buffer.reset(new std::vector<char>(sizeof(float) * nb * dimensions[1]));
		}
		nc_fill_with_values(group_id, var_id, from_tx, count_tx, buffer->data());
		return (float*)buffer->data();
	}

	double* nc_read_double_2d_var(const int group_id, const int var_id, const int from, const int nb, std::unique_ptr<std::vector<char>>& buffer) {
		vector<size_t> dimensions = nc_get_var_dimensions(group_id, var_id);
		if (dimensions.size() != 2) {
			throw string("Expected a 2D variable for calling nc_read_double_2d_var");
		}
		size_t from_tx[] = { (size_t)from, 0 };
		size_t count_tx[] = { (size_t)nb, dimensions[1] };
		if (buffer == nullptr) {
			buffer.reset(new std::vector<char>(sizeof(double) * nb * dimensions[1]));
		}
		nc_fill_with_values(group_id, var_id, from_tx, count_tx, buffer->data());
		return (double*)buffer->data();
	}

	int* nc_read_int_2d_var(const int group_id, const int var_id, const int from, const int nb, std::unique_ptr<std::vector<char>>& buffer) {
		vector<size_t> dimensions = nc_get_var_dimensions(group_id, var_id);
		if (dimensions.size() != 2) {
			throw string("Expected a 2D variable for calling nc_read_int_2d_var");
		}
		size_t from_tx[] = { (size_t)from, 0 };
		size_t count_tx[] = { (size_t)nb, dimensions[1] };
		if (buffer == nullptr) {
			buffer.reset(new std::vector<char>(sizeof(int) * nb * dimensions[1]));
		}
		nc_fill_with_values(group_id, var_id, from_tx, count_tx, buffer->data());
		return (int*)buffer->data();
	}

	short* nc_read_short_2d_var(const int group_id, const int var_id, const int from, const int nb, std::unique_ptr<std::vector<char>>& buffer) {
		vector<size_t> dimensions = nc_get_var_dimensions(group_id, var_id);
		if (dimensions.size() != 2) {
			throw string("Expected a 2D variable for calling nc_read_short_2d_var");
		}
		size_t from_tx[] = { (size_t)from, 0 };
		size_t count_tx[] = { (size_t)nb, dimensions[1] };
		if (buffer == nullptr) {
			buffer.reset(new std::vector<char>(sizeof(short) * nb * dimensions[1]));
		}
		nc_fill_with_values(group_id, var_id, from_tx, count_tx, buffer->data());
		return (short*)buffer->data();
	}

	unsigned char* nc_read_byte_2d_var(const int group_id, const int var_id, const int from, const int nb, std::unique_ptr<std::vector<char>>& buffer) {
		vector<size_t> dimensions = nc_get_var_dimensions(group_id, var_id);
		if (dimensions.size() != 2) {
			throw string("Expected a 2D variable for calling nc_read_byte_2d_var");
		}
		size_t from_tx[] = { (size_t)from, (size_t)0 };
		size_t count_tx[] = { (size_t)nb, dimensions[1] };
		if (buffer == nullptr) {
			buffer.reset(new std::vector<char>(sizeof(unsigned char) * nb * dimensions[1]));
		}
		nc_fill_with_values(group_id, var_id, from_tx, count_tx, buffer->data());
		return (unsigned char*)buffer->data();
	}

	std::unique_ptr<std::vector<nc_vlen_t>> nc_read_vlen_var(const int group_id, const int var_id, const int from, const int nb) {
		vector<size_t> dimensions = nc_get_var_dimensions(group_id, var_id);
		vector<size_t> from_idx = vector<size_t>(dimensions.size(), 0);
		from_idx[0] = from;
		dimensions[0] = nb;
		auto result = std::make_unique<std::vector<nc_vlen_t>>(dimensions[1]);
		// Read values for corresponding to the ping
		nc_fill_with_values(group_id, var_id, from_idx.data(), dimensions.data(), result->data());
		return std::move(result);
	}

	std::vector<std::string> nc_read_string_var(const int group_id, const int var_id) {
		vector<size_t> dimensions = nc_get_var_dimensions(group_id, var_id);
		vector<size_t> from_idx{ 0 };
		std::vector<char*> buffer{ dimensions[0] };
		int ret = nc_get_vara_string(group_id, var_id, from_idx.data(), dimensions.data(), buffer.data());
		std::vector<std::string> result;
		for (char* elem : buffer) {
			result.emplace_back(elem);
		}
		nc_free_string(buffer.size(), buffer.data());
		return result;
	}
} // end namespace 

