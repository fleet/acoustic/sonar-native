#include <fstream>

#include "sonarnative/sonarnative.hpp"
#include "sonarnative/utils/conversion_utils.hpp" // toRadians
#include "sonarnative/spatialization/spatializer.hpp"


#include "log4cpp/convenience.h"
#include "log4cpp/Category.hh"

namespace sonarnative {
	namespace spatialization {

		const double ECCENTRICITY_SQUARED = 0.00669437999014133;
		const double SEMI_MAJOR_AXIS = 6378137.0;


		// Logger definition
		LOG4CPP_LOGGER("sonarnative.spatializer");
		
		void Spatializer::configure_swath_level(SwathData& swath_data) {
			this->swath_data = swath_data;

			cos_platform_latitude = cos(toRadians(swath_data.platform_latitude));
			sin_platform_latitude = sin(toRadians(swath_data.platform_latitude));
			cos_platform_heading = cos(toRadians(swath_data.platform_heading));
			sin_platform_heading = sin(toRadians(swath_data.platform_heading));

			double e2 = 1.0 - ECCENTRICITY_SQUARED * sin_platform_latitude * sin_platform_latitude;
			double norm = SEMI_MAJOR_AXIS / sqrt(e2);
			double radius = norm * (1.0 - ECCENTRICITY_SQUARED) / e2;
			cos_heading_div_radius = toDegrees(cos_platform_heading) / radius;
			sin_heading_div_radius = toDegrees(sin_platform_heading) / radius;
			cos_heading_div_norm = toDegrees(cos_platform_heading) / norm / cos_platform_latitude;
			sin_heading_div_norm = toDegrees(sin_platform_heading) / norm / cos_platform_latitude;
			samplerate = 1.0 / swath_data.sample_interval;
			
		}

		void Spatializer::configure_beam_level(BeamData& beam_data) {
			this->beam_data = beam_data;

			//bathy placement if SpatializationMethod is DetectionInterpolation
			if (SpatializationMethod::DetectionInterpolation == method && !swath_data.detectionsPerSectorPerAntenna.empty()) {
				DetectionInterpolator& detectionIt = swath_data.detectionsPerSectorPerAntenna[beam_data.tx_sector * swath_data.antennaCount + beam_data.rx_transducer];
				if (detectionIt.size() > 1) {
					DetectionPoint point = detectionIt.getNormalizedInterpolatedValue((float)toRadians(beam_data.rx_beam_rotation_phi));

					across_norm = point.across;
					along_norm = point.along;
					depth_norm = point.depth;
					float distanceNorm = 1.f;
					float equivalentCelerity = distanceNorm / point.travelTime;
					beamRange = equivalentCelerity / samplerate;
					hasDetection = true;
					return;
				}
			}
			// less than 2 detections in sector or other method
			hasDetection = false;
			beamRange = beam_data.equivalentCelerity / (2 * samplerate);
			sinEquivalentPointingAngle = sin(beam_data.equivalentBeamPointingAngleRad);
			cosEquivalentPointingAngle = cos(beam_data.equivalentBeamPointingAngleRad);
			tanEquivalentTiltAngle = tan(beam_data.equivalentTiltAngleRad);
			cosEquivalentTiltAngle = cos(beam_data.equivalentTiltAngleRad);
			tanHeadingAngle = tan(toRadians(beam_data.tx_transducer_rotation_z));
			cosHeadingAngle = cos(toRadians(beam_data.tx_transducer_rotation_z));
		}

		void Spatializer::spatialize(EchoData& echo) {
			// find X,Z by interpolation between bottomRange detection and this sample
			double rangeMeter = beamRange * echo.rank;
			double depthRelTrans;
			double acrossDistance;
			double alongDistance;

			//bathy placement by detection interpolation
			if (hasDetection) {
				depthRelTrans = rangeMeter * depth_norm ;
				acrossDistance = rangeMeter * across_norm + beam_data.rx_transducer_offset_y;
				alongDistance = rangeMeter * along_norm + beam_data.rx_transducer_offset_x;
			}
			else {
				depthRelTrans = rangeMeter * cosEquivalentPointingAngle * cosEquivalentTiltAngle;
				acrossDistance = -rangeMeter * sinEquivalentPointingAngle + beam_data.rx_transducer_offset_y;
				alongDistance = rangeMeter * sinEquivalentPointingAngle * tanHeadingAngle +
					depthRelTrans * tanEquivalentTiltAngle * cosHeadingAngle + beam_data.rx_transducer_offset_x;
			}

			// Set resulting values for the echo
			echo.across = acrossDistance;
			echo.along = alongDistance;
			echo.height = beamRange;
			// The attitude of the plateforme is neglegted to compute z offset between rx and tx
			echo.elevation = -depthRelTrans - (swath_data.tx_transducer_depth + beam_data.rx_transducer_offset_z - beam_data.tx_transducer_offset_z);
			echo.latitude = swath_data.platform_latitude + alongDistance * cos_heading_div_radius - acrossDistance * sin_heading_div_radius;
			echo.longitude = swath_data.platform_longitude + alongDistance * sin_heading_div_norm + acrossDistance * cos_heading_div_norm;
		}
	}
}
