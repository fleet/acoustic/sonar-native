#include "sonarnative/spatialization/filters.hpp"

#include "sonarnative/utils/conversion_utils.hpp"  // toRadians

namespace sonarnative {
namespace spatialization {

using namespace sonarnative::api;

bool EchoFilter::inspects_echo(SwathData& swath_data, BeamData& beam_data, EchoData& echo) noexcept {
    if (threshold_parameter.enable && !is_threshold_suitable(echo)) return false;
    if (specular_parameter.enable && !is_specular_suitable(swath_data, echo)) return false;
    if (bottom_parameter.enable && !is_bottom_suitable(swath_data, beam_data, echo)) return false;
    if (side_lobe_parameter.enable && !is_side_lobe_suitable(swath_data, beam_data, echo)) return false;
    return true;
}

bool EchoFilter::inspects_localized_echo(EchoData& echo) noexcept {
    if (depth_parameter.enable && !is_depth_suitable(echo)) return false;
    if (across_distance_parameter.enable && !is_across_distance_suitable(echo)) return false;
    return true;
}

// Check ThresholdParameter
bool EchoFilter::is_threshold_suitable(EchoData& echo) noexcept {
    return echo.value >= threshold_parameter.min && echo.value <= threshold_parameter.max;
}

bool EchoFilter::is_bottom_suitable(SwathData& swath_data, BeamData& beam_data, EchoData& echo) noexcept {
    // Compute the value of min_bottom for current beam
    int min_bottom = beam_data.bottomRangeIndex;
    if (min_bottom <= 0) {
        return true;
    }

    double alpha = bottom_parameter.angle_coefficient * (1.0 - 1.0 / cos(toRadians(beam_data.rx_beam_rotation_phi)));
    // SAMPLE
    if (bottom_parameter.tolerance_type == BottomFilterParameter::ToleranceType::sample)
        return echo.rank <= alpha + min_bottom - bottom_parameter.tolerance_absolute;
    // RANGEPERCENT:
    return echo.rank <= ((100.0 + alpha - bottom_parameter.tolerance_percent) / 100.0) * min_bottom;
}

// Check ThresholdSphereParameter
bool EchoFilter::is_specular_suitable(SwathData& swath_data, EchoData& echo) noexcept {
    if (specular_parameter.below) {
        return echo.rank < swath_data.sphere_range - specular_parameter.tolerance;
    } else {
        return abs(echo.rank - swath_data.sphere_range) > specular_parameter.tolerance;
    }
}

// Check DepthParameter
bool EchoFilter::is_depth_suitable(EchoData& echo) noexcept {
    return echo.elevation >= -depth_parameter.max && echo.elevation <= -depth_parameter.min;
}

// Check AcrossDistanceParameter
bool EchoFilter::is_across_distance_suitable(EchoData& echo) noexcept {
    return echo.across >= across_distance_parameter.min && echo.across <= across_distance_parameter.max;
}

// Check SideLobeParameter
bool EchoFilter::is_side_lobe_suitable(SwathData& swath_data, BeamData& beam_data, EchoData& echo) noexcept {
    size_t rankSectorIndex = echo.rank * swath_data.sectorCount + beam_data.tx_sector;
    return echo.rawValue > swath_data.sidelobe_ref_backscatter_per_rank_per_sector[rankSectorIndex] + side_lobe_parameter.threshold;
}

int EchoFilter::get_sampling_threshold() noexcept { return sampling_parameter.threshold; }

BeamIndexParameter EchoFilter::get_beam_index_parameter() noexcept { return beam_index_parameter; }
SampleIndexParameter EchoFilter::get_sample_index_parameter() noexcept { return sample_index_parameter; }
MultipingParameter EchoFilter::get_multiping_parameter() noexcept { return multiping_parameter; }

void EchoFilter::apply(SamplingParameter parameter) noexcept { sampling_parameter = parameter; }
void EchoFilter::apply(BottomFilterParameter parameter) noexcept { bottom_parameter = parameter; }
void EchoFilter::apply(ThresholdParameter parameter) noexcept { threshold_parameter = parameter; }
void EchoFilter::apply(SpecularFilterParameter parameter) noexcept { specular_parameter = parameter; }
void EchoFilter::apply(BeamIndexParameter parameter) noexcept { beam_index_parameter = parameter; }
void EchoFilter::apply(SampleIndexParameter parameter) noexcept { sample_index_parameter = parameter; }
void EchoFilter::apply(DepthParameter parameter) noexcept { depth_parameter = parameter; }
void EchoFilter::apply(AcrossDistanceParameter parameter) noexcept { across_distance_parameter = parameter; }
void EchoFilter::apply(SideLobeParameter parameter) noexcept { side_lobe_parameter = parameter; }
void EchoFilter::apply(MultipingParameter parameter) noexcept { multiping_parameter = parameter; }
}  // namespace spatialization
}  // namespace sonarnative
