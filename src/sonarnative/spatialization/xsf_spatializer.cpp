#include "sonarnative/spatialization/xsf_spatializer.hpp"

#include <cmath>
#include <functional>
#include <future>
#include <thread>
#include <vector>

#include "Eigen/Dense"
#include "log4cpp/Category.hh"
#include "log4cpp/convenience.h"
#include "sonarnative/spatialization/linear_interpolator.hpp"
#include "sonarnative/spatialization/spatializer.hpp"
#include "sonarnative/utils/conversion_utils.hpp"

namespace sonarnative {
namespace xsf {
using namespace std;
using namespace sonarnative::spatialization;
using namespace Eigen;

constexpr float backscatterResonOffset = -42.f;

// Logger definition
LOG4CPP_LOGGER("sonarnative.xsf_spatializer");

XsfSpatializer::XsfSpatializer(string xsf_path, int ncid) : xsf_driver(XsfDriver(xsf_path, ncid)) {
    LOG4CPP_INFO_S(logger) << "XSF Spatializer created."
                           << " xsf_path=" << xsf_path;
}

XsfSpatializer::~XsfSpatializer() { close(); }

void XsfSpatializer::initialize() {
    LOG4CPP_INFO_S(logger) << "XSF Spatializer initialization. Opening the file...";

    // initialize XSF driver
    xsf_driver.open();
}

void XsfSpatializer::perform(int swath, XsfData& xsf_data, BeamEchoCallback& callback) {
    // First Apply multiping sequence index filter
    api::MultipingParameter multiping = echo_filter.get_multiping_parameter();
    if (multiping.enable && xsf_data.multiping_sequence != nullptr && multiping.index != xsf_data.multiping_sequence[swath]) return;

    // Init and compute parameters that are available for whole swath
    SwathData swath_data;
    init_swath_data(xsf_data, swath, swath_data);

    compute_swath_data(xsf_data, swath_data);

    // Applying the beam index filter
    int first_beam = 0;
    int last_beam = (int)get_beam_count();
    api::BeamIndexParameter beam_range = echo_filter.get_beam_index_parameter();
    if (beam_range.enable) {
        first_beam = beam_range.min;
        if (beam_range.max != INT_MAX) {
            last_beam = min(last_beam, beam_range.max + 1);
        }
    }

    if (multi_thread && last_beam - first_beam > 10)  // mutithreading not useful when less than 10 beams are required
    {
        int number_of_threads = min(max((int)std::thread::hardware_concurrency(), 1), 4);
        int beams_per_thread = static_cast<int>(ceil((last_beam - first_beam) * 1.f / number_of_threads));
        std::vector<std::future<void>> futurs(number_of_threads);
        int threadId = 0;
        for (int threadId = 0; threadId < number_of_threads; threadId++, first_beam += beams_per_thread) {
            futurs[threadId] = std::async(std::launch::async, &XsfSpatializer::_perform, this, swath, std::ref(xsf_data), std::ref(swath_data),
                                          first_beam, fmin(first_beam + beams_per_thread, last_beam), std::ref(callback));
        }

        // Wait for all computations
        std::for_each(futurs.begin(), futurs.end(), [](std::future<void>& oneFutur) { oneFutur.get(); });
    } else {
        _perform(swath, xsf_data, swath_data, first_beam, last_beam, callback);
    }
}

float XsfSpatializer::compute_two_way_travel_time(const XsfData& xsf_data, const SwathData& swath_data, const int beam) {
    float travelTime = 2.f * xsf_data.detected_bottom_range[beam] / swath_data.sound_speed_at_transducer;
    return travelTime;
}

int XsfSpatializer::compute_bottom_range_index(const XsfData& xsf_data, const SwathData& swath_data, const float twoWayTravelTime, const int beam) {
    float blanking = xsf_data.blanking_interval[beam];
    float sample_interval_value = swath_data.sample_interval;

    int tx_sector = (xsf_data.transmit_beam_index != nullptr && xsf_data.transmit_beam_index[beam] >= 0) ? xsf_data.transmit_beam_index[beam] : 0;
    float time_offset = xsf_data.sample_time_offset[tx_sector];
    int bottomRangeIndex = (int)round((twoWayTravelTime + time_offset - blanking) / sample_interval_value);
    return bottomRangeIndex;
}

// Set values on the BeamData
void XsfSpatializer::init_beam_data(XsfData& xsf_data, int beam, SwathData& swath_data, BeamData& beam_data) {
    size_t dim_beam = get_beam_count();
    size_t dim_tx_beam = get_sector_count();
    size_t dim_detection = get_detection_count();

    beam_data.beam = beam;
    beam_data.tx_sector = 0;
    if (xsf_data.transmit_beam_index != nullptr && xsf_data.transmit_beam_index[beam] >= 0) {
        beam_data.tx_sector = xsf_data.transmit_beam_index[beam];
    }
    if (xsf_data.transmit_transducer_index != nullptr && xsf_data.transmit_transducer_index[beam_data.tx_sector] >= 0) {
        beam_data.tx_transducer = xsf_data.transmit_transducer_index[beam_data.tx_sector];
    } else {
        beam_data.tx_transducer = xsf_data.default_tx_transducer_index;
    }

    bool antennaChanged = false;
    if (xsf_data.receive_transducer_index != nullptr && xsf_data.receive_transducer_index[beam] >= 0) {
        antennaChanged = (beam > 0) && (xsf_data.receive_transducer_index[beam] != xsf_data.receive_transducer_index[beam - 1]);
        beam_data.rx_transducer = xsf_data.receive_transducer_index[beam];
    } else {
        beam_data.rx_transducer = xsf_data.default_rx_transducer_index;
    }

    float surfaceCelerity = swath_data.sound_speed_at_transducer;
    double eqCelerityRatio = swath_data.equivalentCelerityRatio;
    double wcAngleRefAbsoluteRad = toRadians(xsf_data.rx_beam_rotation_phi[beam_data.beam]);
    double sinEquivalentAngle = sin(wcAngleRefAbsoluteRad) * eqCelerityRatio;

    beam_data.equivalentCelerity = (float)(surfaceCelerity * eqCelerityRatio);
    beam_data.equivalentBeamPointingAngleRad = asin(sinEquivalentAngle);
    beam_data.equivalentTiltAngleRad = swath_data.equivalentTiltAngleRadPerSector[beam_data.tx_sector];

    beam_data.rx_transducer_offset_x = xsf_data.transducer_offset_x[beam_data.rx_transducer];
    beam_data.rx_transducer_offset_y = xsf_data.transducer_offset_y[beam_data.rx_transducer];
    beam_data.rx_transducer_offset_z = xsf_data.transducer_offset_z[beam_data.rx_transducer];
    beam_data.tx_transducer_offset_z = xsf_data.transducer_offset_z[beam_data.tx_transducer];
    beam_data.tx_transducer_rotation_z = xsf_data.transducer_rotation_z[beam_data.tx_transducer];

    beam_data.beam_opening_across = xsf_data.beamwidth_receive_major[swath_data.swath * dim_tx_beam + beam_data.tx_sector];
    beam_data.beam_opening_along = xsf_data.beamwidth_transmit_minor[swath_data.swath * dim_tx_beam + beam_data.tx_sector];
    beam_data.rx_beam_rotation_phi = xsf_data.rx_beam_rotation_phi[beam];

    beam_data.bottomRangeIndex = 0;

    float travelTime = 0.0f;
    // retrieve real travel time if detction is valid
    if (xsf_data.status != nullptr && xsf_data.status[beam] == 0) {  // Check validity
        if (xsf_data.detected_bottom_range != nullptr) {
            travelTime = compute_two_way_travel_time(xsf_data, swath_data, beam);
        } else if (xsf_data.detection_two_way_travel_time != nullptr && dim_beam == dim_detection) {  // reson
            travelTime = xsf_data.detection_two_way_travel_time[beam];
        }
    }
    // interpolate travel time if detection is invalid or absent
    if (travelTime <= 0.f && !swath_data.detectionsPerSectorPerAntenna.empty()) {
        DetectionInterpolator& detectionIt =
            swath_data.detectionsPerSectorPerAntenna[beam_data.tx_sector * swath_data.antennaCount + beam_data.rx_transducer];
        if (detectionIt.size() > 0) {
            DetectionPoint point = detectionIt.getInterpolatedValue((float)toRadians(beam_data.rx_beam_rotation_phi));
            travelTime = point.travelTime;
        }
    }
    // deduce bottom range index
    if (travelTime > 0.f) beam_data.bottomRangeIndex = compute_bottom_range_index(xsf_data, swath_data, travelTime, beam);

    if (beam > 0 && !antennaChanged) {
        beam_data.non_overlapping_opening_angle = fabs(xsf_data.rx_beam_rotation_phi[beam] - xsf_data.rx_beam_rotation_phi[beam - 1]);
    } else if (dim_beam > beam + 1) {
        beam_data.non_overlapping_opening_angle = fabs(xsf_data.rx_beam_rotation_phi[beam + 1] - xsf_data.rx_beam_rotation_phi[beam]);
    } else {
        beam_data.non_overlapping_opening_angle = beam_data.beam_opening_across;
    }
    beam_data.non_overlapping_opening_angle *= (float)eqCelerityRatio;
}

// Computes the value of the swath_data's attribute sphere_range (in case of specular filtering)
void XsfSpatializer::compute_sphere_range(const XsfData& xsf_data, SwathData& swath_data) {
    if (xsf_data.detected_bottom_range != nullptr) {
        const size_t beam_count = get_beam_count();
        for (int beam = 0; beam < beam_count; beam++) {
            float travelTime = compute_two_way_travel_time(xsf_data, swath_data, beam);
            if (travelTime > 0.f) {
                int bottomRangeIndex = compute_bottom_range_index(xsf_data, swath_data, travelTime, beam);
                if (bottomRangeIndex > 0)  // if valid
                {
                    swath_data.sphere_range = min(swath_data.sphere_range, bottomRangeIndex);
                }
            }
        }
    } else if (xsf_data.detection_two_way_travel_time != nullptr && get_beam_count() == get_detection_count()) {  // reson
        const size_t beam_count = get_beam_count();
        for (int beam = 0; beam < beam_count; beam++) {
            float travelTime = xsf_data.detection_two_way_travel_time[beam];
            if (travelTime > 0.f) {
                int bottomRangeIndex = compute_bottom_range_index(xsf_data, swath_data, travelTime, beam);
                if (bottomRangeIndex > 0)  // if valid
                {
                    swath_data.sphere_range = min(swath_data.sphere_range, bottomRangeIndex);
                }
            }
        }
    }
}

// Computes the value of the swath_data's attribute max_backscatter_per_rank (in case of side lobe filtering)
void XsfSpatializer::compute_median_backscatter_per_rank(XsfData& xsf_data, SwathData& swath_data, bool force) {
    const size_t beam_count = get_beam_count();
    // get max vlen
    size_t max_vlen = 0;
    for (int beam = 0; beam < beam_count; beam++) {
        nc_vlen_t& beam_backscatter_r = (*xsf_data.swath_backscatter_r)[beam];
        if (beam_backscatter_r.len > max_vlen) {
            max_vlen = beam_backscatter_r.len;
        }
    }
    if (max_vlen == 0) return;

    std::vector<std::vector<float>> backscatter_stats(swath_data.sectorCount * max_vlen);
    for (int rank = 0; rank < backscatter_stats.size(); rank++) {
        backscatter_stats[rank].reserve(beam_count);
    }
    for (int beam = 0; beam < beam_count; beam++) {
        int tx_sector = (xsf_data.transmit_beam_index != nullptr && xsf_data.transmit_beam_index[beam] >= 0) ? xsf_data.transmit_beam_index[beam] : 0;
        nc_vlen_t& beam_backscatter_r = (*xsf_data.swath_backscatter_r)[beam];
        for (int rank = 0; rank < beam_backscatter_r.len; rank++) {
            float backscatter_r_value = 0.f;
            if (xsf_data.backscatter_r_type == NC_SHORT) {
                short* backscatter_r_value_ptr = (short*)beam_backscatter_r.p + rank;
                backscatter_r_value = *backscatter_r_value_ptr;
            } else if (xsf_data.backscatter_r_type == NC_BYTE) {
                // Case of XSF
                char* backscatter_r_value_ptr = (char*)beam_backscatter_r.p + rank;
                backscatter_r_value = *backscatter_r_value_ptr;
            } else if (xsf_data.backscatter_r_type == NC_FLOAT) {
                // Case of EK80
                float* backscatter_r_value_ptr = (float*)beam_backscatter_r.p + rank;
                backscatter_r_value = *backscatter_r_value_ptr;
            }

            if (backscatter_r_value != xsf_data.backscatter_r_missing_value) {
                float current = xsf_data.backscatter_r_scale * backscatter_r_value + xsf_data.backscatter_r_offset;
                // TODO : remove this conversion with complient Reson files
                current = convertTodB(current);
                if (!isnan(current)) {
                    backscatter_stats[tx_sector + rank * swath_data.sectorCount].push_back(current);
                }
            }
        }
    }
    // apply stats median
    for (int rank = 0; rank < backscatter_stats.size(); rank++) {
        std::sort(backscatter_stats[rank].begin(), backscatter_stats[rank].end());
    }

    if (force || signal_processing.get_range_normalization_parameter().enable) {
        float quantile = 0.5f;
        swath_data.normalization_ref_backscatter_per_rank_per_sector.resize(backscatter_stats.size(), NAN);
        for (int rank = 0; rank < backscatter_stats.size(); rank++) {
            if (!backscatter_stats[rank].empty()) {
                swath_data.normalization_ref_backscatter_per_rank_per_sector[rank] =
                    backscatter_stats[rank][(size_t)((backscatter_stats[rank].size() - 1) * quantile)];
            }
        }
        std::vector<float> median_backscatter = swath_data.normalization_ref_backscatter_per_rank_per_sector;
        std::sort(median_backscatter.begin(), median_backscatter.begin() + swath_data.sectorCount * swath_data.sphere_range);
        // get median of the computed quantiles above specular sphere
        swath_data.normalization_ref_backscatter = median_backscatter[(size_t)((swath_data.sectorCount * swath_data.sphere_range - 1) * 0.5f)];
    }

    if (echo_filter.get_side_lobe_parameter().enable) {
        float quantile = echo_filter.get_side_lobe_parameter().quantile;
        swath_data.sidelobe_ref_backscatter_per_rank_per_sector.resize(swath_data.sectorCount * max_vlen, NAN);
        for (int rank = 0; rank < backscatter_stats.size(); rank++) {
            if (!backscatter_stats[rank].empty()) {
                swath_data.sidelobe_ref_backscatter_per_rank_per_sector[rank] =
                    backscatter_stats[rank][(size_t)((backscatter_stats[rank].size() - 1) * quantile)];
            }
        }
    }
}

float XsfSpatializer::convertTodB(float value) {
    if (xsf_driver.isReson()) {
        return amplitudeTodB(value - signal_processing.get_reson_offset_parameter().offset + 1.f);
    }
    return value;
}

// Fills the values of the object SwathData
void XsfSpatializer::init_swath_data(XsfData& xsf_data, int swath, SwathData& swath_data) {
    swath_data.swath = swath;
    swath_data.platform_longitude = xsf_data.platform_longitude[swath];
    swath_data.platform_latitude = xsf_data.platform_latitude[swath];
    swath_data.platform_heading = xsf_data.platform_heading[swath];
    swath_data.platform_roll = xsf_data.platform_roll[swath];
    swath_data.platform_pitch = xsf_data.platform_pitch[swath];
    swath_data.ping_time = xsf_data.ping_time[swath];
    swath_data.sample_interval = xsf_data.sample_interval[swath];
    swath_data.sound_speed_at_transducer = xsf_data.sound_speed_at_transducer[swath];
    swath_data.tx_transducer_depth = xsf_data.tx_transducer_depth != nullptr ? xsf_data.tx_transducer_depth[swath] : 0.0f;
    swath_data.detectionBeamStabilized = xsf_data.detection_beam_stabilisation != nullptr ? xsf_data.detection_beam_stabilisation[swath] : false;
    swath_data.antennaCount = get_transducer_count();
    swath_data.sectorCount = get_sector_count();

    // swath_data.sphere_range used only with threshold sphere filters
    swath_data.sphere_range = (int)(*xsf_data.swath_backscatter_r)[0].len;
    if (echo_filter.get_specular_parameter().enable || signal_processing.get_range_normalization_parameter().enable) {
        compute_sphere_range(xsf_data, swath_data);
    }

    // swath_data.sphere_range used only with threshold sphere filters
    if (echo_filter.get_side_lobe_parameter().enable || signal_processing.get_range_normalization_parameter().enable) {
        compute_median_backscatter_per_rank(xsf_data, swath_data);
    }
}

void XsfSpatializer::compute_swath_data(XsfData& xsf_data, SwathData& swath_data) {
    // Compute swath data to guess beam positions (equivalent tilt angle / equivalent celerity / detection positions ...)

    // Store equivalent tilt angles per sector
    vector<vector<double>> eqTiltAngleRad(swath_data.sectorCount);
    // Compute equivalent celerity over surface celerity
    vector<double> eqCelerityOverSurfaceRatio;

    if (xsf_driver.has_bathymetry()) {
        size_t detection_dim = get_detection_count();
        // Transducers rotations
        vector<Matrix3d> rotA2V(get_transducer_count());
        for (int i = 0; i < rotA2V.size(); i++) {
            double rollOffset = xsf_data.transducer_rotation_x[i];
            double pitchOffset = xsf_data.transducer_rotation_y[i];
            double yawOffset = xsf_data.transducer_rotation_z[i];

            rotA2V[i] = AngleAxisd(toRadians(yawOffset), Vector3d::UnitZ()) * AngleAxisd(toRadians(pitchOffset), Vector3d::UnitY()) *
                        AngleAxisd(toRadians(rollOffset), Vector3d::UnitX()) * Matrix3d::Identity();
        }

        // prepare detection interpolators (also used for bottom detection)
        swath_data.detectionsPerSectorPerAntenna.resize(swath_data.sectorCount * swath_data.antennaCount);

        if (SpatializationMethod::EquivalentCelerity == method) {
            // prepare equivalent celerity stats holders
            eqCelerityOverSurfaceRatio.reserve(detection_dim);
            for (auto& vec : eqTiltAngleRad) vec.reserve(detection_dim);
        }
        LinearInterpolator rollInterpolator(xsf_data.attitude_time, xsf_data.attitude_roll, get_attitude_count());
        LinearInterpolator pitchInterpolator(xsf_data.attitude_time, xsf_data.attitude_pitch, get_attitude_count());
        for (int beam = 0; beam < detection_dim; beam++) {
            if (xsf_data.status[beam] == 0) {  // Check validity
                int rxTransducerIndex = xsf_data.detection_rx_transducer_index != nullptr && xsf_data.detection_rx_transducer_index[beam] >= 0
                                            ? xsf_data.detection_rx_transducer_index[beam]
                                            : xsf_data.default_rx_transducer_index;

                int txTransducerIndex = xsf_data.detection_tx_transducer_index != nullptr && xsf_data.detection_tx_transducer_index[beam] >= 0
                                            ? xsf_data.detection_tx_transducer_index[beam]
                                            : xsf_data.default_tx_transducer_index;

                double txHeadingOffsetRad = toRadians(xsf_data.transducer_rotation_z[txTransducerIndex]);
                double along = xsf_data.detection_x[beam];
                double across = xsf_data.detection_y[beam];
                double depth = xsf_data.detection_z[beam];
                short txSector = xsf_data.detection_tx_beam[beam];

                float travelTime = 0.f;
                if (xsf_data.detection_two_way_travel_time != nullptr && !isnan(xsf_data.detection_two_way_travel_time[beam])) {
                    travelTime = xsf_data.detection_two_way_travel_time[beam];
                }

                Matrix3d rotACS = AngleAxisd(toRadians(xsf_data.detection_beam_pointing_angle[beam]), Vector3d::UnitX()) * Matrix3d::Identity();
                float roll = swath_data.platform_roll;
                float pitch = swath_data.platform_pitch;
                if (travelTime > 0.f) {
                    roll = rollInterpolator.getInterpolatedValue(swath_data.ping_time + toNanoseconds(travelTime));
                    pitch = pitchInterpolator.getInterpolatedValue(swath_data.ping_time + toNanoseconds(travelTime));
                }
                Matrix3d rotV2S =
                    AngleAxisd(toRadians(pitch), Vector3d::UnitY()) * AngleAxisd(toRadians(roll), Vector3d::UnitX()) * Matrix3d::Identity();

                double pointingAngleEarthRad;
                if (xsf_driver.isReson()) {
                    pointingAngleEarthRad = toRadians(xsf_data.rx_beam_rotation_phi[beam]);
                } else {
                    if (isnan(xsf_data.detection_beam_pointing_angle[beam])) continue;

                    if (swath_data.detectionBeamStabilized) {
                        // ME70
                        pointingAngleEarthRad = toRadians(xsf_data.detection_beam_pointing_angle[beam]);
                    } else {
                        Matrix3d rotVCS = rotA2V[rxTransducerIndex] * rotACS;
                        Matrix3d rotSCS = rotV2S * rotVCS;

                        // get extrinsic xAxis angles
                        pointingAngleEarthRad = rotSCS.eulerAngles(0, 1, 2)[0];
                    }
                }

                Vector3d transducerOffset =
                    rotV2S * Vector3d(xsf_data.transducer_offset_x[rxTransducerIndex], xsf_data.transducer_offset_y[rxTransducerIndex],
                                      xsf_data.transducer_offset_z[rxTransducerIndex]);
                double alongOffset = transducerOffset[0];
                double acrossOffset = transducerOffset[1];
                double depthOffset = transducerOffset[2];
                double x = across - acrossOffset;
                double y = along - alongOffset;
                double z = depth - depthOffset;

                DetectionPoint point;
                point.across = (float)x;
                point.along = (float)y;
                point.depth = (float)z;
                point.travelTime = travelTime;
                swath_data.detectionsPerSectorPerAntenna[txSector * swath_data.antennaCount + rxTransducerIndex].emplace(
                    std::make_pair((float)tan(pointingAngleEarthRad), point));

                if (SpatializationMethod::EquivalentCelerity == method) {
                    if (abs(pointingAngleEarthRad) > 0.0) {
                        double rangeBathyMeterRefRx = sqrt(x * x + y * y + z * z);
                        // compute an equivalent tilt angle between Rx Antenna and point detection instead of
                        // computing the two planes intersection between Tx and Rx

                        // find parameters to fit y = a*x + b
                        double a = tan(txHeadingOffsetRad);
                        double b = y - a * x;
                        // cos(heading) = h / b  where h is projected orthogonal distance from the origin to the fitted line
                        double h = b * cos(txHeadingOffsetRad);
                        double equivalentTiltAngleRad = atan2(h, z);
                        eqTiltAngleRad[txSector].push_back(equivalentTiltAngleRad);

                        double sinEqPointingAngleRad = x / rangeBathyMeterRefRx;
                        double equivalentCelerityRatio = abs(sinEqPointingAngleRad / sin(pointingAngleEarthRad));
                        eqCelerityOverSurfaceRatio.push_back(equivalentCelerityRatio);
                    }
                }
            }
        }
    }

    // fill swath data with computed stats
    swath_data.equivalentCelerityRatio = 1.0;
    if (!eqCelerityOverSurfaceRatio.empty()) {
        // find median
        std::sort(eqCelerityOverSurfaceRatio.begin(), eqCelerityOverSurfaceRatio.end());
        swath_data.equivalentCelerityRatio = eqCelerityOverSurfaceRatio[eqCelerityOverSurfaceRatio.size() / 2];
        // check max possible celerity ratio value
        size_t dim_rx_beam = xsf_driver.get_dim_beam();
        const std::pair<float*, float*> minmaxPointingAngle =
            std::minmax_element(xsf_data.rx_beam_rotation_phi, xsf_data.rx_beam_rotation_phi + dim_rx_beam);
        double maxPointingAngle = toRadians(max(abs(*minmaxPointingAngle.first), abs(*minmaxPointingAngle.second)));
        static const double maxAdmissibleSinAngle = sin(toRadians(85.0));
        swath_data.equivalentCelerityRatio = min(swath_data.equivalentCelerityRatio, maxAdmissibleSinAngle / abs(sin(maxPointingAngle)));
    }

    swath_data.equivalentTiltAngleRadPerSector.resize(swath_data.sectorCount);
    for (int txBeam = 0; txBeam < swath_data.sectorCount; txBeam++) {
        if (!eqTiltAngleRad[txBeam].empty()) {
            std::sort(eqTiltAngleRad[txBeam].begin(), eqTiltAngleRad[txBeam].end());
            swath_data.equivalentTiltAngleRadPerSector[txBeam] = eqTiltAngleRad[txBeam][eqTiltAngleRad[txBeam].size() / 2];
        } else {
            int txTransducerIndex = xsf_data.transmit_transducer_index != nullptr && xsf_data.transmit_transducer_index[txBeam] >= 0
                                        ? xsf_data.transmit_transducer_index[txBeam]
                                        : xsf_data.default_tx_transducer_index;
            double txHeadingOffsetRad = toRadians(xsf_data.transducer_rotation_z[txTransducerIndex]);
            double tanTxTiltAngle = tan(toRadians(xsf_data.tx_beam_rotation_theta[txBeam])) * cos(txHeadingOffsetRad);
            swath_data.equivalentTiltAngleRadPerSector[txBeam] = atan(tanTxTiltAngle);
        }
    }
}

struct BeamEchosIterator {
    BeamEchosIterator() { reset(); }
    BeamEchos beam_echos;
    // Direct access to vectors data for performance purposes (much better than array index access)
    double *longitude_ptr, *latitude_ptr;
    float *across_ptr, *along_ptr, *beam_opening_along_ptr, *height_ptr, *elevation_ptr, *echo_ptr, *beam_opening_across_ptr,
        *non_overlapping_beam_opening_angle_ptr;

    void reset() {
        // Reset buffers
        beam_echos.size = 0;
        longitude_ptr = beam_echos.longitude.data();
        latitude_ptr = beam_echos.latitude.data();
        across_ptr = beam_echos.across.data();
        along_ptr = beam_echos.along.data();
        beam_opening_along_ptr = beam_echos.beam_opening_along.data();
        height_ptr = beam_echos.height.data();
        elevation_ptr = beam_echos.elevation.data();
        echo_ptr = beam_echos.echo.data();
        beam_opening_across_ptr = beam_echos.beam_opening_across.data();
        non_overlapping_beam_opening_angle_ptr = beam_echos.non_overlapping_beam_opening_angle.data();
    }
    void accept(EchoData& echo) {
        // Set resulting values for the echo
        *across_ptr = echo.across;
        *along_ptr = echo.along;
        *beam_opening_across_ptr = echo.beam_opening_across;
        *beam_opening_along_ptr = echo.beam_opening_along;
        *height_ptr = echo.height;
        *elevation_ptr = echo.elevation;
        *latitude_ptr = echo.latitude;
        *longitude_ptr = echo.longitude;
        *echo_ptr = echo.value;
        *non_overlapping_beam_opening_angle_ptr = echo.non_overlapping_beam_opening_angle;

        // Progress in buffers
        longitude_ptr++, latitude_ptr++, across_ptr++, along_ptr++, beam_opening_across_ptr++, beam_opening_along_ptr++, height_ptr++,
            elevation_ptr++, echo_ptr++, non_overlapping_beam_opening_angle_ptr++, beam_echos.size++;
    }
};

void XsfSpatializer::_perform(int swath, XsfData& xsf_data, SwathData& swath_data, int first_beam, int last_beam, BeamEchoCallback& callback) {
    Spatializer spatializer(this->method);
    BeamEchosIterator beamEchosItr = BeamEchosIterator();

    size_t dim_tx_beam = xsf_driver.get_dim_tx_beam();
    size_t dim_beam = xsf_driver.get_dim_beam();

    //
    BeamData beam_data;
    EchoData echo_data;
    //

    spatializer.configure_swath_level(swath_data);

    int sampling = echo_filter.get_sampling_threshold();
    for (int beam = first_beam; beam < last_beam; beam++) {
        nc_vlen_t& beam_backscatter_r = (*xsf_data.swath_backscatter_r)[beam];
        if (beam_backscatter_r.len == 0) continue;

        init_beam_data(xsf_data, beam, swath_data, beam_data);
        spatializer.configure_beam_level(beam_data);

        echo_data.beam_opening_across = beam_data.beam_opening_across;
        echo_data.beam_opening_along = beam_data.beam_opening_along;
        echo_data.non_overlapping_beam_opening_angle = beam_data.non_overlapping_opening_angle;

        // Applying the range index filter
        int first_rank = 0;
        int last_rank = beam_backscatter_r.len;
        api::SampleIndexParameter sample_index = echo_filter.get_sample_index_parameter();
        if (sample_index.enable) {
            first_rank = sample_index.min;
            if (sample_index.max != INT_MAX) {
                last_rank = min(last_rank, sample_index.max + 1);
            }
        }
        // Iterator of backscatter_r values
        for (int rank = first_rank; rank < last_rank; rank += sampling) {
            float backscatter_r_value = 0;
            if (xsf_data.backscatter_r_type == NC_SHORT) {
                short* backscatter_r_value_ptr = (short*)beam_backscatter_r.p + rank;
                backscatter_r_value = *backscatter_r_value_ptr;
            } else if (xsf_data.backscatter_r_type == NC_BYTE) {
                // Case of XSF
                char* backscatter_r_value_ptr = (char*)beam_backscatter_r.p + rank;
                backscatter_r_value = *backscatter_r_value_ptr;
            } else if (xsf_data.backscatter_r_type == NC_FLOAT) {
                // Case of EK80
                float* backscatter_r_value_ptr = (float*)beam_backscatter_r.p + rank;
                backscatter_r_value = *backscatter_r_value_ptr;
            }

            if (backscatter_r_value != xsf_data.backscatter_r_missing_value) {
                echo_data.rank = rank;
                echo_data.rawValue = xsf_data.backscatter_r_scale * backscatter_r_value + xsf_data.backscatter_r_offset;
                // TODO : remove this conversion with complient Reson files
                echo_data.rawValue = convertTodB(echo_data.rawValue);

                signal_processing.process_echo(swath_data, beam_data, echo_data);

                // Check the echo before calling the spatialization
                if (echo_filter.inspects_echo(swath_data, beam_data, echo_data)) {
                    spatializer.spatialize(echo_data);
                    // if (std::signbit(echo_data.across) != std::signbit(beam_data.rx_transducer_offset_y)) continue;
                    //  Able to compute the location ? No when heading is NaN for example
                    if (!std::isnan(echo_data.latitude) && !std::isnan(echo_data.longitude)  //
                        && echo_filter.inspects_localized_echo(echo_data))                   // Applying other filters
                    {
                        // Echo is good
                        echo_data.height *= sampling;
                        beamEchosItr.accept(echo_data);
                        if (beamEchosItr.beam_echos.size == BEAM_ECHO_PER_CALLBACK) {
                            callback(beamEchosItr.beam_echos);
                            beamEchosItr.reset();
                        }
                    }
                }
            }
        }
    }

    // Callback
    if (beamEchosItr.beam_echos.size != 0) {
        callback(beamEchosItr.beam_echos);
    }
}

size_t XsfSpatializer::estimate_beam_echo_count(size_t from_swath, size_t swath_count) {
    size_t result = 0;
    size_t last_swath = min(from_swath + swath_count, xsf_driver.get_dim_ping_time());
    for (size_t swath_idx = from_swath; swath_idx < last_swath; swath_idx++) {
        std::vector<nc_vlen_t>& backscatter_r = *xsf_driver.load_vlen_backscatter_r((int)swath_idx);
        for (int beam = 0; beam < xsf_driver.get_dim_beam(); beam++) {
            result += backscatter_r[beam].len;
        }
    }
    LOG4CPP_DEBUG_S(logger) << "XSF Spatializer, estimated beam echoes " << result;

    return result;
}

float XsfSpatializer::estimate_normalization_ref_level(size_t from_swath, size_t swath_count) {
    float result = 0.f;

    std::vector<float> refValues;
    refValues.reserve(swath_count);
    XsfData xsf_data = XsfData();
    int last_swath = (int)min(from_swath + swath_count, xsf_driver.get_dim_ping_time());
    for (int swath_idx = from_swath; swath_idx < last_swath; swath_idx++) {
        xsf_data.load(xsf_driver, swath_idx);
        SwathData swath_data;
        init_swath_data(xsf_data, swath_idx, swath_data);
        if (swath_data.normalization_ref_backscatter_per_rank_per_sector.empty()) {
            compute_sphere_range(xsf_data, swath_data);
            compute_median_backscatter_per_rank(xsf_data, swath_data, true);
        }
        refValues.push_back(swath_data.normalization_ref_backscatter);
    }
    std::sort(refValues.begin(), refValues.end());
    result = refValues[(refValues.size() - 1) / 2];
    LOG4CPP_DEBUG_S(logger) << "XSF Spatializer, estimated normalization ref level" << result;

    return result;
}
void XsfSpatializer::change_beam_group_number(int other_beam_group_number) { xsf_driver.change_beam_group_number(other_beam_group_number); }

void XsfSpatializer::perform_for_swaths(int from_swath, int swath_count, BeamEchoCallback& callback) {
    LOG4CPP_DEBUG_S(logger) << "XSF Spatializer, computing beam echo for " << swath_count << " swaths...";

    XsfData xsf_data = XsfData();
    int last_swath = min(from_swath + swath_count, (int)xsf_driver.get_dim_ping_time());
    for (int swath_idx = from_swath; swath_idx < last_swath; swath_idx++) {
        xsf_data.load(xsf_driver, swath_idx);
        perform(swath_idx, xsf_data, callback);
    }
}

void XsfSpatializer::perform_all_swath(BeamEchoCallback& callback) { return perform_for_swaths(0, (int)xsf_driver.get_dim_ping_time(), callback); }

void XsfSpatializer::close() { xsf_driver.close(); }

size_t XsfSpatializer::get_swath_count() { return xsf_driver.get_dim_ping_time(); }

size_t XsfSpatializer::get_beam_count() { return xsf_driver.get_dim_beam(); }

size_t XsfSpatializer::get_detection_count() { return xsf_driver.get_dim_detection(); }

size_t XsfSpatializer::get_sector_count() { return xsf_driver.get_dim_tx_beam(); }

size_t XsfSpatializer::get_transducer_count() { return xsf_driver.get_dim_transducer(); }

size_t XsfSpatializer::get_attitude_count() { return xsf_driver.get_dim_attitude_time(); }

EchoFilter& XsfSpatializer::get_filter() { return echo_filter; }

SignalProcessing& XsfSpatializer::get_signal_processing() { return signal_processing; }

void XsfSpatializer::set_multi_thread(bool multi_thread) { this->multi_thread = multi_thread; }
void XsfSpatializer::set_spatialization_method(SpatializationMethod method) { this->method = method; }
}  // namespace xsf
}  // namespace sonarnative
