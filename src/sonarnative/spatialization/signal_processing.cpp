#include "sonarnative/spatialization/signal_processing.hpp"

#include "sonarnative/utils/conversion_utils.hpp"  // toRadians

namespace sonarnative {
namespace spatialization {

using namespace sonarnative::api;

void SignalProcessing::apply(ResonOffsetParameter parameter) noexcept { reson_offset_parameter = parameter; }
void SignalProcessing::apply(RangeNormalizationParameter parameter) noexcept { range_normalization_parameter = parameter; }

void SignalProcessing::process_echo(SwathData& swath_data, BeamData& beam_data, EchoData& echo) {
    echo.value = echo.rawValue;
    if (range_normalization_parameter.enable)  // compensation
    {
        size_t rankSectorIndex = echo.rank * swath_data.sectorCount + beam_data.tx_sector;
        float swath_ref = swath_data.normalization_ref_backscatter;
        float rank_ref = swath_data.normalization_ref_backscatter_per_rank_per_sector[rankSectorIndex];
        float target_ref = range_normalization_parameter.offset;
        //if (rank_ref > swath_ref) 
        //    echo.value += swath_ref - rank_ref;
        //echo.value += target_ref - swath_ref;
        echo.value += target_ref - rank_ref;
    }
}
}  // namespace spatialization
}  // namespace sonarnative
