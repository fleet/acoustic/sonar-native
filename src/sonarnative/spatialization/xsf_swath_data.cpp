#include "sonarnative/spatialization/xsf_swath_data.hpp"

#include "log4cpp/Category.hh"
#include "log4cpp/convenience.h"

namespace sonarnative {
namespace xsf {

// Logger definition
LOG4CPP_LOGGER("sonarnative.xsf_swath_data");

void XsfData::load(XsfDriver& xsf_driver, int swath) {
    LOG4CPP_DEBUG_S(logger) << "XSF Spatializer, loading swath " << swath;

    // Retrieve xsf_data of group Attitude
    if (xsf_driver.has_attitude()) {
        attitude_roll = xsf_driver.read_var_attitude_roll();
        attitude_pitch = xsf_driver.read_var_attitude_pitch();
        attitude_time = xsf_driver.read_var_attitude_time();
    }

    // Retrieve xsf_data of group Beam_group1
    platform_longitude = xsf_driver.read_var_platform_longitude();
    platform_latitude = xsf_driver.read_var_platform_latitude();
    platform_heading = xsf_driver.read_var_platform_heading();
    platform_pitch = xsf_driver.read_var_platform_pitch();
    platform_roll = xsf_driver.read_var_platform_roll();
    ping_time = xsf_driver.read_var_ping_time();
    sample_interval = xsf_driver.read_var_sample_interval();
    sound_speed_at_transducer = xsf_driver.read_var_sound_speed_at_transducer();
    if (xsf_driver.has_var_tx_transducer_depth()) {
        tx_transducer_depth = xsf_driver.read_var_tx_transducer_depth();
    }
    if (xsf_driver.has_var_multiping_sequence()) {
        multiping_sequence = xsf_driver.read_var_multiping_sequence();
    }

    transducer_offset_x = xsf_driver.read_var_transducer_offset_x();
    transducer_offset_y = xsf_driver.read_var_transducer_offset_y();
    transducer_offset_z = xsf_driver.read_var_transducer_offset_z();
    transducer_rotation_x = xsf_driver.read_var_transducer_rotation_x();
    transducer_rotation_y = xsf_driver.read_var_transducer_rotation_y();
    transducer_rotation_z = xsf_driver.read_var_transducer_rotation_z();
    beamwidth_receive_major = xsf_driver.read_var_beamwidth_receive_major();
    beamwidth_transmit_minor = xsf_driver.read_var_beamwidth_transmit_minor();

    default_rx_transducer_index = xsf_driver.getRxTransducers()[0];
    default_tx_transducer_index = xsf_driver.getTxTransducers()[0];

    // Retrieve xsf_data for a unique swath
    rx_beam_rotation_phi = xsf_driver.read_var_rx_beam_rotation_phi(swath, 1);
    tx_beam_rotation_theta = xsf_driver.read_var_tx_beam_rotation_theta(swath, 1);
    if (xsf_driver.has_var_receive_transducer_index()) {
        receive_transducer_index = xsf_driver.read_var_receive_transducer_index();
    }
    if (xsf_driver.has_var_transmit_transducer_index()) {
        transmit_transducer_index = xsf_driver.read_var_transmit_transducer_index(swath, 1);
    }
    if (xsf_driver.has_var_transmit_beam_index()) {
        transmit_beam_index = xsf_driver.read_var_transmit_beam_index(swath, 1);
    }
    if (xsf_driver.has_bathymetry()) {
        detection_tx_beam = xsf_driver.read_var_detection_tx_beam(swath, 1);
        detection_rx_transducer_index = xsf_driver.read_var_detection_rx_transducer_index(swath, 1);
        detection_tx_transducer_index = xsf_driver.read_var_detection_tx_transducer_index(swath, 1);
        status = xsf_driver.read_var_status(swath, 1);
        detection_x = xsf_driver.read_var_detection_x(swath, 1);
        detection_y = xsf_driver.read_var_detection_y(swath, 1);
        detection_z = xsf_driver.read_var_detection_z(swath, 1);
        detection_beam_pointing_angle = xsf_driver.read_var_detection_beam_pointing_angle(swath, 1);
    }
    if (xsf_driver.has_var_detection_beam_stabilisation()) {
        detection_beam_stabilisation = xsf_driver.read_var_detection_beam_stabilisation();
    }
    if (xsf_driver.has_var_detected_bottom_range()) {
        detected_bottom_range = xsf_driver.read_var_detected_bottom_range(swath, 1);
        blanking_interval = xsf_driver.read_var_blanking_interval(swath, 1);
        sample_time_offset = xsf_driver.read_var_sample_time_offset();
    }
    if (xsf_driver.has_var_detection_two_way_travel_time()) {
        detection_two_way_travel_time = xsf_driver.read_var_detection_two_way_travel_time(swath, 1);
        blanking_interval = xsf_driver.read_var_blanking_interval(swath, 1);
        sample_time_offset = xsf_driver.read_var_sample_time_offset();
    }

    swath_backscatter_r = xsf_driver.load_vlen_backscatter_r(swath);
    backscatter_r_scale = xsf_driver.get_backscatter_r_scale();
    backscatter_r_offset = xsf_driver.get_backscatter_r_offset();
    backscatter_r_type = xsf_driver.get_backscatter_r_type();
    backscatter_r_missing_value = xsf_driver.get_backscatter_r_missing_value();
}
}  // namespace xsf
}  // namespace sonarnative
