#include <sonarnative/sample/sample.hpp>

#include <log4cpp/convenience.h>
#include <log4cpp/Category.hh>

namespace sonarnative {
    


	// Logger definition
	LOG4CPP_LOGGER("sonarnative.Sample")

	Sample::Sample(std::string name): m_name(name) {} 
			
	std::string Sample::name()
	{
		LOG4CPP_INFO(logger, "Entering name accessor");
		return m_name;
	}

	void Sample::count(int from, int to, ProgressCallback* callback)
	{
		LOG4CPP_INFO_S(logger) << "Entering count from " << from << " to " << to;
		for (int i = from; i <= to; i++) {
			std::string msg = "Step ";
			msg.append(std::to_string(i));
			double runningProgress = (double) ((int64_t) i - from) / (double) ((int64_t) to - from);

			LOG4CPP_INFO_S(logger) << "count progression " << runningProgress;
			callback->progress(runningProgress, msg);
		}
		LOG4CPP_INFO_S(logger) << "End of count ";
	}

}
