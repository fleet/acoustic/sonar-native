#include "sonarnative/driver/xsf_driver.hpp"

#include "log4cpp/Category.hh"
#include "log4cpp/convenience.h"
#include "sonarnative/utils/file/netcdf_utils.hpp"

namespace sonarnative::xsf {

// Logger definition
LOG4CPP_LOGGER("sonarnative.xsf_driver");

static const std::string GRP_NAME_BEAM_GROUP = "Sonar/Beam_group";

XsfDriver::XsfDriver(std::string xsf_path, int ncid) : xsf_path(xsf_path), vlen_backscatter_r(VlenUniquePtr(nullptr, nc_delete_vlen)) {
    reset();
    file_id = ncid;
    LOG4CPP_INFO_S(logger) << "XSF Spatializer created."
                           << " xsf_path=" << xsf_path << " ncid=" << ncid;
}
void XsfDriver::reset() noexcept {
    file_id = -1;
    std::fill(nc_grp_ids.begin(), nc_grp_ids.end(), -1);
    std::fill(nc_var_ids.begin(), nc_var_ids.end(), -1);
    std::fill(nc_dims.begin(), nc_dims.end(), -1);

    // Free memory
    for (int i = 0; i < VAR_IDX_NUM; i++) {
        var_values[i].reset(nullptr);
    }
}

XsfDriver::XsfDriver(XsfDriver&& to_move) noexcept : vlen_backscatter_r(std::move(to_move.vlen_backscatter_r)) {
    xsf_path = to_move.xsf_path;
    bathymetry_available = to_move.bathymetry_available;
    file_id = to_move.file_id;
    nc_grp_ids = to_move.nc_grp_ids;
    nc_dims = to_move.nc_dims;
    nc_var_ids = to_move.nc_var_ids;
    var_values = std::move(to_move.var_values);
}

int XsfDriver::open() {
    if (file_id == -1) {
        file_id = nc_open_file(xsf_path.c_str());
        is_internally_open = true;
    }

    // check constructor
    readConstructor();

    // init transducer info
    readTransducers();

    // By default, work with Sonar/Beam_group1 and Sonar/Beam_group1/Bathymetry groups
    change_beam_group_number(1);

    return file_id;
}

void XsfDriver::close() {
    if (file_id != -1 && is_internally_open) {
        nc_close_quietly(file_id);
    }
    is_internally_open = false;
    file_id = -1;
}

int XsfDriver::get_file_id() { return file_id; }

int XsfDriver::get_beam_group_number() { return beam_group_number; }

bool XsfDriver::change_beam_group_number(int other_beam_group_number) {
    if (beam_group_number != other_beam_group_number) {
        // Test if Beam_group[other_beam_group_number] exists
        std::string other_beam_group_name = GRP_NAME_BEAM_GROUP + std::to_string(other_beam_group_number);
        int other_beam_group_id = nc_get_group(this->file_id, other_beam_group_name);
        if (other_beam_group_id != -1) {
            beam_group_number = other_beam_group_number;
            nc_grp_ids[GRP_IDX_CURRENT_BEAM_GROUP] = other_beam_group_id;
            GRP_NAMES[GRP_IDX_CURRENT_BEAM_GROUP] = other_beam_group_name;
            GRP_NAMES[GRP_IDX_BATHYMETRY] = other_beam_group_name + "/Bathymetry";

            // Test if bathimetric group and variable are available
            bathymetry_available = false;
            nc_grp_ids[GRP_IDX_BATHYMETRY] = nc_get_group_quietly(this->file_id, this->GRP_NAMES.at(GRP_IDX_BATHYMETRY));
            if (nc_grp_ids[GRP_IDX_BATHYMETRY] != -1) {
                bathymetry_available = nc_get_variable_quietly(nc_grp_ids[GRP_IDX_BATHYMETRY], "detection_tx_beam") != -1;
            }

            // Reset variable ids of Sonar/Beam_group
            int beam_group_var_idx[]{VAR_IDX_DETECTION_TX_BEAM,
                                     VAR_IDX_DETECTION_RX_TRANSDUCER_INDEX,
                                     VAR_IDX_DETECTION_TX_TRANSDUCER_INDEX,
                                     VAR_IDX_STATUS,
                                     VAR_IDX_DETECTION_X,
                                     VAR_IDX_DETECTION_Y,
                                     VAR_IDX_DETECTION_Z,
                                     VAR_IDX_DETECTION_BEAM_POINTING_ANGLE,
                                     VAR_IDX_DETECTION_BEAM_STABILISATION,
                                     VAR_IDX_PLATFORM_LATITUDE,
                                     VAR_IDX_PLATFORM_LONGITUDE,
                                     VAR_IDX_PLATFORM_HEADING,
                                     VAR_IDX_PLATFORM_PITCH,
                                     VAR_IDX_PLATFORM_ROLL,
                                     VAR_IDX_SAMPLE_INTERVAL,
                                     VAR_IDX_SOUND_SPEED_AT_TRANSDUCER,
                                     VAR_IDX_RX_BEAM_ROTATION_PHI,
                                     VAR_IDX_TX_BEAM_ROTATION_THETA,
                                     VAR_IDX_BEAMWIDTH_RECEIVE_MAJOR,
                                     VAR_IDX_BEAMWIDTH_TRANSMIT_MINOR,
                                     VAR_IDX_BACKSCATTER_R,
                                     VAR_IDX_TX_TRANSDUCER_DEPTH,
                                     VAR_IDX_RECEIVE_TRANSDUCER_INDEX,
                                     VAR_IDX_TRANSMIT_TRANSDUCER_INDEX,
                                     VAR_IDX_TRANSMIT_BEAM_INDEX,
                                     VAR_IDX_DETECTED_BOTTOM_RANGE,
                                     VAR_IDX_BLANKING_INTERVAL,
                                     VAR_IDX_SAMPLE_TIME_OFFSET,
                                     VAR_IDX_DETECTION_TWO_WAY_TRAVEL_TIME,
                                     VAR_IDX_MULTIPING_SEQUENCE};
            for (int var_idx : beam_group_var_idx) {
                var_values[var_idx].reset(nullptr);
            }

            // Retrieve attitude group for detection interpolation
            if (bathymetry_available) readAttitude();

            LOG4CPP_INFO_S(logger) << "Switched to " << GRP_NAMES[GRP_IDX_CURRENT_BEAM_GROUP];

            return true;
        } else {
        }
        return false;
    }
    return true;  // Same number
}

void XsfDriver::readTransducers() {
    unsigned char* transducer_function = read_var_transducer_function();
    const int num_transducers = (int)get_dim_transducer();
    for (int i = 0; i < num_transducers; i++) {
        if (transducer_function[i] == 0) {
            rx_transducer_idx.push_back(i);
        } else if (transducer_function[i] == 1) {
            tx_transducer_idx.push_back(i);
        } else if (transducer_function[i] == 3) {
            rx_transducer_idx.push_back(i);
            tx_transducer_idx.push_back(i);
        }
    }
}

void XsfDriver::readConstructor() {
    std::string constructor = nc_group_att_string(get_group_id_sonar(), "sonar_manufacturer");
    if (constructor.find("Reson") != std::string::npos) {
        LOG4CPP_INFO_S(logger) << "Sonar manufacturer is Reson";
        is_reson = true;
    }
}

void XsfDriver::readAttitude() { get_preferred_attitude_subgroup_id(); }

int XsfDriver::get_nc_group_id(int group_idx) {
    if (nc_grp_ids[group_idx] == -1) nc_grp_ids[group_idx] = nc_get_group(this->file_id, this->GRP_NAMES.at(group_idx));
    return nc_grp_ids[group_idx];
}
int XsfDriver::get_group_id_sonar() { return get_nc_group_id(GRP_IDX_SONAR); }
int XsfDriver::get_group_id_beam_group() { return get_nc_group_id(GRP_IDX_CURRENT_BEAM_GROUP); }
int XsfDriver::get_group_id_platform() { return get_nc_group_id(GRP_IDX_PLATFORM); }
int XsfDriver::get_group_id_bathymetry() { return get_nc_group_id(GRP_IDX_BATHYMETRY); }

int XsfDriver::get_preferred_attitude_subgroup_id() {
    int preferred_mru = nc_group_att_int(get_group_id_beam_group(), "preferred_MRU");
    std::string mru_id = "none";
    // now retrieve the name of the sensor
    // use netcdf api to ensure that group really exist
    int mruIdsVarId = nc_get_variable_quietly(get_group_id_platform(), "MRU_ids");
    if (mruIdsVarId != -1) {
        std::vector<std::string> mruIds = nc_read_string_var(get_group_id_platform(), mruIdsVarId);
        if (preferred_mru < mruIds.size() && !mruIds[preferred_mru].empty()) mru_id = mruIds[preferred_mru];
    }
    // check position_id
    // position ids are not always well set, we use default value if an error is in file
    // use netcdf api to ensure that group really exist
    int mru_group_id = nc_get_group_quietly(this->file_id, GRP_NAMES[GRP_IDX_ATTITUDE] + "/" + mru_id);
    if (mru_group_id == -1) {
        // we use the first group found as default attitude_id
        int numGrps;
        int attitude_group_id = nc_get_group_quietly(this->file_id, GRP_NAMES[GRP_IDX_ATTITUDE]);
        if (attitude_group_id != -1) {
            // get number of groups
            nc_inq_grps(attitude_group_id, &numGrps, nullptr);
            // get group ids
            std::vector<int> mruIds(numGrps);
            nc_inq_grps(attitude_group_id, &numGrps, mruIds.data());
            mru_group_id = mruIds[0];
        }
    }
    nc_grp_ids[GRP_IDX_ATTITUDE_SUBGROUP] = mru_group_id;
    if (mru_group_id != -1) {
        nc_inq_grpname(mru_group_id, GRP_NAMES[GRP_IDX_ATTITUDE_SUBGROUP].data());
    }
    return nc_grp_ids[GRP_IDX_ATTITUDE_SUBGROUP];
}

size_t XsfDriver::get_dim(int group_idx, int dim_idx) {
    if (nc_dims[dim_idx] == -1) nc_dims[dim_idx] = sonarnative::nc_get_dimension(get_nc_group_id(group_idx), DIM_NAMES.at(dim_idx));
    return nc_dims[dim_idx];
}
size_t XsfDriver::get_dim_ping_time() { return get_dim(GRP_IDX_CURRENT_BEAM_GROUP, DIM_IDX_PING_TIME); }
size_t XsfDriver::get_dim_beam() { return get_dim(GRP_IDX_CURRENT_BEAM_GROUP, DIM_IDX_BEAM); }
size_t XsfDriver::get_dim_tx_beam() { return get_dim(GRP_IDX_CURRENT_BEAM_GROUP, DIM_IDX_TX_BEAM); }
size_t XsfDriver::get_dim_transducer() { return get_dim(GRP_IDX_PLATFORM, DIM_IDX_TRANSDUCER); }
size_t XsfDriver::get_dim_detection() { return has_bathymetry() ? get_dim(GRP_IDX_BATHYMETRY, DIM_IDX_DETECTION) : 0; }
size_t XsfDriver::get_dim_attitude_time() { return get_dim(GRP_IDX_ATTITUDE_SUBGROUP, DIM_IDX_ATTITUDE_TIME); }

int XsfDriver::get_var_id(int group_idx, int var_idx) {
    if (nc_var_ids[var_idx] == -1) nc_var_ids[var_idx] = nc_get_variable(get_nc_group_id(group_idx), VAR_NAMES.at(var_idx));
    return nc_var_ids[var_idx];
}
int XsfDriver::get_var_id_transducer_function() { return get_var_id(GRP_IDX_PLATFORM, VAR_IDX_TRANSDUCER_FUNCTION); }
int XsfDriver::get_var_id_transducer_offset_x() { return get_var_id(GRP_IDX_PLATFORM, VAR_IDX_TRANSDUCER_OFFSET_X); }
int XsfDriver::get_var_id_transducer_offset_y() { return get_var_id(GRP_IDX_PLATFORM, VAR_IDX_TRANSDUCER_OFFSET_Y); }
int XsfDriver::get_var_id_transducer_offset_z() { return get_var_id(GRP_IDX_PLATFORM, VAR_IDX_TRANSDUCER_OFFSET_Z); }
int XsfDriver::get_var_id_transducer_rotation_x() { return get_var_id(GRP_IDX_PLATFORM, VAR_IDX_TRANSDUCER_ROTATION_X); }
int XsfDriver::get_var_id_transducer_rotation_y() { return get_var_id(GRP_IDX_PLATFORM, VAR_IDX_TRANSDUCER_ROTATION_Y); }
int XsfDriver::get_var_id_transducer_rotation_z() { return get_var_id(GRP_IDX_PLATFORM, VAR_IDX_TRANSDUCER_ROTATION_Z); }
int XsfDriver::get_var_id_detection_tx_beam() { return get_var_id(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_TX_BEAM); }
int XsfDriver::get_var_id_detection_rx_transducer_index() { return get_var_id(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_RX_TRANSDUCER_INDEX); }
int XsfDriver::get_var_id_detection_tx_transducer_index() { return get_var_id(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_TX_TRANSDUCER_INDEX); }
int XsfDriver::get_var_id_status() { return get_var_id(GRP_IDX_BATHYMETRY, VAR_IDX_STATUS); }
int XsfDriver::get_var_id_detection_x() { return get_var_id(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_X); }
int XsfDriver::get_var_id_detection_y() { return get_var_id(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_Y); }
int XsfDriver::get_var_id_detection_z() { return get_var_id(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_Z); }
int XsfDriver::get_var_id_detection_beam_pointing_angle() { return get_var_id(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_BEAM_POINTING_ANGLE); }
int XsfDriver::get_var_id_platform_latitude() { return get_var_id(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_PLATFORM_LATITUDE); }
int XsfDriver::get_var_id_platform_longitude() { return get_var_id(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_PLATFORM_LONGITUDE); }
int XsfDriver::get_var_id_platform_heading() { return get_var_id(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_PLATFORM_HEADING); }
int XsfDriver::get_var_id_platform_roll() { return get_var_id(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_PLATFORM_ROLL); }
int XsfDriver::get_var_id_sample_interval() { return get_var_id(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_SAMPLE_INTERVAL); }
int XsfDriver::get_var_id_sound_speed_at_transducer() { return get_var_id(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_SOUND_SPEED_AT_TRANSDUCER); }
int XsfDriver::get_var_id_rx_beam_rotation_phi() { return get_var_id(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_RX_BEAM_ROTATION_PHI); }
int XsfDriver::get_var_id_tx_beam_rotation_theta() { return get_var_id(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_TX_BEAM_ROTATION_THETA); }
int XsfDriver::get_var_id_beamwidth_receive_major() { return get_var_id(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_BEAMWIDTH_RECEIVE_MAJOR); }
int XsfDriver::get_var_id_beamwidth_transmit_minor() { return get_var_id(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_BEAMWIDTH_TRANSMIT_MINOR); }
int XsfDriver::get_var_id_backscatter_r() { return get_var_id(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_BACKSCATTER_R); }

double* XsfDriver::read_double_var(int group_idx, int var_idx) {
    if (var_values[var_idx] == nullptr) nc_read_double(get_nc_group_id(group_idx), get_var_id(group_idx, var_idx), var_values[var_idx]);
    return (double*)var_values[var_idx]->data();
}
float* XsfDriver::read_float_var(int group_idx, int var_idx) {
    if (var_values[var_idx] == nullptr) nc_read_float(get_nc_group_id(group_idx), get_var_id(group_idx, var_idx), var_values[var_idx]);
    return (float*)var_values[var_idx]->data();
}
int* XsfDriver::read_int_var(int group_idx, int var_idx) {
    if (var_values[var_idx] == nullptr) nc_read_int(get_nc_group_id(group_idx), get_var_id(group_idx, var_idx), var_values[var_idx]);
    return (int*)var_values[var_idx]->data();
}
unsigned char* XsfDriver::read_byte_var(int group_idx, int var_idx) {
    if (var_values[var_idx] == nullptr) nc_read_byte(get_nc_group_id(group_idx), get_var_id(group_idx, var_idx), var_values[var_idx]);
    return (unsigned char*)var_values[var_idx]->data();
}
uint64_t* XsfDriver::read_ulong_var(int group_idx, int var_idx) {
    if (var_values[var_idx] == nullptr) nc_read_ulong(get_nc_group_id(group_idx), get_var_id(group_idx, var_idx), var_values[var_idx]);
    return (uint64_t*)var_values[var_idx]->data();
}

float* XsfDriver::read_float_2d_var(int group_idx, int var_idx, int from, int nb) {
    int group_id = get_nc_group_id(group_idx);
    int var_id = get_var_id(group_idx, var_idx);
    nc_read_float_2d_var(group_id, var_id, from, nb, var_values[var_idx]);
    return (float*)var_values[var_idx]->data();
}
int* XsfDriver::read_int_2d_var(int group_idx, int var_idx, int from, int nb) {
    int group_id = get_nc_group_id(group_idx);
    int var_id = get_var_id(group_idx, var_idx);
    nc_read_int_2d_var(group_id, var_id, from, nb, var_values[var_idx]);
    return (int*)var_values[var_idx]->data();
}
short* XsfDriver::read_short_2d_var(int group_idx, int var_idx, int from, int nb) {
    int group_id = get_nc_group_id(group_idx);
    int var_id = get_var_id(group_idx, var_idx);
    nc_read_short_2d_var(group_id, var_id, from, nb, var_values[var_idx]);
    return (short*)var_values[var_idx]->data();
}
unsigned char* XsfDriver::read_byte_2d_var(int group_idx, int var_idx, int from, int nb) {
    int group_id = get_nc_group_id(group_idx);
    int var_id = get_var_id(group_idx, var_idx);
    nc_read_byte_2d_var(group_id, var_id, from, nb, var_values[var_idx]);
    return (unsigned char*)var_values[var_idx]->data();
}

double* XsfDriver::read_var_platform_latitude() { return read_double_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_PLATFORM_LATITUDE); }
double* XsfDriver::read_var_platform_longitude() { return read_double_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_PLATFORM_LONGITUDE); }
float* XsfDriver::read_var_platform_heading() { return read_float_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_PLATFORM_HEADING); }
float* XsfDriver::read_var_platform_pitch() { return read_float_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_PLATFORM_PITCH); }
float* XsfDriver::read_var_platform_roll() { return read_float_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_PLATFORM_ROLL); }
uint64_t* XsfDriver::read_var_ping_time() { return read_ulong_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_PING_TIME); }
float* XsfDriver::read_var_sample_interval() { return read_float_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_SAMPLE_INTERVAL); }
float* XsfDriver::read_var_sound_speed_at_transducer() { return read_float_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_SOUND_SPEED_AT_TRANSDUCER); }
float* XsfDriver::read_var_tx_transducer_depth() { return read_float_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_TX_TRANSDUCER_DEPTH); }
unsigned char* XsfDriver::read_var_transducer_function() { return read_byte_var(GRP_IDX_PLATFORM, VAR_IDX_TRANSDUCER_FUNCTION); }
float* XsfDriver::read_var_transducer_offset_x() { return read_float_var(GRP_IDX_PLATFORM, VAR_IDX_TRANSDUCER_OFFSET_X); }
float* XsfDriver::read_var_transducer_offset_y() { return read_float_var(GRP_IDX_PLATFORM, VAR_IDX_TRANSDUCER_OFFSET_Y); }
float* XsfDriver::read_var_transducer_offset_z() { return read_float_var(GRP_IDX_PLATFORM, VAR_IDX_TRANSDUCER_OFFSET_Z); }
float* XsfDriver::read_var_transducer_rotation_x() { return read_float_var(GRP_IDX_PLATFORM, VAR_IDX_TRANSDUCER_ROTATION_X); }
float* XsfDriver::read_var_transducer_rotation_y() { return read_float_var(GRP_IDX_PLATFORM, VAR_IDX_TRANSDUCER_ROTATION_Y); }
float* XsfDriver::read_var_transducer_rotation_z() { return read_float_var(GRP_IDX_PLATFORM, VAR_IDX_TRANSDUCER_ROTATION_Z); }
float* XsfDriver::read_var_sample_time_offset() { return read_float_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_SAMPLE_TIME_OFFSET); }
float* XsfDriver::read_var_beamwidth_receive_major() { return read_float_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_BEAMWIDTH_RECEIVE_MAJOR); }
float* XsfDriver::read_var_beamwidth_transmit_minor() { return read_float_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_BEAMWIDTH_TRANSMIT_MINOR); }
/* Return the entire attitude roll as float. Raise std::string on error */
float* XsfDriver::read_var_attitude_roll() { return read_float_var(GRP_IDX_ATTITUDE_SUBGROUP, VAR_IDX_ATTITUDE_ROLL); }
/* Return the entire attitude pitch as float. Raise std::string on error */
float* XsfDriver::read_var_attitude_pitch() {
    return read_float_var(GRP_IDX_ATTITUDE_SUBGROUP, VAR_IDX_ATTITUDE_PITCH);
} /* Return the entire attitude roll as float. Raise std::string on error */
uint64_t* XsfDriver::read_var_attitude_time() { return read_ulong_var(GRP_IDX_ATTITUDE_SUBGROUP, VAR_IDX_ATTITUDE_TIME); }
int8_t* XsfDriver::read_var_detection_beam_stabilisation() {
    return (int8_t*)read_byte_var(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_BEAM_STABILISATION);
}
unsigned char* XsfDriver::read_var_multiping_sequence() { return read_byte_var(GRP_IDX_BATHYMETRY, VAR_IDX_MULTIPING_SEQUENCE); }

float* XsfDriver::read_var_rx_beam_rotation_phi(int from, int nb) {
    return read_float_2d_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_RX_BEAM_ROTATION_PHI, from, nb);
}
float* XsfDriver::read_var_tx_beam_rotation_theta(int from, int nb) {
    return read_float_2d_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_TX_BEAM_ROTATION_THETA, from, nb);
}
int* XsfDriver::read_var_receive_transducer_index() { return read_int_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_RECEIVE_TRANSDUCER_INDEX); }
int* XsfDriver::read_var_transmit_transducer_index(int from, int nb) {
    return read_int_2d_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_TRANSMIT_TRANSDUCER_INDEX, from, nb);
}
int* XsfDriver::read_var_transmit_beam_index(int from, int nb) {
    return read_int_2d_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_TRANSMIT_BEAM_INDEX, from, nb);
}
short* XsfDriver::read_var_detection_tx_beam(int from, int nb) { return read_short_2d_var(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_TX_BEAM, from, nb); }
short* XsfDriver::read_var_detection_rx_transducer_index(int from, int nb) {
    return read_short_2d_var(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_RX_TRANSDUCER_INDEX, from, nb);
}
short* XsfDriver::read_var_detection_tx_transducer_index(int from, int nb) {
    return read_short_2d_var(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_TX_TRANSDUCER_INDEX, from, nb);
}
unsigned char* XsfDriver::read_var_status(int from, int nb) { return read_byte_2d_var(GRP_IDX_BATHYMETRY, VAR_IDX_STATUS, from, nb); }
float* XsfDriver::read_var_detection_x(int from, int nb) { return read_float_2d_var(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_X, from, nb); }
float* XsfDriver::read_var_detection_y(int from, int nb) { return read_float_2d_var(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_Y, from, nb); }
float* XsfDriver::read_var_detection_z(int from, int nb) { return read_float_2d_var(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_Z, from, nb); }
float* XsfDriver::read_var_detection_beam_pointing_angle(int from, int nb) {
    return read_float_2d_var(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_BEAM_POINTING_ANGLE, from, nb);
}
float* XsfDriver::read_var_detection_two_way_travel_time(int from, int nb) {
    return read_float_2d_var(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_TWO_WAY_TRAVEL_TIME, from, nb);
}
float* XsfDriver::read_var_detected_bottom_range(int from, int nb) {
    return read_float_2d_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_DETECTED_BOTTOM_RANGE, from, nb);
}
float* XsfDriver::read_var_blanking_interval(int from, int nb) {
    return read_float_2d_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_BLANKING_INTERVAL, from, nb);
}

std::vector<nc_vlen_t>* XsfDriver::load_vlen_backscatter_r(int ping_time_idx) {
    std::unique_ptr<std::vector<nc_vlen_t>> vlen = nc_read_vlen_var(get_group_id_beam_group(), get_var_id_backscatter_r(), ping_time_idx, 1);
    vlen_backscatter_r.reset(vlen.release());
    return vlen_backscatter_r.get();
}
float XsfDriver::get_backscatter_r_offset() { return nc_offset(get_group_id_beam_group(), get_var_id_backscatter_r()); }
float XsfDriver::get_backscatter_r_scale() { return nc_scale_factor(get_group_id_beam_group(), get_var_id_backscatter_r()); }

nc_type XsfDriver::get_backscatter_r_type() {
    nc_type result = nc_get_vlen_base_type(get_group_id_beam_group(), "sample_t");
    if (result != NC_BYTE && result != NC_SHORT && result != NC_FLOAT) {
        throw std::string("backscatter_r type not managed.");
    }
    return result;
}

short XsfDriver::get_backscatter_r_missing_value() {
    if (nc_get_var_type(get_group_id_beam_group(), get_var_id_backscatter_r()) == NC_SHORT) {
        return nc_short_missing_value(get_group_id_beam_group(), get_var_id_backscatter_r());
    }
    return -32768;
}

bool XsfDriver::has_var(int group_idx, int var_idx) {
    if (nc_var_ids[var_idx] == -1) nc_var_ids[var_idx] = sonarnative::nc_get_variable_quietly(get_nc_group_id(group_idx), VAR_NAMES.at(var_idx));
    return nc_var_ids[var_idx] != -1;
}
bool XsfDriver::has_var_tx_transducer_depth() { return has_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_TX_TRANSDUCER_DEPTH); }
bool XsfDriver::has_var_receive_transducer_index() { return has_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_RECEIVE_TRANSDUCER_INDEX); }
bool XsfDriver::has_var_transmit_transducer_index() { return has_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_TRANSMIT_TRANSDUCER_INDEX); }
bool XsfDriver::has_var_transmit_beam_index() { return has_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_TRANSMIT_BEAM_INDEX); }
bool XsfDriver::has_var_detected_bottom_range() { return has_var(GRP_IDX_CURRENT_BEAM_GROUP, VAR_IDX_DETECTED_BOTTOM_RANGE); }
bool XsfDriver::has_var_detection_two_way_travel_time() {
    return has_bathymetry() && has_var(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_TWO_WAY_TRAVEL_TIME);
}
bool XsfDriver::has_var_multiping_sequence() { return has_bathymetry() && has_var(GRP_IDX_BATHYMETRY, VAR_IDX_MULTIPING_SEQUENCE); }
bool XsfDriver::has_var_detection_beam_stabilisation() {
    return has_bathymetry() && has_var(GRP_IDX_BATHYMETRY, VAR_IDX_DETECTION_BEAM_STABILISATION);
}

}  // namespace sonarnative::xsf