#include <cstring>
#include <fstream>
#include <mutex>

#if defined(_WIN32) || defined(WIN32)
#include <filesystem>
#else
#include <experimental/filesystem>
using namespace std::experimental;
#endif
#include "log4cpp/Category.hh"
#include "log4cpp/convenience.h"
#include "sonarnative/api/xsf_api.hpp"

namespace sonarnative {
namespace api {

using namespace std;
using namespace sonarnative::xsf;

// Logger definition
LOG4CPP_LOGGER("sonarnative.xsf_api");

SpatializerHolder::SpatializerHolder(std::string xsf, int ncid) { hold = std::make_shared<xsf::XsfSpatializer>(xsf, ncid); }

size_t SpatializerHolder::get_swath_count() { return hold->get_swath_count(); }

size_t SpatializerHolder::get_beam_count() { return hold->get_beam_count(); }

SpatializerHolder open_spatializer(string xsf, int ncid, bool multi_thread) noexcept {
    SpatializerHolder result(xsf, ncid);
    result.last_error_msg = "";
    try {
        result.hold->initialize();
        result.hold->set_multi_thread(multi_thread);
    } catch (const string& error) {
        result.last_error_msg = error;
        LOG4CPP_ERROR(logger, error);
    } catch (const std::exception& e) {
        result.last_error_msg = e.what();
        LOG4CPP_ERROR(logger, e.what());
    } catch (...) {
        result.last_error_msg = "unknown exception";
        LOG4CPP_ERROR(logger, "unknown exception");
    }
    return result;
}

void change_beam_group_number(SpatializerHolder& spatializer, int other_beam_group_number) noexcept {
    spatializer.last_error_msg = "";
    if (spatializer.hold != nullptr) {
        try {
            spatializer.hold->change_beam_group_number(other_beam_group_number);
        } catch (const string& error) {
            spatializer.last_error_msg = error;
            LOG4CPP_ERROR(logger, error);
        } catch (const std::exception& e) {
            spatializer.last_error_msg = e.what();
            LOG4CPP_ERROR(logger, e.what());
        } catch (...) {
            spatializer.last_error_msg = "unknown exception";
            LOG4CPP_ERROR(logger, "unknown exception");
        }
    }
}

void set_spatialization_method(SpatializerHolder& spatializer, SpatializationMethod method) noexcept {
    spatializer.last_error_msg = "";
    if (spatializer.hold != nullptr) {
        spatializer.hold->set_spatialization_method(method);
    }
}

/* Close an opend SpatializerHolder */
void close_spatializer(SpatializerHolder& spatializer) noexcept {
    spatializer.hold->close();
    spatializer.hold = nullptr;
}

class MemBeamEchoCallback {
    mutex cumulation_lock;
    MemEchos& cumulated_echos;

   public:
    MemBeamEchoCallback(MemEchos& memEchos) : cumulated_echos(memEchos) {}
    // Accumulates BeamEchos from spatializer to MemEchos
    void cumulate(BeamEchos& beam_echos) {
        lock_guard<mutex> guard(cumulation_lock);
        if (cumulated_echos.size + beam_echos.size <= cumulated_echos.capacity) {
            // Accumulates BeamEchos from spatializer to MemEchos
            memcpy(cumulated_echos.longitude + cumulated_echos.size, beam_echos.longitude.data(), sizeof(double) * beam_echos.size);
            memcpy(cumulated_echos.latitude + cumulated_echos.size, beam_echos.latitude.data(), sizeof(double) * beam_echos.size);
            memcpy(cumulated_echos.across + cumulated_echos.size, beam_echos.across.data(), sizeof(float) * beam_echos.size);
            memcpy(cumulated_echos.along + cumulated_echos.size, beam_echos.along.data(), sizeof(float) * beam_echos.size);
            memcpy(cumulated_echos.beam_opening_along + cumulated_echos.size, beam_echos.beam_opening_along.data(), sizeof(float) * beam_echos.size);
            memcpy(cumulated_echos.beam_opening_across + cumulated_echos.size, beam_echos.beam_opening_across.data(),
                   sizeof(float) * beam_echos.size);
            memcpy(cumulated_echos.elevation + cumulated_echos.size, beam_echos.elevation.data(), sizeof(float) * beam_echos.size);
            memcpy(cumulated_echos.height + cumulated_echos.size, beam_echos.height.data(), sizeof(float) * beam_echos.size);
            memcpy(cumulated_echos.echo + cumulated_echos.size, beam_echos.echo.data(), sizeof(float) * beam_echos.size);
            memcpy(cumulated_echos.non_overlapping_beam_opening_angle + cumulated_echos.size, beam_echos.non_overlapping_beam_opening_angle.data(),
                   sizeof(float) * beam_echos.size);
            cumulated_echos.size += beam_echos.size;
        } else {
            throw string("No space left in buffer. Spatialization aborted ");
        }
    }
};

int estimate_beam_echo_count(SpatializerHolder& spatializer, int from_swath, int swath_count) noexcept {
    spatializer.last_error_msg = "";
    try {
        return spatializer.hold->estimate_beam_echo_count(from_swath, swath_count);
    } catch (const string& error) {
        spatializer.last_error_msg = error;
        LOG4CPP_ERROR(logger, error);
        return 0;
    } catch (const std::exception& e) {
        spatializer.last_error_msg = e.what();
        LOG4CPP_ERROR(logger, e.what());
        return 0;
    } catch (...) {
        spatializer.last_error_msg = "unknown exception";
        LOG4CPP_ERROR(logger, "unknown exception");
        return 0;
    }
}

float estimate_normalization_ref_level(SpatializerHolder& spatializer, int from_swath, int swath_count) noexcept {
    spatializer.last_error_msg = "";
    try {
        return spatializer.hold->estimate_normalization_ref_level(from_swath, swath_count);
    } catch (const string& error) {
        spatializer.last_error_msg = error;
        LOG4CPP_ERROR(logger, error);
        return 0.f;
    } catch (const std::exception& e) {
        spatializer.last_error_msg = e.what();
        LOG4CPP_ERROR(logger, e.what());
        return 0.f;
    } catch (...) {
        spatializer.last_error_msg = "unknown exception";
        LOG4CPP_ERROR(logger, "unknown exception");
        return 0.f;
    }
}

void apply_filter(SpatializerHolder& spatializer, const SamplingParameter& param) noexcept { spatializer.hold->get_filter().apply(param); }
void apply_filter(SpatializerHolder& spatializer, const ThresholdParameter& param) noexcept { spatializer.hold->get_filter().apply(param); }
void apply_filter(SpatializerHolder& spatializer, const BottomFilterParameter& param) noexcept { spatializer.hold->get_filter().apply(param); }
void apply_filter(SpatializerHolder& spatializer, const SpecularFilterParameter& param) noexcept { spatializer.hold->get_filter().apply(param); }
void apply_filter(SpatializerHolder& spatializer, const BeamIndexParameter& param) noexcept { spatializer.hold->get_filter().apply(param); }
void apply_filter(SpatializerHolder& spatializer, const SampleIndexParameter& param) noexcept { spatializer.hold->get_filter().apply(param); }
void apply_filter(SpatializerHolder& spatializer, const DepthParameter& param) noexcept { spatializer.hold->get_filter().apply(param); }
void apply_filter(SpatializerHolder& spatializer, const AcrossDistanceParameter& param) noexcept { spatializer.hold->get_filter().apply(param); }
void apply_filter(SpatializerHolder& spatializer, const SideLobeParameter& param) noexcept { spatializer.hold->get_filter().apply(param); }
void apply_filter(SpatializerHolder& spatializer, const MultipingParameter& param) noexcept { spatializer.hold->get_filter().apply(param); }

void apply_signal_processing(SpatializerHolder& spatializer, const ResonOffsetParameter& param) noexcept {
    spatializer.hold->get_signal_processing().apply(param);
}
void apply_signal_processing(SpatializerHolder& spatializer, const RangeNormalizationParameter& param) noexcept {
    spatializer.hold->get_signal_processing().apply(param);
}

void spatialize_in_memory(SpatializerHolder& spatializer, int from_swath, int swath_count, MemEchos& mem_echo) noexcept {
    spatializer.last_error_msg = "";
    try {
        LOG4CPP_DEBUG_S(logger) << "Spatialization in memory required for " << swath_count << " swaths";

        // Reset MemEchos
        mem_echo.size = 0;

        // Creates a reference on member cumulate of MemBeamEchoCallback
        MemBeamEchoCallback echo_callback = MemBeamEchoCallback(mem_echo);
        using placeholders::_1;  // To grab BeamEchos argument
        BeamEchoCallback callback = bind(&MemBeamEchoCallback::cumulate, ref(echo_callback), _1);

        spatializer.hold->perform_for_swaths(from_swath, swath_count, callback);

        LOG4CPP_DEBUG_S(logger) << "Spatialization finish with success. Produced " << mem_echo.size << " echoes";

    } catch (const string& error) {
        spatializer.last_error_msg = error;
        LOG4CPP_ERROR(logger, error);
    } catch (const std::exception& e) {
        spatializer.last_error_msg = e.what();
        LOG4CPP_ERROR(logger, e.what());
    } catch (...) {
        spatializer.last_error_msg = "unknown exception";
        LOG4CPP_ERROR(logger, "unknown exception");
    }
}

class FileBeamEchoCallback {
    FileEchos& cumulated_echos;
    mutex cumulation_lock;

    static const size_t BUFFER_SIZE = 1024 * 32;
    array<ofstream, LAYER_COUNT> stream;
    array<char[BUFFER_SIZE], LAYER_COUNT> buffer;

   public:
    FileBeamEchoCallback(FileEchos& fileEchos, string folder) : cumulated_echos(fileEchos) {
        for (size_t i = 0; i < LAYER_COUNT; i++) {
            stream[i].rdbuf()->pubsetbuf(buffer.at(i), BUFFER_SIZE);
            stream[i].open(fileEchos.file_path(i), ios::binary);
            if (!stream[i].is_open()) throw string("Unable to open the file ").append(fileEchos.file_path(i).c_str());
        }
    }

    // Cumulates BeamEchos from spatializer to FileEchos
    void cumulate(BeamEchos& beam_echos) {
        lock_guard<mutex> guard(cumulation_lock);
        stream.at(LAYER_IDX::longitude).write((char*)beam_echos.longitude.data(), sizeof(double) * beam_echos.size);
        stream.at(LAYER_IDX::latitude).write((char*)beam_echos.latitude.data(), sizeof(double) * beam_echos.size);
        stream.at(LAYER_IDX::across).write((char*)beam_echos.across.data(), sizeof(float) * beam_echos.size);
        stream.at(LAYER_IDX::along).write((char*)beam_echos.along.data(), sizeof(float) * beam_echos.size);
        stream.at(LAYER_IDX::beam_opening_across).write((char*)beam_echos.beam_opening_across.data(), sizeof(float) * beam_echos.size);
        stream.at(LAYER_IDX::beam_opening_along).write((char*)beam_echos.beam_opening_along.data(), sizeof(float) * beam_echos.size);
        stream.at(LAYER_IDX::elevation).write((char*)beam_echos.elevation.data(), sizeof(float) * beam_echos.size);
        stream.at(LAYER_IDX::height).write((char*)beam_echos.height.data(), sizeof(float) * beam_echos.size);
        stream.at(LAYER_IDX::echo).write((char*)beam_echos.echo.data(), sizeof(float) * beam_echos.size);
        stream.at(LAYER_IDX::non_overlapping_beam_opening_angle)
            .write((char*)beam_echos.non_overlapping_beam_opening_angle.data(), sizeof(float) * beam_echos.size);
        cumulated_echos.size(cumulated_echos.size() + beam_echos.size);
    }
};

/* Generates all the beam ethoes belonging to the specified range of swath */
FileEchos spatialize_on_disk(SpatializerHolder& spatializer, int from_swath, int swath_count, string folder) noexcept {
    LOG4CPP_INFO_S(logger) << "Spatialization on disk required for " << swath_count << " swaths";
    spatializer.last_error_msg = "";

    FileEchos result = FileEchos(folder);
    try {
#if defined(_WIN32) || defined(WIN32)
        filesystem::create_directories(folder);
#endif
        // Creates a reference on member cumulate of FileBeamEchoCallback
        FileBeamEchoCallback echo_callback(ref(result), folder);
        using placeholders::_1;  // To grab BeamEchos argument
        BeamEchoCallback callback = bind(&FileBeamEchoCallback::cumulate, ref(echo_callback), _1);

        spatializer.hold->perform_for_swaths(from_swath, swath_count, callback);

        LOG4CPP_INFO_S(logger) << "Spatialization finish with success. Produced " << result.size() << " echoes";

    } catch (const string& error) {
        spatializer.last_error_msg = error;
        LOG4CPP_ERROR(logger, error);
        result.size(0);
    } catch (const std::exception& e) {
        spatializer.last_error_msg = e.what();
        LOG4CPP_ERROR(logger, e.what());
        result.size(0);
    } catch (...) {
        spatializer.last_error_msg = "unknown exception";
        LOG4CPP_ERROR(logger, "unknown exception");
        result.size(0);
    }
    return result;
}

FileEchos spatialize_all_on_disk(SpatializerHolder& spatializer, string folder) noexcept {
    return spatialize_on_disk(spatializer, 0, spatializer.hold->get_swath_count(), folder);
}

MemEchos::MemEchos(int echo_count)
    : capacity(echo_count),
      size(0),
      longitude(new double[echo_count]),
      latitude(new double[echo_count]),
      across(new float[echo_count]),
      along(new float[echo_count]),
      beam_opening_across(new float[echo_count]),
      beam_opening_along(new float[echo_count]),
      elevation(new float[echo_count]),
      height(new float[echo_count]),
      echo(new float[echo_count]),
      non_overlapping_beam_opening_angle(new float[echo_count]) {}
MemEchos::MemEchos() : MemEchos(0) {}
MemEchos::~MemEchos() {
    delete[] longitude, latitude, across, along, beam_opening_across, beam_opening_along, elevation, height, echo, non_overlapping_beam_opening_angle;
}

FileEchos::FileEchos(string folder) : _size(0) {
    for (size_t i = 0; i < LAYER_IDX::COUNT; i++) {
        _file_path[i] = folder + "/layer_" + LAYER_NAME[i] + ".dat";
    }
}
int FileEchos::size() { return _size; }
void FileEchos::size(int size) { _size = size; }
string FileEchos::file_path(int layer_idx) { return _file_path[layer_idx]; }
string FileEchos::longitude() { return _file_path[LAYER_IDX::longitude]; }
string FileEchos::latitude() { return _file_path[LAYER_IDX::latitude]; }
string FileEchos::across() { return _file_path[LAYER_IDX::across]; }
string FileEchos::along() { return _file_path[LAYER_IDX::along]; }
string FileEchos::beam_opening_across() { return _file_path[LAYER_IDX::beam_opening_across]; }
string FileEchos::beam_opening_along() { return _file_path[LAYER_IDX::beam_opening_along]; }
string FileEchos::elevation() { return _file_path[LAYER_IDX::elevation]; }
string FileEchos::height() { return _file_path[LAYER_IDX::height]; }
string FileEchos::echo() { return _file_path[LAYER_IDX::echo]; }
string FileEchos::non_overlapping_beam_opening_angle() { return _file_path[LAYER_IDX::non_overlapping_beam_opening_angle]; }
}  // namespace api
}  // namespace sonarnative
