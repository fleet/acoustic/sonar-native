#include "sonarnative/api/filters_api.hpp"

namespace sonarnative {
namespace api {

SamplingParameter::SamplingParameter(int threshold) : threshold(threshold) {}

ThresholdParameter::ThresholdParameter(bool enable, float min, float max) : enable(enable), min(min), max(max) {}

MultipingParameter::MultipingParameter(bool enable, int index) : enable(enable), index(index) {}

BottomFilterParameter BottomFilterParameter::new_sample(double angle_coefficient, int tolerance_absolute, bool enable) {
    BottomFilterParameter result = BottomFilterParameter();
    result.enable = enable;
    result.tolerance_type = ToleranceType::sample;
    result.angle_coefficient = angle_coefficient;
    result.tolerance_absolute = tolerance_absolute;
    return result;
}

BottomFilterParameter BottomFilterParameter::new_range_percent(double angle_coefficient, double tolerance_percent, bool enable) {
    BottomFilterParameter result = BottomFilterParameter();
    result.enable = enable;
    result.tolerance_type = ToleranceType::range_percent;
    result.angle_coefficient = angle_coefficient;
    result.tolerance_percent = tolerance_percent;
    return result;
}

SpecularFilterParameter::SpecularFilterParameter(bool enable, bool below, int tolerance) : enable(enable), below(below), tolerance(tolerance) {}
BeamIndexParameter::BeamIndexParameter(bool enable, int min, int max) : enable(enable), min(min), max(max) {}
SampleIndexParameter::SampleIndexParameter(bool enable, int min, int max) : enable(enable), min(min), max(max) {}
DepthParameter::DepthParameter(bool enable, float min, float max) : enable(enable), min(min), max(max) {}
AcrossDistanceParameter::AcrossDistanceParameter(bool enable, float min, float max) : enable(enable), min(min), max(max) {}
SideLobeParameter::SideLobeParameter(bool enable, float threshold) : enable(enable), threshold(threshold) {}
SideLobeParameter::SideLobeParameter(bool enable, float threshold, float quantile) : enable(enable), threshold(threshold), quantile(quantile) {}
}  // namespace api
}  // namespace sonarnative