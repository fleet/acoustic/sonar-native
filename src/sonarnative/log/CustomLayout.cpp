/*
 * CustomLayout.cpp
 */

#include "sonarnative/log/CustomLayout.hpp"

#include "log4cpp/Priority.hh"

#include <memory>

#ifdef LOG4CPP_HAVE_SSTREAM
#include <sstream>
#endif

namespace sonarnative {

	CustomLayout::CustomLayout() {
	}

	CustomLayout::~CustomLayout() {
	}

	std::string CustomLayout::format(const log4cpp::LoggingEvent& event) {
		std::ostringstream message;

		const std::string& priorityName = log4cpp::Priority::getPriorityName(event.priority);
		message << event.timeStamp.getSeconds() << " " << priorityName << " "
				<< event.categoryName << " " << event.ndc << ": " << event.message;

		return message.str();
	}
}
