#include "sonarnative/log/appender.hpp"
#include "sonarnative/log/CustomLayout.hpp"

#include "log4cpp/LayoutAppender.hh"
#include "log4cpp/LoggingEvent.hh"
#include "log4cpp/Priority.hh"


namespace sonarnative {

	LogAppender::LogAppender(LogProxy * p) :
		log4cpp::LayoutAppender("Ifremer"),
		proxy(p) {
		setLayout(new CustomLayout());
	}

	LogAppender::~LogAppender() {
	}

	bool LogAppender::reopen()
	{
		return true;
	}
	void LogAppender::close()
	{
	}

	void LogAppender::_append(const log4cpp::LoggingEvent& event) {
		std::string message(_getLayout().format(event));
		switch (event.priority) {
			case log4cpp::Priority::FATAL: proxy->fatal(message); break;
			case log4cpp::Priority::ERROR: proxy->error(message); break;
			case log4cpp::Priority::WARN: proxy->warn(message); break;
			case log4cpp::Priority::INFO: proxy->info(message); break;
			default: proxy->debug(message); break;
		}
	}
}

