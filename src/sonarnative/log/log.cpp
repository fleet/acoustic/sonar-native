#include "sonarnative/log/log.hpp"

#include <log4cpp/Category.hh>
#include <log4cpp/Appender.hh>
#include <log4cpp/FileAppender.hh>
#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/Layout.hh>
#include <log4cpp/BasicLayout.hh>
#include <log4cpp/Priority.hh>


namespace sonarnative {

	// Initialization of static attribute
	Log* Log::singleton = nullptr;

	Log::Log() : appender(nullptr) {
	}

	Log::~Log() {
		removeAppender();
	}

	sonarnative::Log* Log::instance() {
		if (!Log::singleton) {
			Log::singleton = new Log();
		}
		return Log::singleton;
	}

	void Log::initialize(sonarnative::LogProxy* p)
	{
		appender = new sonarnative::LogAppender(p);

		auto& root = log4cpp::Category::getRoot();
		root.setPriority(log4cpp::Priority::DEBUG);

		root.addAppender(appender);

		root.info("sonarnative logger initialized");
	}

	void Log::removeAppender()
	{
		if (appender) {
			auto& root = log4cpp::Category::getRoot();
			root.removeAppender(appender);
			appender = nullptr;
		}
	}

	void Log::initializeLog(sonarnative::LogProxy* p)
	{
		auto logger = sonarnative::Log::instance();
		logger->initialize(p);
	}

	void Log::detachProxy()
	{
		auto logger = sonarnative::Log::instance();
		logger->removeAppender();
	}
}
