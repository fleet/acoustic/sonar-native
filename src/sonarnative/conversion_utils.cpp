#include "sonarnative/utils/conversion_utils.hpp"

#define _USE_MATH_DEFINES 1
#include <math.h>

namespace sonarnative {
    
	double toRadians(double x) { return x / 180.0 * M_PI; }
	double toDegrees(double x) { return x / M_PI * 180; }

	float dBToEnergy(float dB) {
		return powf(10, dB / 10.f);
	}

	float energyTodB(float value) {
		if (value < 0.f)
			return NAN;
		return 10 * log10f(value);
	}

	float dBToAmplitude(float dB) {
		return powf(10.f, dB / 20.0f);
	}

	float amplitudeTodB(float value) {
		if (value < 0.f)
			return NAN;
		return 20 * log10f(value);
	}

}
