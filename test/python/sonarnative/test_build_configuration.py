import unittest
import sonarnative



class TestBuildConfig(unittest.TestCase):
    
    def test_version(self):
        config = sonarnative.BuildConfiguration
        self.assertTrue(config.VERSION_MAJOR >= 1)
        self.assertTrue(config.VERSION_MINOR >= 0)
        self.assertTrue(config.VERSION_PATCH >= 0)

