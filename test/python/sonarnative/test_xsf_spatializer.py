import datetime
import os
import tempfile as tmp

import pytest
import sonarnative



    
def test_read_swath1():

    data_dir = os.environ['V3D_TESTDATA_DIR']
    spatializer = sonarnative.open_spatializer(data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc", False)

    sonarnative.set_spatialization_method(spatializer, sonarnative.SpatializationMethod.DetectionInterpolation)
    assert spatializer.get_swath_count() == 1800
    assert spatializer.get_beam_count() == 512
    echoes_count = sonarnative.estimate_beam_echo_count(spatializer, 0, 1)
    assert echoes_count == 168410

    mem_echos = sonarnative.MemEchos(echoes_count)
    assert mem_echos.size == 0
    sonarnative.spatialize_in_memory(spatializer, 0, 1, mem_echos)
    sonarnative.close_spatializer(spatializer)
    assert mem_echos.size == 168410

    # Check some values like test_xsf_spatializer.cpp...
    assert mem_echos.longitude[0] == -4.374026466050585
    assert mem_echos.latitude[0] == 48.30366698940525
    assert mem_echos.across[0] == pytest.approx(-0.372)
    assert mem_echos.beam_opening_along[0] == pytest.approx(1.0000495)
    assert mem_echos.elevation[0] == pytest.approx(-2.6650815)
    assert mem_echos.height[0] == pytest.approx(0.09677112)
    assert mem_echos.echo[0] == pytest.approx(-64.0)

    assert mem_echos.longitude[10000] == -4.374034052754707
    assert mem_echos.latitude[10000] == 48.303625071289765
    assert mem_echos.across[10000] == pytest.approx(-5.066958)
    assert mem_echos.beam_opening_along[10000] == pytest.approx(1.0000495)
    assert mem_echos.elevation[10000] == pytest.approx(-9.300919)
    assert mem_echos.height[10000] == pytest.approx(0.09677112)
    assert mem_echos.echo[10000] == pytest.approx(-64.0)


def test_sampling_filtering():
    data_dir = os.environ['V3D_TESTDATA_DIR']
    spatializer = sonarnative.open_spatializer(data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc", True)
    mem_echos = sonarnative.MemEchos(168410)
    sonarnative.apply_sampling_filter(spatializer, sonarnative.SamplingParameter(10))
    sonarnative.spatialize_in_memory(spatializer, 0, 1, mem_echos)
    sonarnative.close_spatializer(spatializer)
    assert mem_echos.size == 17020

def test_threshold_filtering():
    data_dir = os.environ['V3D_TESTDATA_DIR']
    spatializer = sonarnative.open_spatializer(data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc", False)
    mem_echos = sonarnative.MemEchos(168410)
    sonarnative.apply_threshold_filter(spatializer, sonarnative.ThresholdParameter(True, 8.5, 8.5))
    sonarnative.spatialize_in_memory(spatializer, 0, 1, mem_echos)
    sonarnative.close_spatializer(spatializer)
    assert mem_echos.size == 72

def test_range_percent_filtering():
    data_dir = os.environ['V3D_TESTDATA_DIR']
    spatializer = sonarnative.open_spatializer(data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc", False)
    mem_echos = sonarnative.MemEchos(4881)
    sonarnative.apply_bottom_filter(spatializer, sonarnative.BottomFilterParameter.new_range_percent(1.5, 25.0, True))
    sonarnative.spatialize_in_memory(spatializer, 1200, 1, mem_echos)
    sonarnative.close_spatializer(spatializer)
    assert mem_echos.size == 4096

def test_sample_filtering():
    data_dir = os.environ['V3D_TESTDATA_DIR']
    spatializer = sonarnative.open_spatializer(data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc", False)
    mem_echos = sonarnative.MemEchos(7027)
    sonarnative.apply_bottom_filter(spatializer, sonarnative.BottomFilterParameter.new_sample(10.0, 150, True))
    sonarnative.spatialize_in_memory(spatializer, 666, 1, mem_echos)
    sonarnative.close_spatializer(spatializer)
    assert mem_echos.size == 7027

def test_threshold_sphere_filtering():
    data_dir = os.environ['V3D_TESTDATA_DIR']
    spatializer = sonarnative.open_spatializer(data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc", False)
    mem_echos = sonarnative.MemEchos(168410)
    sonarnative.apply_threshold_sphere_filter(spatializer, sonarnative.ThresholdSphereParameter(True, -10.0, 30.0, 250))
    sonarnative.spatialize_in_memory(spatializer, 0, 1, mem_echos)
    sonarnative.close_spatializer(spatializer)
    assert mem_echos.size == 18252

def test_threshold_below_sphere_filtering():
    data_dir = os.environ['V3D_TESTDATA_DIR']
    spatializer = sonarnative.open_spatializer(data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc", False)
    mem_echos = sonarnative.MemEchos(168410)
    sonarnative.apply_threshold_below_sphere_filter(spatializer, sonarnative.ThresholdBelowSphereParameter(True, -5.0, 5.0, 2))
    sonarnative.spatialize_in_memory(spatializer, 0, 1, mem_echos)
    sonarnative.close_spatializer(spatializer)
    assert mem_echos.size == 138662

def test_beam_index_filtering():
    data_dir = os.environ['V3D_TESTDATA_DIR']
    spatializer = sonarnative.open_spatializer(data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc", False)
    mem_echos = sonarnative.MemEchos(168410)
    sonarnative.apply_beam_index_filter(spatializer, sonarnative.BeamIndexParameter(True, 50, 75))
    sonarnative.spatialize_in_memory(spatializer, 0, 1, mem_echos)
    sonarnative.close_spatializer(spatializer)
    assert mem_echos.size == 8628

def test_depth_filtering():
    data_dir = os.environ['V3D_TESTDATA_DIR']
    spatializer = sonarnative.open_spatializer(data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc", False)
    mem_echos = sonarnative.MemEchos(168410)
    sonarnative.apply_depth_filter(spatializer, sonarnative.DepthParameter(True, 20.0, 25.0))
    sonarnative.spatialize_in_memory(spatializer, 0, 1, mem_echos)
    sonarnative.close_spatializer(spatializer)
    assert mem_echos.size == 28626

def test_across_distance_filter_filtering():
    data_dir = os.environ['V3D_TESTDATA_DIR']
    spatializer = sonarnative.open_spatializer(data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc", False)
    mem_echos = sonarnative.MemEchos(168410)
    sonarnative.apply_across_distance_filter(spatializer, sonarnative.AcrossDistanceParameter(True, -5.0, 5.0))
    sonarnative.spatialize_in_memory(spatializer, 0, 1, mem_echos)
    sonarnative.close_spatializer(spatializer)
    assert mem_echos.size == 100177

def test_sidelobe_filter_filtering():
    data_dir = os.environ['V3D_TESTDATA_DIR']
    spatializer = sonarnative.open_spatializer(data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc", False)
    mem_echos = sonarnative.MemEchos(168410)
    sonarnative.apply_side_lobe_filter(spatializer, sonarnative.SideLobeParameter(True, 12.0, 1.0))
    sonarnative.spatialize_in_memory(spatializer, 0, 1, mem_echos)
    sonarnative.close_spatializer(spatializer)
    assert mem_echos.size == 63248


def test_bench_xsf_spatialization():

    with tmp.TemporaryDirectory(prefix="Bench_XSF_", suffix="_Python") as o_path:
        data_dir = os.environ['V3D_TESTDATA_DIR']
        spatializer = sonarnative.open_spatializer(data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc", True)

        now = datetime.datetime.now()
        file_echos = sonarnative.spatialize_all_on_disk(spatializer, o_path)
        sonarnative.close_spatializer(spatializer)
        print(f"XSF Spatialization produced {file_echos.size} echoes")
        print(f"XSF Spatialization takes {datetime.datetime.now() - now}")

        assert file_echos.size == 223260924
        assert os.path.getsize(file_echos.longitude) == 8 * 223260924
        assert os.path.getsize(file_echos.latitude) == 8 * 223260924
        assert os.path.getsize(file_echos.across) == 4 * 223260924
        assert os.path.getsize(file_echos.beam_opening_along) == 4 * 223260924
        assert os.path.getsize(file_echos.elevation) == 4 * 223260924
        assert os.path.getsize(file_echos.height) == 4 * 223260924
        assert os.path.getsize(file_echos.echo) == 4 * 223260924


#def test_unit():
#    spatializer = sonarnative.open_spatializer("C:\\Users\\asaunier\\Documents\\Data\\ESSTECH-2022-1\\EM304\\0012_20220111_205414.xsf.nc", False)
#    mem_echos = sonarnative.MemEchos(776024)
#    sonarnative.spatialize_in_memory(spatializer, 0, 1, mem_echos)
#    sonarnative.close_spatializer(spatializer)
#    assert mem_echos.size > 0

