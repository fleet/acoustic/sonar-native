// activate sonarnative-dev
// cd build
// cmake --build . --config Debug --target install
// Copy this file and GlobeTestUtil.java into build/bin/Debug
// cd bin/Debug
// Compile them : javac -cp "../sonarnative.jar;." *.java
// Launch : java -ea -Xmx2048m -cp "../sonarnative.jar;." TestJNI

import java.io.File;
import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.nio.file.StandardOpenOption;

import fr.ifremer.sonarnative.SonarNative;
import fr.ifremer.sonarnative.SonarNativeEchoBuffers;
import fr.ifremer.sonarnative.SonarNativeException;
import fr.ifremer.sonarnative.SonarNativeLibLoader;
import fr.ifremer.sonarnative.jni.AcrossDistanceParameter;
import fr.ifremer.sonarnative.jni.BeamIndexParameter;
import fr.ifremer.sonarnative.jni.BottomFilterParameter;
import fr.ifremer.sonarnative.jni.DepthParameter;
import fr.ifremer.sonarnative.jni.FileEchos;
import fr.ifremer.sonarnative.jni.Log;
import fr.ifremer.sonarnative.jni.LogProxy;
import fr.ifremer.sonarnative.jni.ProgressCallback;
import fr.ifremer.sonarnative.jni.SamplingParameter;
import fr.ifremer.sonarnative.jni.SideLobeParameter;
import fr.ifremer.sonarnative.jni.SpatializerHolder;
import fr.ifremer.sonarnative.jni.ThresholdBelowSphereParameter;
import fr.ifremer.sonarnative.jni.ThresholdParameter;
import fr.ifremer.sonarnative.jni.ThresholdSphereParameter;

public class TestJNI {

	static class JavaLogger extends LogProxy {
		public String lastMessage = "";

		@Override
		public void fatal(String message) {
			System.err.println("FATAL : " + message);
			lastMessage = message;
		}

		@Override
		public void error(String message) {
			System.err.println("ERROR : " + message);
			lastMessage = message;
		}

		@Override
		public void warn(String message) {
			System.out.println("WARN : " + message);
			lastMessage = message;
		}

		@Override
		public void info(String message) {
			System.out.println("INFO : " + message);
			lastMessage = message;
		}

		@Override
		public void debug(String message) {
			System.out.print(".");
			lastMessage = message;
		}
	}

	static class JavaProgressCallback extends ProgressCallback {
		@Override
		public void progress(double complete, String message) {
			System.out.println("ProgressCallback message='" + message + "' complete=" + complete);
		}
	}

	public static void main(String argv[]) throws IOException {
		testSonarNative();
	}

	/** Test all SonarNative classes */
	static void testSonarNative() throws IOException {
		System.out.println("--- Sonar Native ---");

		SonarNativeLibLoader.loadNativeLibrary();
		System.out.println("Library loaded : " + SonarNativeLibLoader.isAvailable());
		assert SonarNativeLibLoader.isAvailable();

		System.out.println("---");
		System.out.println("sonarnative.version() returns " + fr.ifremer.sonarnative.jni.SonarNative.version());

		JavaLogger logger = new JavaLogger();
		Log.initializeLog(logger);

		try {
			testMemorySpatializer();
			testSpatializerOnDisk();
		} catch (SonarNativeException e) {
			e.printStackTrace();
		}

		logger.delete();
	}

	/** Test the XsfSpatializer class */
	static void testMemorySpatializer() throws SonarNativeException {
		System.out.println("--- XSF Spatializer ---");

		File xsfFile = new File(GlobeTestUtil.getTestDataPath() + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc");
		System.out.println("Using XSF file " + xsfFile.getPath() + ", exists : " + xsfFile.exists());
		assert xsfFile.exists() : "XSF file not found. Check your environment variable V3D_TESTDATA_DIR ";

		SpatializerHolder spatializer = SonarNative.openSpatializer(xsfFile.getPath(), false);

		// XSF Spatialization in memory
		int echoesCount = SonarNative.estimateBeamEchoCount(spatializer, 0, 1);
		System.out.println("XSF Spatialization, estimating echoes count. Expecting 168410, got " + echoesCount);

		SonarNativeEchoBuffers buffers = new SonarNativeEchoBuffers(echoesCount);
		SonarNative.spatialize(spatializer, 0, 1, buffers);
		System.out.println(
				"XSF Spatialization in menory of first swath. Expecting 168410 echoes, got " + buffers.getEchoCount());
		assert buffers.getEchoCount() == 168410;
		System.out.println("Check some values like test_xsf_spatializer.cpp...");
		assert buffers.longitudes.get(0) == -4.374026466050585 : "Wrong longitude value";
		assert buffers.latitudes.get(0) == 48.30366698940525;
		assert buffers.acrosses.get(0) == -0.372f;
		assert buffers.beamOpeningAlongs.get(0) == 1.0000495f : "Wrong along value";
		assert buffers.elevations.get(0) == -2.6650815f;
		assert buffers.heights.get(0) == 0.09677112f;
		assert buffers.echoes.get(0) == -64.0f;
		assert buffers.nonOverlappingBeamOpeningAngles.get(0) == 0.16999817f;
		System.out.println("Echo 0 OK");

		assert buffers.longitudes.get(10000) == -4.3740340527547072 : "Wrong longitude value";
		assert buffers.latitudes.get(10000) == 48.303625071289765;
		assert buffers.acrosses.get(10000) == -5.066958f;
		assert buffers.beamOpeningAlongs.get(10000) == 1.0000495f : "Wrong along value";
		assert buffers.elevations.get(10000) == -9.300919f;
		assert buffers.heights.get(10000) == 0.09677112f;
		assert buffers.echoes.get(10000) == -64.0f;
		assert buffers.nonOverlappingBeamOpeningAngles.get(10000) == 0.17000198f;
		System.out.println("Echo 10000 OK");

		System.out.println("Check Threshold filtering...");
		var samplingParameter = new SamplingParameter(10);
		SonarNative.applyFilter(spatializer, samplingParameter);
		SonarNative.spatialize(spatializer, 0, 1, buffers);
		assert buffers.getEchoCount() == 17020;
		SonarNative.applyFilter(spatializer, new SamplingParameter()); // Reset filter

		System.out.println("Check Threshold filtering...");
		var thresholdParameter = new ThresholdParameter(true, 8.5f, 8.5f);
		SonarNative.applyFilter(spatializer, thresholdParameter);
		SonarNative.spatialize(spatializer, 0, 1, buffers);
		assert buffers.getEchoCount() == 72;
		SonarNative.applyFilter(spatializer, new ThresholdParameter()); // Reset filter

		System.out.println("Check Bottom filtering...");
		var bottomParameter = BottomFilterParameter.new_range_percent(1.5, 25.0);
		SonarNative.applyFilter(spatializer, bottomParameter);
		SonarNative.spatialize(spatializer, 1200, 1, buffers);
		assert buffers.getEchoCount() == 21025;
		bottomParameter = BottomFilterParameter.new_sample(10.0, 150);
		SonarNative.applyFilter(spatializer, bottomParameter);
		SonarNative.spatialize(spatializer, 666, 2, buffers);
		assert buffers.getEchoCount() == 7027 + 6970;
		SonarNative.applyFilter(spatializer, new BottomFilterParameter()); // Reset filter

		System.out.println("Check Threshold Sphere filtering...");
		var thresholdSphereParameter = new ThresholdSphereParameter(true, -10.0f, 30.0f, 250);
		SonarNative.applyFilter(spatializer, thresholdSphereParameter);
		SonarNative.spatialize(spatializer, 0, 1, buffers);
		assert buffers.getEchoCount() == 18252;
		SonarNative.applyFilter(spatializer, new ThresholdSphereParameter()); // Reset filter

		System.out.println("Check Threshold Below Sphere filtering...");
		var thresholdBelowSphereParameter = new ThresholdBelowSphereParameter(true, -5.0f, 5.0f, 2);
		SonarNative.applyFilter(spatializer, thresholdBelowSphereParameter);
		SonarNative.spatialize(spatializer, 0, 1, buffers);
		assert buffers.getEchoCount() == 138662;
		SonarNative.applyFilter(spatializer, new ThresholdBelowSphereParameter()); // Reset filter

		System.out.println("Check beam index filtering...");
		var beamIndexParameter = new BeamIndexParameter(true, 50, 75);
		SonarNative.applyFilter(spatializer, beamIndexParameter);
		SonarNative.spatialize(spatializer, 0, 1, buffers);
		assert buffers.getEchoCount() == 8628;
		SonarNative.applyFilter(spatializer, new BeamIndexParameter()); // Reset filter

		System.out.println("Check depth filtering...");
		var depthParameter = new DepthParameter(true, 20.0f, 25.0f);
		SonarNative.applyFilter(spatializer, depthParameter);
		SonarNative.spatialize(spatializer, 0, 1, buffers);
		assert buffers.getEchoCount() == 28626;
		SonarNative.applyFilter(spatializer, new DepthParameter()); // Reset filter

		System.out.println("Check across distance filtering...");
		var acrossDistanceParameter = new AcrossDistanceParameter(true, -5.0f, 5.0f);
		SonarNative.applyFilter(spatializer, acrossDistanceParameter);
		SonarNative.spatialize(spatializer, 0, 1, buffers);
		assert buffers.getEchoCount() == 100177;
		SonarNative.applyFilter(spatializer, new AcrossDistanceParameter()); // Reset filter

		System.out.println("Check side lobe  filtering...");
		var sideLobeParameter = new SideLobeParameter(true, 12.0f, 0.5f);
		SonarNative.applyFilter(spatializer, sideLobeParameter);
		SonarNative.spatialize(spatializer, 0, 1, buffers);
		assert buffers.getEchoCount() == 63248;
		SonarNative.applyFilter(spatializer, new SideLobeParameter()); // Reset filter

		SonarNative.closeSpatializer(spatializer);

	}

	/** Test XSF Spatialization in files */
	static void testSpatializerOnDisk() throws SonarNativeException {
		System.out.println("--- Bench XSF Spatializer on disk---");

		long start = System.currentTimeMillis();
		SpatializerHolder spatializer = SonarNative
				.openSpatializer(GlobeTestUtil.getTestDataPath() + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc");
		FileEchos fileEchos = SonarNative.spatializeOnDisk(spatializer, "C:/temp/Bench_XSF_java");
		SonarNative.closeSpatializer(spatializer);
		System.out.println("XSF Spatialization. takes " + (System.currentTimeMillis() - start) + " ms");
		assert fileEchos.size() == 223260924;

		var longitudeFile = new File(fileEchos.longitude());
		assert longitudeFile.length() == 223260924 * Double.BYTES;
		var latitudeFile = new File(fileEchos.latitude());
		assert latitudeFile.length() == 223260924 * Double.BYTES;
		var acrossFile = new File(fileEchos.across());
		assert acrossFile.length() == 223260924 * Float.BYTES;
		var alongFile = new File(fileEchos.beam_opening_along());
		assert alongFile.length() == 223260924 * Float.BYTES;
		var elevationFile = new File(fileEchos.elevation());
		assert elevationFile.length() == 223260924 * Float.BYTES;
		var heightFile = new File(fileEchos.height());
		assert heightFile.length() == 223260924 * Float.BYTES;
		var echoFile = new File(fileEchos.echo());
		assert echoFile.length() == 223260924 * Float.BYTES;

		double minLon = Double.POSITIVE_INFINITY;
		double maxLon = Double.NEGATIVE_INFINITY;
		double minLat = Double.POSITIVE_INFINITY;
		double maxLat = Double.NEGATIVE_INFINITY;
		try (var longitudeChannel = FileChannel.open(longitudeFile.toPath(), StandardOpenOption.READ);
				var latitudeChannel = FileChannel.open(latitudeFile.toPath(), StandardOpenOption.READ)) {
			var longitudeBuffer = longitudeChannel.map(MapMode.READ_ONLY, 0, longitudeFile.length())
					.order(ByteOrder.nativeOrder()).asDoubleBuffer();
			var latitudeBuffer = latitudeChannel.map(MapMode.READ_ONLY, 0, latitudeFile.length())
					.order(ByteOrder.nativeOrder()).asDoubleBuffer();
			for (int i = 0; i < 223260924; i++) {
				double lon = longitudeBuffer.get();
				minLon = Math.min(minLon, lon);
				maxLon = Math.max(maxLon, lon);
				double lat = latitudeBuffer.get();
				minLat = Math.min(minLat, lat);
				maxLat = Math.max(maxLat, lat);
			}
		} catch (Exception e) {
			e.printStackTrace();
			assert false;
		}

		assert minLon == -4.422866499514086;
		assert maxLon == -4.373957768525229;
		assert minLat == 48.302663355456914;
		assert maxLat == 48.307521073230674;

		longitudeFile.delete();
		latitudeFile.delete();
		acrossFile.delete();
		alongFile.delete();
		elevationFile.delete();
		heightFile.delete();
		echoFile.delete();
	}

}
