// Extract from GLOBE test utilities.

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * This class will determine where is located the data test files
 */
public class GlobeTestUtil {
	/**
	 * Nom de la variable d'environnement pour la configuration du répertoire des données de test.
	 */
	public static final String ENV_TESTDATA_DIR = "V3D_TESTDATA_DIR";

	/**
	 * Nom de la fichier d'environnement pour la configuration du répertoire des données de test.
	 */
	public static final String USER_TXT_FILE = ".GLOBE_TESTDATADIR.txt";

	/**
	 * Renvoi le chemin contenant les données de tests. Ce chemin peut être paramétré en utilisant la variable
	 * d'environnement 3DV_TESTDATA_DIR
	 */
	public static String getTestDataPath() {
		String path = System.getenv(ENV_TESTDATA_DIR);
		if (path != null)
			return path;

		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
			path = readFile("c:\\");
		} else {
			path = readFile("\\");
		}
		if (path != null)
			return path;
		String userDir = System.getProperty("user.home");
		if (userDir != null) {
			path = readFile(userDir);
		}
		// default case
		if (path == null)
			path = "./";
		return path;
	}

	/**
	 * Read the file containing the path definition
	 * 
	 * @return the path or null if the file does not exist
	 */
	public static String readFile(String userDir) {
		String path = null;
		Path file = Paths.get(userDir, USER_TXT_FILE);

		if (Files.exists(file)) {
			try (BufferedReader reader = Files.newBufferedReader(file)) {
				String line = null;
				while ((line = reader.readLine()) != null) {
					if (Files.exists(Paths.get(line))) {
						path = line;
					}

				}
			} catch (IOException x) {
				x.printStackTrace();
			}

		} else {
			System.err.println("File does not exist" + file.getFileName());
		}
		return path;
	}
}
