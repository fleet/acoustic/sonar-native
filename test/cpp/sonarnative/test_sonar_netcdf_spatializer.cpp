﻿#include <doctest.h>

#include <string>
#include <fstream> 

#include "sonarnative/log/log.hpp"
#include "logproxy.hpp"

using namespace std;

#include "sonarnative/api/xsf_api.hpp"
using namespace sonarnative;
#include "common/testUtils.hpp"

TEST_SUITE_BEGIN("Sonar netcdf spatialization tests");

TEST_CASE("Sonar netcdf spatialization - check first swath, Beam_group1" * doctest::may_fail()) {

	auto proxy = new testsonarnative::CoutLogProxy();
	// Uncomment for tracing
	// sonarnative::Log::initializeLog(proxy);

	string data_dir = testcommon::getTestDataPath();
	string file_path = (data_dir + "/file/cpp/PELGAS19_009_20190504_051733_1.nc").c_str();
	CHECK(testcommon::fileExists(file_path));


	auto spatializer = api::open_spatializer(file_path, -1, false);
	int echo_count = api::estimate_beam_echo_count(spatializer, 0, 1);
	auto echoes = api::MemEchos(echo_count);
	spatialize_in_memory(spatializer, 0, 1, echoes);
	CHECK_EQ(echoes.size, 531); // Less than echo_count : some echo have missing_value
	CHECK_EQ(echoes.longitude[0], -1.9842264726273855);
	CHECK_EQ(echoes.latitude[0], 45.219822933631306);
	CHECK_EQ(echoes.across[0], 0.8f);
	CHECK_EQ(echoes.beam_opening_across[0], 10.3f);
	CHECK_EQ(echoes.beam_opening_along[0], 10.13f);
	CHECK_EQ(echoes.elevation[0], -6.03508711f);
	CHECK_EQ(echoes.height[0], 0.18831959f);
	CHECK_EQ(echoes.echo[0], -21.13f);

	api::change_beam_group_number(spatializer, 6);
	echo_count = api::estimate_beam_echo_count(spatializer, 0, 1);
	spatialize_in_memory(spatializer, 0, 1, echoes);
	CHECK_EQ(echoes.echo[0], 27.64f);

	api::close_spatializer(spatializer);
}

TEST_SUITE_END(); // end of testsuite 
