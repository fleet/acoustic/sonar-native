﻿#include <doctest.h>

#include <string>
#include <chrono>
#include <fstream> 
#include <functional> 
#include <inttypes.h> 
#include <mutex> 

using namespace std;

#if defined(_WIN32) || defined(WIN32)
#include <filesystem>
#else
#include <experimental/filesystem>
using namespace experimental;
#endif


#include "sonarnative/api/xsf_api.hpp"
using namespace sonarnative;
#include "common/testUtils.hpp"

TEST_SUITE_BEGIN("XSF spatialization tests");


//Uncomment to check specific file : 
// TODO : add simple test with xsf from kmall (missing detection_bottom_range)
TEST_CASE("XSF spatialization - spatialize in memory") {
	//string xsf_path = "C:\\Users\\asaunier\\Documents\\Globe_Documents\\2022 02 Formation Globe\\donnees\\test_bulleur_thalia\\0090_20190429_063439_Bassop.xsf.nc";
	string xsf_path = "C:\\Users\\asaunier\\Documents\\Data\\Etude_spatialisation_WC\\A.SAUNIER\\EnginTireBouchon\\0031_20040101_125439_raw.xsf.nc";
	CHECK(testcommon::fileExists(xsf_path));

	auto spatializer = api::open_spatializer(xsf_path);
	int echoes_count = estimate_beam_echo_count(spatializer, 629, 1);
	auto echoes = api::MemEchos(echoes_count);
	auto filter_parameter = api::SpecularFilterParameter(true, true, 3);
	apply_filter(spatializer, filter_parameter);
	spatialize_in_memory(spatializer, 1203, 1, echoes);
	api::close_spatializer(spatializer);

	CHECK_GT(echoes.size, 0);
}


TEST_CASE("XSF spatialization - check first swath") {
	string data_dir = testcommon::getTestDataPath();
	string xsf_path = (data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc").c_str();
	CHECK(testcommon::fileExists(xsf_path));


	auto spatializer = api::open_spatializer(xsf_path, -1, false);
	auto echoes = api::MemEchos(168410);
	spatialize_in_memory(spatializer, 0, 1, echoes);
	api::close_spatializer(spatializer);

	CHECK_EQ(echoes.size, 168410);

	// Check first echo of first beam
	CHECK_EQ(echoes.longitude[0], -4.374026466050585);
	CHECK_EQ(echoes.latitude[0], 48.30366698940525);
	CHECK_EQ(echoes.across[0], -0.372f);
	CHECK_EQ(echoes.beam_opening_along[0], 1.0000495f);
	CHECK_EQ(echoes.elevation[0], -2.66508150100708f);
	CHECK_EQ(echoes.height[0], 0.09677112f);
	CHECK_EQ(echoes.echo[0], -64.0f);
	CHECK_EQ(echoes.beam_opening_across[0], 1.30312002f);
	CHECK_EQ(echoes.non_overlapping_beam_opening_angle[0], 0.169998169f);


	// Check last echo of first beam (380 echoes expected)
	CHECK_EQ(echoes.longitude[379], -4.3740644046592481);
	CHECK_EQ(echoes.latitude[379], 48.303456269609448);
	CHECK_EQ(echoes.across[379], -23.97155f);
	CHECK_EQ(echoes.beam_opening_along[379], 1.0000495f);
	CHECK_EQ(echoes.elevation[379], -30.740139f);
	CHECK_EQ(echoes.height[379], 0.09677112f);
	CHECK_EQ(echoes.echo[379], -19.0f);
	CHECK_EQ(echoes.beam_opening_across[379], 1.30312002f);
	CHECK_EQ(echoes.non_overlapping_beam_opening_angle[379], 0.169998169f);

	// Check first echo of second beam
	CHECK_EQ(echoes.longitude[380], -4.3740264660505854);
	CHECK_EQ(echoes.latitude[380], 48.303666989405251);
	CHECK_EQ(echoes.across[380], -0.372f);
	CHECK_EQ(echoes.beam_opening_along[380], 1.0000495f);
	CHECK_EQ(echoes.elevation[380], -2.6650815f);
	CHECK_EQ(echoes.height[380], 0.09677112f);
	CHECK_EQ(echoes.echo[380], -64.0f);
	CHECK_EQ(echoes.beam_opening_across[380], 1.30312002f);
	CHECK_EQ(echoes.non_overlapping_beam_opening_angle[380], 0.169998169f);


	// Check 10000th sample
	CHECK_EQ(echoes.longitude[10000], -4.3740340527547072);
	CHECK_EQ(echoes.latitude[10000], 48.303625071289765);
	CHECK_EQ(echoes.across[10000], -5.0669579f);
	CHECK_EQ(echoes.beam_opening_along[10000], 1.0000495f);
	CHECK_EQ(echoes.elevation[10000], -9.300919f);
	CHECK_EQ(echoes.height[10000], 0.09677112f);
	CHECK_EQ(echoes.echo[10000], -64.0f);

	// Check min/max
	double minLat = echoes.latitude[0], maxLat = echoes.latitude[0];
	double minLon = echoes.longitude[0], maxLon = echoes.longitude[0];
	for (size_t i = 0; i < echoes.size; i++)
	{
		minLon = fmin(minLon, echoes.longitude[i]);
		maxLon = fmax(maxLon, echoes.longitude[i]);
		minLat = fmin(minLat, echoes.latitude[i]);
		maxLat = fmax(maxLat, echoes.latitude[i]);
	}

	CHECK_EQ(minLon, -4.3740644046592481);
	CHECK_EQ(maxLon, -4.3739874011150928);
	CHECK_EQ(minLat, 48.303456269609448);
	CHECK_EQ(maxLat, 48.303896790217166);
}


TEST_CASE("XSF spatialization - Sampling filtering") {
	string data_dir = testcommon::getTestDataPath();
	string xsf_path = (data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc").c_str();
	CHECK(testcommon::fileExists(xsf_path));


	auto spatializer = api::open_spatializer(xsf_path);
	auto echoes = api::MemEchos(168410);
	auto sampling_parameter = api::SamplingParameter(10);
	apply_filter(spatializer, sampling_parameter);
	spatialize_in_memory(spatializer, 0, 1, echoes);
	api::close_spatializer(spatializer);

	CHECK_EQ(echoes.size, 17020);
}

TEST_CASE("XSF spatialization - Threshold filtering") {
	string data_dir = testcommon::getTestDataPath();
	string xsf_path = (data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc").c_str();
	CHECK(testcommon::fileExists(xsf_path));


	auto spatializer = api::open_spatializer(xsf_path);
	auto echoes = api::MemEchos(168410);
	auto threshold_parameter = api::ThresholdParameter(true, 8.5f, 8.5f);
	apply_filter(spatializer, threshold_parameter);
	spatialize_in_memory(spatializer, 0, 1, echoes);
	api::close_spatializer(spatializer);

	CHECK_EQ(echoes.size, 72);
}

TEST_CASE("XSF spatialization - Bottom filtering - range_percent") {
	string data_dir = testcommon::getTestDataPath();
	string xsf_path = (data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc").c_str();
	CHECK(testcommon::fileExists(xsf_path));

	auto spatializer = api::open_spatializer(xsf_path);
	auto bottom_parameter = api::BottomFilterParameter::new_range_percent(1.5, 25.0);
	apply_filter(spatializer, bottom_parameter);
	// Swath 0
	auto echoes = api::MemEchos(20334); // 20334 is the max expected nb of echo
	spatialize_in_memory(spatializer, 0, 1, echoes);
	CHECK_EQ(echoes.size, 18432);
	// Smath 1
	spatialize_in_memory(spatializer, 1, 1, echoes);
	CHECK_EQ(echoes.size, 18432);
	// Smath 1200
	spatialize_in_memory(spatializer, 1200, 1, echoes);
	CHECK_EQ(echoes.size, 20102);

	api::close_spatializer(spatializer);

}
TEST_CASE("XSF spatialization - Bottom filtering - sample") {
	string data_dir = testcommon::getTestDataPath();
	string xsf_path = (data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc").c_str();
	CHECK(testcommon::fileExists(xsf_path));

	auto spatializer = api::open_spatializer(xsf_path);
	int echoes_count = estimate_beam_echo_count(spatializer, 10, 1);
	CHECK_EQ(echoes_count, 247602);
	auto echoes = api::MemEchos(echoes_count);

	// Swath 10
	apply_filter(spatializer, api::BottomFilterParameter::new_sample(300.0, 150));
	spatialize_in_memory(spatializer, 10, 1, echoes);
	CHECK_EQ(echoes.size, 55713);

	// Swath 666
	apply_filter(spatializer, api::BottomFilterParameter::new_sample(10.0, 150));
	spatialize_in_memory(spatializer, 666, 1, echoes);
	CHECK_EQ(echoes.size, 7027);

	// Swath 667
	spatialize_in_memory(spatializer, 667, 1, echoes);
	CHECK_EQ(echoes.size, 6970);

	// Swath 666 + 667
	spatialize_in_memory(spatializer, 666, 2, echoes);
	CHECK_EQ(echoes.size, 7027 + 6970);

	api::close_spatializer(spatializer);

}

TEST_CASE("XSF spatialization - Specular filtering") {
	string data_dir = testcommon::getTestDataPath();
	string xsf_path = (data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc").c_str();
	CHECK(testcommon::fileExists(xsf_path));

	auto spatializer = api::open_spatializer(xsf_path);
	auto echoes = api::MemEchos(168410);
	auto specular_parameter = api::SpecularFilterParameter(true, false, 250);
	apply_filter(spatializer, specular_parameter);
	spatialize_in_memory(spatializer, 0, 1, echoes);
	api::close_spatializer(spatializer);

	CHECK_EQ(echoes.size, 18252);
}

TEST_CASE("XSF spatialization - Specular below filtering") {
	string data_dir = testcommon::getTestDataPath();
	string xsf_path = (data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc").c_str();
	CHECK(testcommon::fileExists(xsf_path));

	auto spatializer = api::open_spatializer(xsf_path, -1, false);
	auto echoes = api::MemEchos(168410);
	auto specular_parameter = api::SpecularFilterParameter(true, true, 2);
	apply_filter(spatializer, specular_parameter);
	spatialize_in_memory(spatializer, 0, 1, echoes);
	api::close_spatializer(spatializer);

	CHECK_EQ(echoes.size, 138662);
}

TEST_CASE("XSF spatialization - Beam index filtering") {
	string data_dir = testcommon::getTestDataPath();
	string xsf_path = (data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc").c_str();
	CHECK(testcommon::fileExists(xsf_path));

	auto spatializer = api::open_spatializer(xsf_path);
	auto echoes = api::MemEchos(168410);
	auto beam_index_parameter = api::BeamIndexParameter(true, 50, 75);
	apply_filter(spatializer, beam_index_parameter);
	spatialize_in_memory(spatializer, 0, 1, echoes);
	api::close_spatializer(spatializer);

	CHECK_EQ(echoes.size, 8628);
}

TEST_CASE("XSF spatialization - Depth filtering") {
	string data_dir = testcommon::getTestDataPath();
	string xsf_path = (data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc").c_str();
	CHECK(testcommon::fileExists(xsf_path));

	auto spatializer = api::open_spatializer(xsf_path);
	auto echoes = api::MemEchos(168410);
	auto depth_parameter = api::DepthParameter(true, 20.0f, 25.0f);
	apply_filter(spatializer, depth_parameter);
	spatialize_in_memory(spatializer, 0, 1, echoes);
	api::close_spatializer(spatializer);

	CHECK_EQ(echoes.size, 28626);
}

TEST_CASE("XSF spatialization - Across distance filtering") {
	string data_dir = testcommon::getTestDataPath();
	string xsf_path = (data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc").c_str();
	CHECK(testcommon::fileExists(xsf_path));

	auto spatializer = api::open_spatializer(xsf_path);
	auto echoes = api::MemEchos(168410);
	auto across_distance_parameter = api::AcrossDistanceParameter(true, -5.0f, 5.0f);
	apply_filter(spatializer, across_distance_parameter);
	spatialize_in_memory(spatializer, 0, 1, echoes);
	api::close_spatializer(spatializer);

	CHECK_EQ(echoes.size, 100177);
}

TEST_CASE("XSF spatialization - Side lobe filtering") {
	string data_dir = testcommon::getTestDataPath();
	string xsf_path = (data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc").c_str();
	CHECK(testcommon::fileExists(xsf_path));

	auto spatializer = api::open_spatializer(xsf_path);
	auto echoes = api::MemEchos(168410);
	auto side_lobe_parameter = api::SideLobeParameter(true, 12.0f, 0.5f);
	apply_filter(spatializer, side_lobe_parameter); 
	spatialize_in_memory(spatializer, 0, 1, echoes);
	api::close_spatializer(spatializer);

	CHECK_EQ(echoes.size, 63248);
}


TEST_SUITE_END(); // end of testsuite 
