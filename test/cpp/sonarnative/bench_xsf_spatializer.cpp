﻿#include <doctest.h>

#include <string>
#include <chrono>
#include <fstream> 
#include <functional> 
#include <inttypes.h> 
#include <mutex> 

using namespace std;

#if defined(_WIN32) || defined(WIN32)
#include <filesystem>
using namespace std::filesystem;
#else
#include <experimental/filesystem>
using namespace std::experimental::filesystem;
#endif


#include "sonarnative/api/xsf_api.hpp"
using namespace sonarnative;
#include "common/testUtils.hpp"

TEST_SUITE_BEGIN("Bench XSF spatialization");


TEST_CASE("Bench XSF spatialization, beam echoes are dismissed") {
	string data_dir = testcommon::getTestDataPath();
	string xsf_path = (data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc").c_str();
	CHECK(testcommon::fileExists(xsf_path));

	size_t echo_count = 0;
	mutex cumulation_lock;
	function<void(xsf::BeamEchos&)> fake_callback = [&echo_count, &cumulation_lock](xsf::BeamEchos& echoes) {
		std::lock_guard<std::mutex> guard(cumulation_lock);
		echo_count += echoes.size;
	};

	auto start = chrono::steady_clock::now();
	auto spatializer = api::open_spatializer(xsf_path);
	spatializer.hold->perform_all_swath(fake_callback);
	api::close_spatializer(spatializer);
	auto end = chrono::steady_clock::now();

	chrono::duration<double, milli>((end - start)).count();
	cout << "Spatialization of all swaths, beam echoes are dismissed. Generates " << echo_count << " and takes " << chrono::duration<double, milli>(end - start).count() << "ms" << endl;

	CHECK_EQ(echo_count, 223260924);

}

TEST_CASE("Bench XSF spatialization, beam echoes are kept in memory") {

	string data_dir = testcommon::getTestDataPath();
	string xsf_path = (data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc").c_str();
	CHECK(testcommon::fileExists(xsf_path));

	auto start = chrono::steady_clock::now();
	auto spatializer = api::open_spatializer(xsf_path);
	api::set_spatialization_method(spatializer, SpatializationMethod::DetectionInterpolation);
	size_t echo_count = 0;
	api::MemEchos echoes = api::MemEchos(300000);
	for (size_t swath = 0; swath < spatializer.hold->get_swath_count(); swath++)
	{
		spatialize_in_memory(spatializer, swath, 1, echoes);
		echo_count += echoes.size;
		echoes.size = 0;
	}
	api::close_spatializer(spatializer);

	auto end = chrono::steady_clock::now();
	chrono::duration<double, milli>(end - start).count();
	cout << "Spatialization of all swaths, beam echoes are copied in memory. Generates " << echo_count << " and takes " << chrono::duration<double, milli>(end - start).count() << "ms" << endl;
	CHECK_EQ(echo_count, 223260924);
}

TEST_CASE("Bench XSF spatialization, beam echoes are written on disk") {

	string data_dir = testcommon::getTestDataPath();
	string xsf_path = (data_dir + "/file/cpp/0078_20130204_115147_Thalia.xsf.nc").c_str();
	CHECK(testcommon::fileExists(xsf_path));

#if defined(_WIN32) || defined(WIN32)
	string tmpDir = filesystem::temp_directory_path().generic_string();
#else
	string tmpDir = "/temp";
#endif

	auto start = chrono::steady_clock::now();
	auto spatializer = api::open_spatializer(xsf_path);
	auto fileEchos = spatialize_all_on_disk(spatializer, tmpDir + "/Bench_XSF");
	api::close_spatializer(spatializer);

	auto end = chrono::steady_clock::now();
	chrono::duration<double, milli>(end - start).count();
	cout << "Spatialization of all swaths, beam echoes are written on disk. Generates " << fileEchos.size() << " and takes " << chrono::duration<double, milli>(end - start).count() << "ms" << endl;

	// XSF spatialization, no regression checking (Min/Max/Mean of all beam echoes)
	CHECK_EQ(fileEchos.size(), 223260924);

	// Chech files
	ifstream longitude_stream(fileEchos.longitude().c_str(), ios::binary);
	double read_longitudes[xsf::BEAM_ECHO_PER_CALLBACK];
	ifstream latitude_stream(fileEchos.latitude().c_str(), ios::binary);
	double read_latitudes[xsf::BEAM_ECHO_PER_CALLBACK];

	double min_lon = 90.0, max_lon = -90.0;
	double min_lat = 190.0, max_lat = -190.0;
	long double sum_lon = 0.0, sum_lat = 0.0;
	size_t read_echo_idx = xsf::BEAM_ECHO_PER_CALLBACK;
	size_t total_beam_echo_count = 0;
	for (; total_beam_echo_count < fileEchos.size(); total_beam_echo_count++) {
		if (read_echo_idx == xsf::BEAM_ECHO_PER_CALLBACK) {
			size_t nb_byte_to_read = min(xsf::BEAM_ECHO_PER_CALLBACK, fileEchos.size() - xsf::BEAM_ECHO_PER_CALLBACK) * sizeof(double);
			longitude_stream.read((char*)& read_longitudes, nb_byte_to_read);
			latitude_stream.read((char*)& read_latitudes, nb_byte_to_read);
			read_echo_idx = 0;
		}
		min_lon = fmin(min_lon, read_longitudes[read_echo_idx]);
		max_lon = fmax(max_lon, read_longitudes[read_echo_idx]);
		sum_lon += read_longitudes[read_echo_idx];
		min_lat = fmin(min_lat, read_latitudes[read_echo_idx]);
		max_lat = fmax(max_lat, read_latitudes[read_echo_idx]);
		sum_lat += read_latitudes[read_echo_idx];
		read_echo_idx++;
	}
	CHECK_EQ(total_beam_echo_count, 223260924);

	int64_t long_sum_lon = (int64_t)sum_lon;
	int64_t long_sum_lat = (int64_t)sum_lat;

	// Check min/max/mean
	CHECK_EQ(min_lon, -4.4228664995140861);
	CHECK_EQ(max_lon, -4.3739577685252291);
	CHECK_EQ(min_lat, 48.302663355456914);
	CHECK_EQ(max_lat, 48.307521073230674);
	CHECK_EQ(long_sum_lon, -981288077ll);
	CHECK_EQ(long_sum_lat, 10784623233ll);

	longitude_stream.close();
	latitude_stream.close();
#if defined(_WIN32) || defined(WIN32)

	filesystem::remove_all(tmpDir + "/Bench_XSF");
#endif
}

TEST_SUITE_END(); // end of testsuite 
