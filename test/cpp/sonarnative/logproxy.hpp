#pragma once

#include <string>
#include <iostream> 

#include "sonarnative/log/proxy.hpp"

namespace testsonarnative {

	class TestLogProxy : public sonarnative::LogProxy {
	public:
		TestLogProxy() {};
		~TestLogProxy() {};

		std::string message() {
			return last_message;
		}

		virtual void fatal(std::string message) { last_message = message; }
		virtual void error(std::string message) { last_message = message; }
		virtual void warn(std::string message) { last_message = message; }
		virtual void info(std::string message) { last_message = message; }
		virtual void debug(std::string message) { last_message = message; }

		std::string last_message;
	};

	class CoutLogProxy : public sonarnative::LogProxy {
	public:
		CoutLogProxy() {};
		~CoutLogProxy() {};

		virtual void fatal(std::string message) { std::cout << message << std::endl; }
		virtual void error(std::string message) { std::cout << message << std::endl; }
		virtual void warn(std::string message) { std::cout << message << std::endl; }
		virtual void info(std::string message) { std::cout << message << std::endl; }
		virtual void debug(std::string message) { std::cout << message << std::endl; }

		std::string last_message;
	};

} // end namespace testsonarnative