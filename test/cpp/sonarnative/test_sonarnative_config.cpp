#include <doctest.h>
#include <string>

#include "sonarnative/sonarnative.hpp"
#include "sonarnative/log/log.hpp"
#include "sonarnative/sonarnative_config.hpp"
#include "logproxy.hpp"

#include "sonarnative/sample/sample.hpp"


TEST_SUITE_BEGIN("core");

TEST_CASE("check version"){

    #ifndef SONAR_NATIVE_VERSION_MAJOR
        #error "SONAR_NATIVE_VERSION_MAJOR is undefined"
    #endif
    

    #ifndef SONAR_NATIVE_VERSION_MINOR
        #error "SONAR_NATIVE_VERSION_MINOR is undefined"
    #endif


    #ifndef SONAR_NATIVE_VERSION_PATCH
        #error "SONAR_NATIVE_VERSION_PATCH is undefined"
    #endif

    CHECK_GE(SONAR_NATIVE_VERSION_MAJOR , 1);
    CHECK_GE(SONAR_NATIVE_VERSION_MINOR , 0);
    CHECK_GE(SONAR_NATIVE_VERSION_PATCH , 0);
}

TEST_CASE("check log") {

	auto proxy = new testsonarnative::TestLogProxy();
	sonarnative::Log::initializeLog(proxy);
	
	auto sample = new sonarnative::Sample("Sample of logging");
	sample->name();

	CHECK_MESSAGE(proxy->message().find("Entering name accessor") != std::string::npos, "Logging failed");

	delete sample;
}


TEST_SUITE_END(); // end of testsuite core
