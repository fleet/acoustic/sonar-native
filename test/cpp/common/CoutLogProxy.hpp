#pragma once

#include <string>
#include "sonarnative/log/proxy.hpp"

namespace testcommon {

	class CoutLogProxy : public sonarnative::LogProxy {
	public:
		CoutLogProxy();
		~CoutLogProxy();

		virtual void fatal(std::string message);
		virtual void error(std::string message);
		virtual void warn(std::string message);
		virtual void info(std::string message);
		virtual void debug(std::string message);
	};

} // end namespace testcommon