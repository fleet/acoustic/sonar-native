
#include <string>
#include <fstream> // ifstream
#include <cstdlib> // rand

#ifdef _WIN32
#include <io.h> 
#define access    _access_s
#else
#include <unistd.h>
#endif

#if defined(_WIN32) || defined(WIN32)
#include <shlobj.h>
#else
#include <linux/limits.h>
#include <cstring> // strcpy
#define MAX_PATH PATH_MAX 
#endif

#include <doctest.h>
#include "common/testUtils.hpp"


namespace testcommon {

	std::string generateTmpPath()
	{
		char file[MAX_PATH];
		std::tmpnam(file);
		return std::string(file);
	}


	bool fileExists(const std::string& filename)
	{
		return access(filename.c_str(), 0) == 0;
	}

	// Read the content of a file
	std::string readFile(const std::string txtFileName) {
		std::string result = "";
		std::ifstream file(txtFileName);
		if (file.is_open()) {
			file >> result;
			file.close();
		}
		return result;
	}

	// Return the path of test files
	std::string getTestDataPath() {
		std::string result = "";
		if (char* v3dTestData = std::getenv("V3D_TESTDATA_DIR")) {
			result = v3dTestData;
		}
		else {
				char dataDirFile[MAX_PATH];
#ifdef _WIN32
				snprintf(dataDirFile, MAX_PATH, "%s%s%c.GLOBE_TESTDATADIR.txt", getenv("HOMEDRIVE"), getenv("HOMEPATH"), PATH_SEPARATOR);
#else
				snprintf(dataDirFile, MAX_PATH, "%s%c.GLOBE_TESTDATADIR.txt", getenv("HOME"), PATH_SEPARATOR);
#endif
				result = readFile(dataDirFile);
				if (result.length() == 0) {
#ifdef _WIN32
					snprintf(dataDirFile, MAX_PATH, "c:%c.GLOBE_TESTDATADIR.txt", PATH_SEPARATOR);
#else
					std::strcpy(dataDirFile, "/.GLOBE_TESTDATADIR.txt");
#endif
					result = readFile(dataDirFile);
				}
		}

		CHECK_GT(result.length(), 1);

		return result;
	}
} // end namespace testcommon

