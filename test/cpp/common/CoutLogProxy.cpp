#include <string>
#include <iostream>

#include "sonarnative/log/proxy.hpp"
#include "common/CoutLogProxy.hpp"

namespace testcommon {
	 
	CoutLogProxy::CoutLogProxy() {};
	CoutLogProxy::~CoutLogProxy() {};

	void CoutLogProxy::fatal(std::string message) { std::cout << "Fatal : " << message; }
	void CoutLogProxy::error(std::string message) { std::cout << "Error : " << message; }
	void CoutLogProxy::warn(std::string message) { std::cout << "Warning : " << message; }
	void CoutLogProxy::info(std::string message) { std::cout << "Info : " << message; }
	void CoutLogProxy::debug(std::string message) { std::cout << "Debug : " << message; }

} // end namespace testcommon
