#pragma once

#include <string>
#include <iostream>
#include <fstream>
#include <functional> // std::function

#if defined(_WIN32) || defined(WIN32)
const char PATH_SEPARATOR = '\\';
#else
const char PATH_SEPARATOR = '/';
#endif

namespace testcommon {

	// Create a temporary file path
	std::string generateTmpPath();

	// Return the path of test files
	std::string getTestDataPath();

	// Return the path of test files
	bool fileExists(const std::string& filename);

	// Read the content of a file
	std::string readFile(const std::string txtFileName);

	// Create a binary file
	template <class type>
	std::string generateInterpolatedFile(type firstVal, type incVal, int valCount)
	{
		auto result = generateTmpPath();
		auto stream = std::ofstream(result, std::ofstream::binary | std::ofstream::trunc);
		type val = firstVal;
		for (int i = 0; i < valCount; i++)
		{
			stream.write(reinterpret_cast<char*>(&val), sizeof val);
			val += incVal;
		}
		stream.close();

		return result;
	}

	template <class type>
	std::string generateFile(std::function<type(int)> valProvider, int valCount)
	{
		auto result = generateTmpPath();
		auto stream = std::ofstream(result, std::ofstream::binary | std::ofstream::trunc);
		for (int i = 0; i < valCount; i++)
		{
			type val = valProvider(i);
			stream.write(reinterpret_cast<char*>(&val), sizeof val);
		}
		stream.close();

		return result;
	}


} // end namespace testcommon