#pragma once

#include <string>
#include <iomanip>
#include <sstream>

#include "sonarnative/utils/progress_callback.hpp"

namespace testcommon {
    
	/*
	 * Default implementation class to have a the progress feedback.
	 */
	class DefaultProgressCallback : public sonarnative::ProgressCallback
    {
    public:
		// Constructor
		DefaultProgressCallback() = default;

		/*
		 * Progression evolution.
		 */
		void progress(double complete, std::string message) const {
			std::ostringstream streamObj3;
			streamObj3 << std::fixed;
			streamObj3 << std::setprecision(2);
			streamObj3 << complete << "% " << message;
			std::cout << streamObj3.str() << std::endl;
		};
    };

} // end namespace testcommon